#define __IO_RETARGET_STREAM_NONE 0
#define __IO_RETARGET_STREAM_RTA 1
#define __IO_RETARGET_STREAM_COM_0 2
#define __IO_RETARGET_STREAM_COM_1 3
/*#define RTA_EXT_DABT_ERROR 1 */
/*#define __RTA_OUT_SIZE_BYTES 1024 */
/*#define __RTA_TERM_W_BLOCKING 1 */
#define SPI_CLK 12000000UL
#define I2C_CLK 12000000UL
#define UART_CLK 24000000UL
/*#define ARM_IRQ_AVAILABLE ARM_IRQ_5 */
/*#define NUM_SERIAL_DEVICES 2 */
#define UART_PRINTF_PORT 0
#define UART_PRINTF_BAUDRATE 115200UL
/*#define LV_DBUG_UART_PORT 1 */
#define __IO_RETARGET_STDIO_ROUTE __IO_RETARGET_STREAM_COM_0
/*#define LHOST_NAME "board" */

/*#include "LVSysIncludes.h"
#include "LVCCG.h"
#include "ARM_I2CWrapper.h"
#include "ARM_SPIWrapper.h"
*/