/*----------------------------------------------------------------------------
 *      RL-ARM - TCPnet
 *----------------------------------------------------------------------------
 *      Name:    TELNET_UIF.C
 *      Purpose: Telnet Server User Interface Module
 *      Rev.:    V4.05
 *----------------------------------------------------------------------------
 *      This code is part of the RealView Run-Time Library.
 *      Copyright (c) 2004-2009 KEIL - An ARM Company. All rights reserved.
 *---------------------------------------------------------------------------*/

#include "LVSysIncludes.h"
#include "LVCCG.h"
#include "LVConfig.h"

#if (SocketSupport && TNET_ENABLE)

#include "RLARM_Net_Config.h"
#include "File_Config.h"
#include <string.h>
#include <stdio.h>

/* Inline C Node Include Files */
//extern OS_ID sem_gps;

/* Net_Config.c */
extern struct tcp_info tcp_socket[];
extern U8 const tcp_NumSocks;
extern U8 const tnet_EnAuth;
extern U8       tnet_auth_passw[20];
extern LOCALM const nlocalm[3];

/* globale variabelen in labview */
//let op, is een andere taak!
extern U32 GPS_Global_Tijd_0;
extern U32 GPS_Global_datum_1;
extern double GPS_Global_Latitude_2;
extern double GPS_Global_Longitude_3;

/* ANSI ESC Sequences for terminal control. */
#define CLS     "\033[2J"
#define TBLUE   "\033[37;44m"
#define TNORM   "\033[0m"

//extern BOOL LEDrun;
FINFO fileinfo=0;
U32 offset=0;
BOOL firstcall=1;
char currentdir[30]={"\\"};



typedef struct {
 U32 Time;
 U8 Fix;
 U32 Latitude;
 U32 Longitude;
 U16 Speed;
 U16 Course;
 U32 Date;
} GPS_info;

void Get_GPS(GPS_info* gpsinfo);

/* Pointer to Function returning Void (any number of parameters) */
typedef void (*PFV)();


/* My structure of a Telnet U32 storage variable. This variable is private */
/* for each Telnet Session and is not altered by Telnet Server. It is only */
/* set to zero when tnet_process_cmd() is called for the first time.       */
typedef struct {
  U8 id;
  U8 nmax;
  U8 idx;
} MY_BUF;
#define MYBUF(p)        ((MY_BUF *)p)

/* Local variables */
static U8 const tnet_header[] = {
  CLS "\r\n"
  "        " TBLUE
  "*=============================================================*\r\n" TNORM
  "        " TBLUE
  "*            Keil Embedded Telnet Server Demo v3.00           *\r\n" TNORM
  "        " TBLUE
  "*=============================================================*\r\n" TNORM
  };
static U8 const tnet_help1[] = {
  "\r\n\r\n"
  "    Available Commands:\r\n"
  "    ----------------------------\r\n"
  "    led xx      - write hexval xx to LED port\r\n"
  "    led         - enable running lights\r\n"
  "    adin x      - read AD converter intput x\r\n"
  "    meas n      - display n measurements\r\n"
  "    tcpstat     - display a tcp status\r\n"
  "    rinfo       - display remote machine info\r\n"};
static U8 const tnet_help2[] = {
  "    passw [new] - change system password\r\n"
  "    passwd      - display current password\r\n"};
static U8 const tnet_help3[] = {
  "    help, ?     - display this help\r\n"
  "    bye         - disconnect\r\n\r\n"
  "    dir         - get file list\r\n"
  "    cat [file]  - read a file\r\n"
  "    del [file]  - delete a file\r\n"
  "    diskinfo    - display disk info\r\n"
  "    <ESC>,<^C>  - disconnect\r\n"
  "    <BS>        - delete Character left\r\n"
  "    <UP><DOWN>  - recall Command History\r\n"};

static U8 const meas_header[] = {
  "\r\n"
  " Nr.   ADIN0  ADIN1  ADIN2  ADIN3  ADIN4  ADIN5  ADIN6  ADIN7\r\n"
  "=============================================================="};

static U8 const tcp_stat[] = {
  CLS "\r\n"
  "     " TBLUE
  "=============================================================\r\n" TNORM
  "     " TBLUE
  " Socket   State       Rem_IP       Rem_Port  Loc_Port  Timer \r\n" TNORM
  "     " TBLUE
  "=============================================================\r\n" TNORM
  };

static U8 const alg_stat[] = {
  CLS "\r\n"
  "     " TBLUE
  "=============================================================\r\n" TNORM
  "     " TBLUE
  " INFO   State       Rem_IP       Rem_Port  Loc_Port  Timer \r\n" TNORM
  "     " TBLUE
  "=============================================================\r\n" TNORM
  };

static char const state[][11] = {
  "FREE",
  "CLOSED",
  "LISTEN",
  "SYN_REC",
  "SYN_SENT",
  "FINW1",
  "FINW2",
  "CLOSING",
  "LAST_ACK",
  "TWAIT",
  "CONNECT"};

//BOOL LEDrun;

/*----------------------------------------------------------------------------
 *      Functions
 *---------------------------------------------------------------------------*/

//extern void LED_out (U32 val);
//extern U16 AD_in (U32 ch);

/*----------------------------------------------------------------------------
 *      Telnet CallBack Functions
 *---------------------------------------------------------------------------*/

/*--------------------------- tnet_cbfunc -----------------------------------*/

U16 tnet_cbfunc (U8 code, U8 *buf, U16 buflen) {
  /* This function is called by the Telnet Client to get formated system    */
  /* messages for different code values.                                    */
  /* Values for 'code':                                                     */
  /*    0 - initial header                                                  */
  /*    1 - prompt string                                                   */
  /*    2 - header for login only if authorization is enabled               */
  /*    3 - string 'Username' for login                                     */
  /*    4 - string 'Password' for login                                     */
  /*    5 - message 'Login incorrect'                                       */
  /*    6 - message 'Login timeout'                                         */
  /*    7 - Unsolicited messages from Server (ie. Basic Interpreter)        */
  U16 len = 0;

  /* Make a reference to disable compiler warning. */
  buflen = buflen;

  switch (code) {
    case 0:
      /* Write initial header after login. */
      len = str_copy (buf, (U8 *)&tnet_header);
      break;
    case 1:
      /* Write a prompt string. */
         len = str_copy (buf, "\r\nLPC1788> ");
      break;
    case 2:
      /* Write Login header. */
      len = str_copy (buf, CLS "\r\nKeil Embedded Telnet Server V3.00,"
                               " please login...\r\n");
      break;
    case 3:
      /* Write 'username' prompt. */
      len = str_copy (buf, "\r\nUsername: ");
      break;
    case 4:
      /* Write 'Password' prompt. */
      len = str_copy (buf, "\r\nPassword: ");
      break;
    case 5:
      /* Write 'Login incorrect'.message. */
      len = str_copy (buf, "\r\nLogin incorrect");
      break;
    case 6:
      /* Write 'Login Timeout' message. */
      len = str_copy (buf, "\r\nLogin timed out after 60 seconds.\r\n");
      break;
  }
  return (len);
}


/*--------------------------- tnet_process_cmd ------------------------------*/

U16 tnet_process_cmd (U8 *cmd, U8 *buf, U16 buflen, U32 *pvar) {
  /* This is a Telnet Client callback function to make a formatted output   */
  /* for 'stdout'. It returns the number of bytes written to the out buffer.*/
  /* Hi-bit of return value (len is or-ed with 0x8000) is a disconnect flag.*/
  /* Bit 14 (len is or-ed with 0x4000) is a repeat flag for the Tnet client.*/
  /* If this bit is set to 1, the system will call the 'tnet_process_cmd()' */
  /* again with parameter 'pvar' pointing to a 4-byte buffer. This buffer   */
  /* can be used for storing different status variables for this function.  */
  /* It is set to 0 by Telnet server on first call and is not altered by    */
  /* Telnet server for repeated calls. This function should NEVER write     */
  /* more than 'buflen' bytes to the buffer.                                */
  /* Parameters:                                                            */
  /*   cmd    - telnet received command string                              */
  /*   buf    - Telnet transmit buffer                                      */
  /*   buflen - length of this buffer (500-1400 bytes - depends on MSS)     */
  /*   pvar   - pointer to local storage buffer used for repeated loops     */
  /*            This is a U32 variable - size is 4 bytes. Value is:         */
  /*            - on 1st call = 0                                           */
  /*            - 2nd call    = as set by this function on first call       */
  TCP_INFO *tsoc;
  REMOTEM rm;
  U32 val,ch,temp;
  U16 len = 0;
  char filename[50],tempstring[50];
  char buf2[100], findstring[50];
  FILE* fh;
	//FINFO tempinfo;
//	MMCFG MMCCFG;
    int end;
	   	U32 BlockNr,BytesFree;
	U16 BlockLen;
  char* string;
  U16 testlen;
  int i=0;
  PFV address;
  char row1[50],row2[50],row3[50],row4[50],row5[50],row6[50],rowdate[50],rowtime[50],rowgateway[50],rowdns[50],rowdns2[50];
  U32 Date,Time;
  GPS_info gpsinfo;

  switch (MYBUF(pvar)->id) {
    case 0:
      /* First call to this function, the value of '*pvar' is 0 */
      break;

    case 1:
      /* Repeated call, command 'MEAS' measurements display. */
      while (len < buflen-80) {
        /* Let's use as much of the buffer as possible. */
        /* This will produce less packets and speedup the transfer. */
        len += sprintf ((char *)(buf+len), "\r\n%4d", MYBUF(pvar)->idx);
        for (val = 0; val < 8; val++) {
          len += sprintf ((char *)(buf+len), "%7d", 55); //AD_in(val)
        }
        if (++MYBUF(pvar)->idx >= MYBUF(pvar)->nmax) {
          /* OK, we are done. */
          return (len);
        }
      }
      /* Request a repeated call, bit 14 is a repeat flag. */
      return (len | 0x4000);

    case 2:
      /* Repeated call, TCP status display. */
      while (len < buflen-80) {
        /* Let's use as much of the buffer as possible. */
        /* This will produce less packets and speedup the transfer. */
        if (MYBUF(pvar)->idx == 0) {
          len += str_copy (buf, (U8 *)tcp_stat);
        }
        tsoc = &tcp_socket[MYBUF(pvar)->idx];
        len += sprintf   ((char *)(buf+len), "\r\n%9d %10s  ", MYBUF(pvar)->idx, 
                          state[tsoc->State]);
        if (tsoc->State <= TCP_STATE_CLOSED) {
          len += sprintf ((char *)(buf+len),
                          "        -             -         -       -\r\n");
        }
        else if (tsoc->State == TCP_STATE_LISTEN) {
          len += sprintf ((char *)(buf+len),
                          "        -             -     %5d       -\r\n",
                          tsoc->LocPort);
        }
        else {
          /* First temporary print for alignment. */
          sprintf ((char *)(buf+len+16),"%d.%d.%d.%d",tsoc->RemIpAdr[0],
                tsoc->RemIpAdr[1],tsoc->RemIpAdr[2],tsoc->RemIpAdr[3]);
          len += sprintf ((char *)(buf+len),"%15s    %5d    %5d     %4d\r\n",
                          buf+len+16,tsoc->RemPort,tsoc->LocPort,tsoc->AliveTimer);
        }
        if (++MYBUF(pvar)->idx >= tcp_NumSocks) {
          /* OK, we are done, reset the index counter for next callback. */
          MYBUF(pvar)->idx = 0;
          /* Setup a callback delay. This function will be called again after    */
          /* delay has expired. It is set to 20 system ticks 20 * 100ms = 2 sec. */
          tnet_set_delay (20);
          break;
        }
      }
      /* Request a repeated call, bit 14 is a repeat flag. */
      return (len |= 0x4000);



	  case 3:

	Get_GPS(&gpsinfo);

	sprintf(row1,"\r\n*===============================*");
	row1[strlen(row1)]=0;


	sprintf(row2,"\r\n* IP Address: %d.%d.%d.%d",nlocalm[0].IpAdr[0],nlocalm[0].IpAdr[1],nlocalm[0].IpAdr[2],nlocalm[0].IpAdr[3]);
	
	for(i=strlen(row2);i<35;i++)
	{
	 	row2[i]=' ';
	}
	row2[34]='*';
	row2[strlen(row2)]=0;


	sprintf(rowgateway,"\r\n* Def. Gateway: %d.%d.%d.%d",nlocalm[0].DefGW[0],nlocalm[0].DefGW[1],nlocalm[0].DefGW[2],nlocalm[0].DefGW[3]);
	for(i=strlen(rowgateway);i<35;i++)
	{
	 	rowgateway[i]=' ';
	}
	rowgateway[34]='*';
	rowgateway[strlen(rowgateway)]=0;
	
	sprintf(rowdns,"\r\n* Primary DNS: %d.%d.%d.%d",nlocalm[0].PriDNS[0],nlocalm[0].PriDNS[1],nlocalm[0].PriDNS[2],nlocalm[0].PriDNS[3]);
	for(i=strlen(rowdns);i<35;i++)
	{
	 	rowdns[i]=' ';
	}
	rowdns[34]='*';
	rowdns[strlen(rowdns)]=0;	


	sprintf(rowdns2,"\r\n* Secondary DNS: %d.%d.%d.%d",nlocalm[0].SecDNS[0],nlocalm[0].SecDNS[1],nlocalm[0].SecDNS[2],nlocalm[0].SecDNS[3]);
	for(i=strlen(rowdns2);i<35;i++)
	{
	 	rowdns2[i]=' ';
	}
	rowdns2[34]='*';
	rowdns2[strlen(rowdns2)]=0;	
	
	
	sprintf(row3,"\r\n* Signal Strength: %d",75);
	for(i=strlen(row3);i<35;i++)
	{
	 	row3[i]=' ';
	}
	row3[34]='*';
	row3[strlen(row3)]=0;			
						

	//sprintf(row4,"\r\n* GPS Fix: %c",'A');
	sprintf(row4,"\r\n* GPS Fix: %d",gpsinfo.Fix);

	for(i=strlen(row4);i<35;i++)
	{
	 	row4[i]=' ';
	}
	row4[34]='*';
	row4[strlen(row4)]=0;

	//sprintf(row5,"\r\n* GPS Latitude: %f",52.123456);
	sprintf(row5,"\r\n* GPS Latitude: %f",((float)gpsinfo.Latitude/1000000));
	for(i=strlen(row5);i<35;i++)
	{
	 	row5[i]=' ';
	}
	row5[34]='*';
	row5[strlen(row5)]=0;

	//sprintf(row6,"\r\n* GPS Longitude: %f",5.987654);	  
	sprintf(row6,"\r\n* GPS Longitude: %f",((float)gpsinfo.Longitude/1000000));
	for(i=strlen(row6);i<35;i++)
	{
	 	row6[i]=' ';
	}
	row6[34]='*';
	row6[strlen(row6)]=0;

	//Date=fs_get_date();
	sprintf(rowdate,"\r\n* Date: %02d-%02d-%02d",Date>>16,(Date>>8)&&0xF,Date&&0xF );

	for(i=strlen(rowdate);i<35;i++)
	{
	 	rowdate[i]=' ';
	}
	rowdate[34]='*';
	rowdate[strlen(rowdate)]=0;

	//Time=fs_get_time();
	sprintf(rowtime,"\r\n* Time: %02d:%02d:%02d",Time>>16,(Time>>8)&&0xF,Time&&0xF  );

	for(i=strlen(rowtime);i<35;i++)
	{
	 	rowtime[i]=' ';
	}
	rowtime[34]='*';
	rowtime[strlen(rowtime)]=0;



	len = sprintf(buf, CLS "\r\n%s%s%s%s%s%s%s%s%s%s%s%s",row1,rowdate,rowtime,row2,rowgateway,rowdns,rowdns2,row3,row4,row5,row6,row1);
	
	MYBUF(pvar)->idx = 0;
	tnet_set_delay (20);
	//return (len |= 0x4000);
	return (len);
  }

  /* Simple Command line parser */
  len = strlen ((const char *)cmd);

//  if (tnet_ccmp (cmd, "CAT") == __TRUE) {
//    /* 'ADIN' command received */
//    if (len >= 4) {
//      sscanf ((const char *)(cmd+3),"%s",&filename);	  
//	  sprintf(tempstring,"%s%s",currentdir,filename);
//	  finit();
//      fh = fopen(tempstring,"r");
//	  if(fh!=NULL)
//	  {
//	  fseek(fh,offset+=512,SEEK_SET);
//	  len=fread(buf,1,512,fh);
//	  fclose(fh);
//	} else {
//	  len = sprintf((char*)buf,"\r\nFile not found");
//	
//	}	
//	 if(len<512)
//	 {
//	  offset=0;
//	  return (len);
//	 }
//
//      return (len | 0x4000);
//    }
//  }

//   if ((tnet_ccmp (cmd, "DIR") == __TRUE )||(tnet_ccmp (cmd, "LS") == __TRUE )) {
//    /* 'dir' command, display directory text */
//	finit();
//	//while(ffind("*.*",&info)==0)
//	
//	sprintf(findstring,"%s*",currentdir);
//	 end=ffind(findstring,&fileinfo);
//	//{
//	if(fileinfo.attrib==ATTR_DIRECTORY)
//	{
//		len= sprintf(buf2,"\r\n%s\t<DIR>",fileinfo.name);
//	} else
//	{
//		len= sprintf (buf2,"\r\n%s\t<%d kB>",fileinfo.name, fileinfo.size/1024);
//	}
//	//}
//    //len = str_copy (buf,tnet_help);
//
//	if(firstcall)
//	{
//		if(currentdir=="")
//		{
//			len = sprintf(buf,"\r\nCurrent dir: \\\r\n%s",buf2);
//		} else
//		{
//			len = sprintf(buf,"\r\nCurrent dir: %s\r\n%s",currentdir,buf2);
//		}
//		firstcall=0;
//	} else
//	{
//	 len = str_copy (buf,buf2);
//	}
//
//	if(!end)
//	{
//	 len= len | 0x4000;
//	}
//	else
//	{
//	 len = sprintf(buf,"");
//	 fileinfo.fileID  = 0;
//	 firstcall=1;
//	}
//	return len;
//  }


  if (tnet_ccmp (cmd, "STAT") == __TRUE ) {
  Get_GPS(&gpsinfo);
  	// show STAT, GPS, connection
	/*sprintf(row1,"*===========================================================*\n\r");
	sprintf(row2,"* IP Address:         %d.%d.%d.%d                           *\n\r",192,168,1,100);
	sprintf(row3,"* Signal Strength:    %d                                    *\n\r",75);
	sprintf(row4,"* GPS Fix:            %d                                    *\n\r",50);//gpsinfo.Fix);
	sprintf(row5,"* GPS Latitude:       %f                                    *\n\r",52.55);//gpsinfo.Latitude);
	sprintf(row6,"* GPS Longitude:      %f                                    *\n\r",5.92);//gpsinfo.Longitude);
	*/
	len = sprintf(buf, "\n\r* IP Address:         %d.%d.%d.%d\t\t*\n\r"
					   "* Signal Strength:    %d\t\t\t*\n\r"
					   "* GPS Date:           %d\t\t\t*\n\r"
					   "* GPS Time:           %d\t\t\t*\n\r"
					   "* GPS Fix:            %d\t\t\t*\n\r"
					   "* GPS Latitude:       %f\t\t\t*\n\r"
					   "* GPS Longitude:      %f\t\t\t*\n\r"
					   ,nlocalm[0].IpAdr[0],nlocalm[0].IpAdr[1],nlocalm[0].IpAdr[2],nlocalm[0].IpAdr[3],
					   75,gpsinfo.Date,gpsinfo.Time,gpsinfo.Fix,gpsinfo.Latitude,gpsinfo.Longitude);
	//len = sprintf(buf,"%d.%d.%d.%d  %d   %c  %f   %f\r\n",192,168,1,999,75,'A',52.123456,5.987654);
	
	//MYBUF(pvar)->id = 3;
	//len = str_copy(buf,CLS);

  	//return (len | 0x4000);
	return (len);
  }

    if (tnet_ccmp (cmd, "TCPSTAT") == __TRUE) {
    /* Display a TCP status similar to that in HTTP_Demo example. */
    /* Here the local storage '*pvar' is initialized to 0 by Telnet Server.    */
    MYBUF(pvar)->id = 2;
    len = str_copy (buf, CLS);
    return (len | 0x4000);
  }

  if (tnet_ccmp (cmd, "RINFO") == __TRUE) {
    /* Display Remote Machine IP and MAC address. */
    tnet_get_info (&rm);
    len  = sprintf ((char *)buf,"\r\n Remote IP : %d.%d.%d.%d",
                    rm.IpAdr[0],rm.IpAdr[1],rm.IpAdr[2],rm.IpAdr[3]);
    len += sprintf ((char *)(buf+len),
                    "\r\n Remote MAC: %02X-%02X-%02X-%02X-%02X-%02X",
                    rm.HwAdr[0],rm.HwAdr[1],rm.HwAdr[2],
                    rm.HwAdr[3],rm.HwAdr[4],rm.HwAdr[5]);
    return (len);
  }


  if (tnet_ccmp (cmd, "BYE") == __TRUE) {
    /* 'BYE' command, send message and disconnect */
    len = str_copy (buf, "\r\nDisconnect...\r\n");
    /* Hi bit of return value is a disconnect flag */
    return (len | 0x8000);
  }

  if (tnet_ccmp (cmd, "PASSW") == __TRUE && tnet_EnAuth) {
    /* Change the system password. */
    if (len == 5) {
      /* Disable password. */
      tnet_auth_passw[0] = 0;
    }
    else {
      mem_copy (&tnet_auth_passw, &cmd[6], 20);
    }
    len = sprintf ((char *)buf, "\r\n OK, New Password: \"%s\"",tnet_auth_passw);
    return (len);
  }




  if (tnet_ccmp (cmd, "HELP") == __TRUE || tnet_ccmp (cmd, "?") == __TRUE) {
    /* 'HELP' command, display help text */
    len = str_copy (buf,(U8 *)tnet_help1);
    if (tnet_EnAuth) {
      len += str_copy (buf+len,(U8 *)tnet_help2);
    }
    len += str_copy (buf+len,(U8 *)tnet_help3);
    return (len);
  }
  /* Unknown command, display message */
  len = str_copy  (buf, "\r\n==> Unknown Command: ");
  len += str_copy (buf+len, cmd);
  return (len);
}

void Get_GPS(GPS_info* gpsinfo)
{
	//os_sem_wait(sem_gps,2);
	gpsinfo->Date=111111;//GPS_Global_datum_1;
	gpsinfo->Time=121212;//GPS_Global_Tijd_0;
	gpsinfo->Latitude=52.5;//GPS_Global_Latitude_2;
	gpsinfo->Longitude=5.6;//GPS_Global_Longitude_3;
	gpsinfo->Speed=5;
	gpsinfo->Course=180;
	gpsinfo->Fix=1;
	//os_sem_send(sem_gps);

}

#endif

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
