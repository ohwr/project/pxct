/*----------------------------------------------------------------------------
 * Name:    ARM_SPI.c
 * Purpose: Template generic / LPC23xx SPI driver
 * Version: V1.00
 * Note(s):
 *----------------------------------------------------------------------------
 * This file is part of the uVision/ARM development tools.
 * This software may only be used under the terms of a valid, current,
 * end user licence from KEIL for a compatible version of KEIL software
 * development tools. Nothing else gives you the right to use this software.
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *
 * Copyright (c) 2008 Keil - An ARM Company. All rights reserved.
 *----------------------------------------------------------------------------
 * History:
 *          V1.00 Initial Version
 *----------------------------------------------------------------------------*/

#include <RTL.h>

#include <LPC17xx.H>                      // LPC1758 definitions

#include "ARM_SPI.h"
#include "lpc17xx_ssp.h"
#include "lpc17xx_pinsel.h"

#define SPI_MIN_INTERFACE   0
#define SPI_MAX_INTERFACE   0
 
#define SPI_INTERFACE_IN_USE(spiIf) ((SPI_MIN_INTERFACE <= spiIfNr) && (SPI_MAX_INTERFACE >= spiIfNr)) 

/*----------------------------------------------------------------------------
  SPI default sttings
 *---------------------------------------------------------------------------*/
#ifndef SPI_CLK
#define SPI_CLK  12000000UL          /* 48MHz / 4 */
#endif  

#define SPI_CLOCK_RATE          1000                     // in kHz
#define SPI_CLOCK_POLARITY      SPI_CLOCK_POLARITY_IDLELOW
#define SPI_CLOCK_PHASE         SPI_CLOCK_PHASE_FIRSTEDGE

#define SPI_REFERENCE_CLEARED   0
#define SPI_REFERENCE_CREATED   1

#define SPI0_PORT				0 // SPI0 via P0 or P1
#if (SPI0_PORT == 0 )
#define SPI0_CS					16 // P0.16
#else
#define SPI0_CS					22 // P1.22
#endif

/*----------------------------------------------------------------------------
  structure to administrate SPI
 *---------------------------------------------------------------------------*/
typedef struct {
                    int   reference;       // = portNumber + 1
           unsigned int   status;          // opened or closed
           unsigned int   chipSelect;
           unsigned int   clockRate;
           unsigned int   clockPolarity;
           unsigned int   clockPhase;
           
		   volatile unsigned long *spsr;
  		   volatile unsigned long *spdr;
} SPI_INFO;

static SPI_INFO spi_Dev[SPI_MAX_INTERFACE - SPI_MIN_INTERFACE + 1] = {
#if SPI_INTERFACE_IN_USE(0)
  { 1, 
    SPI_REFERENCE_CLEARED, 0, SPI_CLOCK_RATE, SPI_CLOCK_POLARITY_IDLELOW, SPI_CLOCK_PHASE_FIRSTEDGE, 
    (volatile unsigned long *)(LPC_SSP0_BASE + 0x04),   // S0SPSR
    (volatile unsigned long *)(LPC_SSP0_BASE + 0x08),   // S0SPDR
  },
#endif
#if SPI_INTERFACE_IN_USE(1)
  { 2, 
    SPI_REFERENCE_CLEARED, 0, SPI_CLOCK_RATE, SPI_CLOCK_POLARITY_IDLELOW, SPI_CLOCK_PHASE_FIRSTEDGE, 
    (volatile unsigned long *)(LPC_SSP1_BASE + 0x04),   // S0SPSR
    (volatile unsigned long *)(LPC_SSP1_BASE + 0x08),   // S0SPDR
  },
#endif
};

/*----------------------------------------------------------------------------
  Function Prototypes
 *---------------------------------------------------------------------------*/
static void          spiWaitForComplete(SPI_INFO *pSpiDev);
static void          spiSendByte       (SPI_INFO *pSpiDev, unsigned char data);
static unsigned char spiGetReceivedByte(SPI_INFO *pSpiDev);
static unsigned int  spicGetStatus     (SPI_INFO *pSpiDev);

/*----------------------------------------------------------------------------
  SPI create a reference
 *---------------------------------------------------------------------------*/
void ARM_SPI_CreateConfigurationReference (
                         int    chipSelect,
                unsigned int    clockRate,
                unsigned int    clockPolarity,
                unsigned int    clockPhase,
                         int*  pReferenceOut,
                         int*  pErrOut) {

	SSP_CFG_Type ssp_config;
	PINSEL_CFG_Type pincfg;

  if ( (chipSelect < SPI_MIN_INTERFACE) ||
       (chipSelect > SPI_MAX_INTERFACE)   ) {
    *pErrOut = SPI_CHIPSELECT_NOTKNOWN_ERR;              // check the device number
    return;
  }

  if ( (clockPolarity != SPI_CLOCK_POLARITY_IDLELOW ) &&
       (clockPolarity != SPI_CLOCK_POLARITY_IDLEHIGH)   ){ 
    *pErrOut = SPI_CLKPOLARITY_NOTSUPPORTED_ERR;         // check the clock polarity
    return;
  }

  if ( (clockPhase != SPI_CLOCK_POLARITY_IDLELOW ) &&
       (clockPhase != SPI_CLOCK_POLARITY_IDLEHIGH)   ){ 
    *pErrOut = SPI_CLKPHASE_NOTSUPPORTED_ERR;            // check the clock polarity
    return;
  }

  *pErrOut = SPI_OK;

  switch (chipSelect) {
    #if SPI_INTERFACE_IN_USE(0)
    case 0:
		
		#if (SPI0_PORT == 0)
		// SPI Port 0 via P0.15 (SCK0), P0.16 (SSEL0) function GPIO, P0.17 (MISO0), P0.18 (MOSI0)
		pincfg.Portnum=0;
		pincfg.Funcnum=0;
		pincfg.Pinmode=PINSEL_PINMODE_PULLUP;
		pincfg.OpenDrain=PINSEL_PINMODE_NORMAL;
		
		pincfg.Pinnum=SPI0_CS;
		PINSEL_ConfigPin(&pincfg);
		
		pincfg.Funcnum=2;
		pincfg.Pinnum=15;
		PINSEL_ConfigPin(&pincfg);
		pincfg.Pinnum=18;
		PINSEL_ConfigPin(&pincfg);
		pincfg.Pinnum=17;
		PINSEL_ConfigPin(&pincfg);

		GPIO_SetDir(0,(1 << SPI0_CS),1); // P0.16 as output
		#else

		// SPI Port 0 via P1.20 (SCK0), P1.22 (SSEL0) function GPIO, P1.23 (MISO0), P1.24 (MOSI0)
		pincfg.Portnum=1;
		pincfg.Funcnum=0;
		pincfg.Pinmode=PINSEL_PINMODE_PULLUP;
		pincfg.OpenDrain=PINSEL_PINMODE_NORMAL;
		
		pincfg.Pinnum=22;
		PINSEL_ConfigPin(&pincfg);
		
		pincfg.Funcnum=3;
		pincfg.Pinnum=20;
		PINSEL_ConfigPin(&pincfg);
		pincfg.Pinnum=24;
		PINSEL_ConfigPin(&pincfg);
		pincfg.Pinnum=23;
		PINSEL_ConfigPin(&pincfg);

		GPIO_SetDir(1,(1 << SPI0_CS),1); // P1.22 as output
		#endif
	  
	  SSP_ConfigStructInit(&ssp_config);
	  ssp_config.ClockRate=clockRate*1000; // *1000?
	  ssp_config.CPOL=clockPolarity;
	  ssp_config.CPHA=clockPhase;


	  SSP_Init(LPC_SSP0,&ssp_config);
	  SSP_Cmd(LPC_SSP0,1);
      break;
    #endif /* SPI_INTERFACE_IN_USE(0) */
    
    #if SPI_INTERFACE_IN_USE(1)
	case 1:
		pincfg.Portnum=0;
		pincfg.Funcnum=0;
		pincfg.Pinmode=PINSEL_PINMODE_PULLUP;
		pincfg.OpenDrain=PINSEL_PINMODE_NORMAL;
		
		pincfg.Pinnum=6;
		PINSEL_ConfigPin(&pincfg);
		
		pincfg.Funcnum=2;
		pincfg.Pinnum=7;
		PINSEL_ConfigPin(&pincfg);
		pincfg.Pinnum=8;
		PINSEL_ConfigPin(&pincfg);
		pincfg.Pinnum=9;
		PINSEL_ConfigPin(&pincfg);

		GPIO_SetDir(0,0x40,1);

	  SSP_ConfigStructInit(&ssp_config);
	  ssp_config.ClockRate=clockRate*1000;  //*1000?
	  ssp_config.CPOL=clockPolarity;
	  ssp_config.CPHA=clockPhase;

	  SSP_Init(LPC_SSP1,&ssp_config);
	  SSP_Cmd(LPC_SSP1,1);
	  break;
    #endif /* SPI_INTERFACE_IN_USE(1) */

    default:
      ; // Should never happen (protected by port check above)
  };

  spi_Dev[chipSelect - SPI_MIN_INTERFACE].status        = SPI_REFERENCE_CREATED;
  spi_Dev[chipSelect - SPI_MIN_INTERFACE].chipSelect    = chipSelect;
  spi_Dev[chipSelect - SPI_MIN_INTERFACE].clockRate     = clockRate*1000;
  spi_Dev[chipSelect - SPI_MIN_INTERFACE].clockPolarity = clockPolarity;
  spi_Dev[chipSelect - SPI_MIN_INTERFACE].clockPhase    = clockPhase;
  *pReferenceOut = spi_Dev[chipSelect - SPI_MIN_INTERFACE].reference;

}

/*----------------------------------------------------------------------------
  SPI perform a write/read access
 *---------------------------------------------------------------------------*/
void ARM_SPI_WriteRead (
                         int   nReferenceIn,
                    T_bArray*  pDataWr,
                unsigned int   bytesToRead,
                    T_bArray*  pDataRd,
                         int*  pErrOut) {

  SPI_INFO     *pSpiDev;

  unsigned int spiStatus;
  unsigned int i     = 0;
  unsigned int wrLen = pDataWr->len;
  unsigned int wr_reLen = bytesToRead;

  unsigned char CS=0;

  LPC_SSP_TypeDef *SSPx;
  LPC_GPIO_TypeDef *GPIOx;
  SSP_DATA_SETUP_Type SSPDATASETUP;
  
  if(wrLen > wr_reLen)
  {
	wr_reLen = wrLen;
  }

  nReferenceIn -= 1;                                     // reference is starting from 1

  if ( (nReferenceIn < (SPI_MIN_INTERFACE)) ||           // check the I2C reference
       (nReferenceIn > (SPI_MAX_INTERFACE))   ) {
    *pErrOut = SPI_REFERENCE_NOTKNOWN_ERR;
    return;
  }

  *pErrOut = SPI_OK;
  pSpiDev = &spi_Dev[(nReferenceIn - SPI_MIN_INTERFACE)];

  switch(pSpiDev->chipSelect)
  {
   case 0: 
   		SSPx=LPC_SSP0;
		CS=SPI0_CS;
		#if (SPI0_PORT == 0)
			GPIOx=LPC_GPIO0;
		#else
			GPIOx=LPC_GPIO1;
		#endif
   break;
   case 1:
		SSPx=LPC_SSP1;
		CS=6;
		GPIOx=LPC_GPIO0;
   break;
   default:

  }

  if (pSpiDev->status != SPI_REFERENCE_CREATED) {
    *pErrOut = SPI_REFERENCE_NOTCREATED_ERR;
    return;
  }

  GPIOx->FIOCLR = (1 << CS); // CS uit
	
  SSPDATASETUP.tx_data=(void*)pDataWr->data;
  //SSPDATASETUP.tx_cnt=wrLen;
  SSPDATASETUP.rx_data=(void*)pDataRd->data;
  //SSPDATASETUP.rx_cnt=bytesToRead;
  SSPDATASETUP.length=wr_reLen;
  //SSPDATASETUP.status=spiStatus;
	
  SSP_ReadWrite(SSPx,&SSPDATASETUP,SSP_TRANSFER_POLLING);		
	
  GPIOx->FIOSET = (1 << CS);  //CS aan
}


/*----------------------------------------------------------------------------
  SPI close a  reference
 *---------------------------------------------------------------------------*/
void ARM_SPI_CloseReference (
                         int   nReferenceIn,
                         int*  pErrOut) {
  SPI_INFO *pSpiDev;
  PINSEL_CFG_Type pincfg;

  nReferenceIn -= 1;                                     // reference is starting from 1

  if ( (nReferenceIn < (SPI_MIN_INTERFACE)) ||           // check the SPI reference
       (nReferenceIn > (SPI_MAX_INTERFACE))   ) {
    *pErrOut = SPI_REFERENCE_NOTKNOWN_ERR;
    return;
  }

  *pErrOut = SPI_OK;
  pSpiDev = &spi_Dev[(nReferenceIn - SPI_MIN_INTERFACE)];

  switch (pSpiDev->reference) {
    #if SPI_INTERFACE_IN_USE(0)
    case 0:
      //LPC_SC->PCONP   &= ~(1 << 8);                              // disable power contol for SPI0
      //PINSEL0 &= ~0xC0000000;  /* clear pin settings */
      //PINSEL1 &= ~0x0000003F;  /* clear pin settings */

	  /*PINSEL_ConfigPin(0,15,0);
	  PINSEL_ConfigPin(0,16,0);
	  PINSEL_ConfigPin(0,17,0);
	  PINSEL_ConfigPin(0,18,0);
	  */
		pincfg.Portnum=0;
		pincfg.Funcnum=0;
		pincfg.Pinmode=PINSEL_PINMODE_PULLUP;
		pincfg.OpenDrain=PINSEL_PINMODE_NORMAL;
		
		pincfg.Pinnum=16;
		PINSEL_ConfigPin(&pincfg);
		
		//pincfg.Funcnum=2;
		pincfg.Pinnum=15;
		PINSEL_ConfigPin(&pincfg);
		pincfg.Pinnum=17;
		PINSEL_ConfigPin(&pincfg);
		pincfg.Pinnum=18;
		PINSEL_ConfigPin(&pincfg);
	  SSP_DeInit(LPC_SSP0);
      break;
    #endif /* SPI_INTERFACE_IN_USE(0) */

    #if SPI_INTERFACE_IN_USE(1)
	case 1:
		pincfg.Portnum=0;
		pincfg.Funcnum=0;
		pincfg.Pinmode=PINSEL_PINMODE_PULLUP;
		pincfg.OpenDrain=PINSEL_PINMODE_NORMAL;
		
		pincfg.Pinnum=6;
		PINSEL_ConfigPin(&pincfg);
		
		//pincfg.Funcnum=2;
		pincfg.Pinnum=7;
		PINSEL_ConfigPin(&pincfg);
		pincfg.Pinnum=8;
		PINSEL_ConfigPin(&pincfg);
		pincfg.Pinnum=9;
		PINSEL_ConfigPin(&pincfg);
		SSP_DeInit(LPC_SSP1);
	break;    
    #endif /* SPI_INTERFACE_IN_USE(1) */

    default:
      ; // Should never happen (protected by reference check above)  
  }

  pSpiDev->status   = SPI_REFERENCE_CLEARED;
  
}


/*----------------------------------------------------------------------------
  SPI lowlevel functions
 *---------------------------------------------------------------------------*/
static void spiWaitForComplete(SPI_INFO *pSpiDev) {
  while ((*pSpiDev->spsr & (1<<7)) == 0);                // wait until transfer complete
}

static void spiSendByte(SPI_INFO *pSpiDev, unsigned char data) {
  *pSpiDev->spdr = data;                                 // save data into data register
}

static unsigned char spiGetReceivedByte(SPI_INFO *pSpiDev) {
  return (unsigned char)*pSpiDev->spdr;                  // return received data byte
}

static unsigned int spicGetStatus(SPI_INFO *pSpiDev) {
  return *pSpiDev->spsr;                                 // return satus byte
}

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/

