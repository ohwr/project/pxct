/*----------------------------------------------------------------------------
 * Name:    ARM_I2C.c
 * Purpose: Template generic / LPC23xx I2C driver
 * Version: V1.00
 * Note(s):
 *----------------------------------------------------------------------------
 * This file is part of the uVision/ARM development tools.
 * This software may only be used under the terms of a valid, current,
 * end user licence from KEIL for a compatible version of KEIL software
 * development tools. Nothing else gives you the right to use this software.
 *
 * This software is supplied "AS IS" without warranties of any kind.
 *
 * Copyright (c) 2008 Keil - An ARM Company. All rights reserved.
 *----------------------------------------------------------------------------
 * History:
 *          V1.00 Initial Version
 *----------------------------------------------------------------------------*/

#include <RTL.h>

#define I2C_INTERFACE_IN_USE(i2cIf) ((I2C_MIN_INTERFACE <= i2cIfNr) && (I2C_MAX_INTERFACE >= i2cIfNr)) 
 

/* Add the correct header file for your selected CPU */ 
 
#include <LPC17xx.H>                             /* LPC23xx definitions */

#include "ARM_I2C.h"
#include "lpc17xx_i2c.h"

/* Set minimum and maximum port numbers available 
   NOTE: Setting this to only the required ports will reduce RAM and code size    */   
#define I2C_MIN_INTERFACE   0
#define I2C_MAX_INTERFACE   2

/*----------------------------------------------------------------------------
  I2C default sttings
 *---------------------------------------------------------------------------*/
#ifndef I2C_CLK
#define I2C_CLK  12000000UL                              // 48MHz / 4
#endif  

#define I2C_CLOCK_RATE                 100               // in kHz
#define I2C_SLAVE_ADDRESS             0x0E //0x50
#define I2C_ADDRESS_SIZE          I2C_7BIT

#define I2C_REFERENCE_CLEARED            0
#define I2C_REFERENCE_CREATED            1


/*----------------------------------------------------------------------------
  structure to administrate I2C
 *---------------------------------------------------------------------------*/
typedef struct {
                    int   reference;                     // = portNumber + 1
           unsigned int   status;                        // opened or closed
                    int   interface;                     // I2c interface
           unsigned int   slaveAddress;                  // slave device address
           unsigned int   addressSize;                   // 7bit or 10bit address   
           unsigned int   clockRate;                     // clock rate in kHz
           
/* Add the correct defines for the I2C registers. Also add 
   any other port specific variables that are required */
 
  volatile unsigned long *i2conset;
  volatile unsigned long *i2stat;
  volatile unsigned long *i2dat;
  volatile unsigned long *i2sclh;
  volatile unsigned long *i2scll;
  volatile unsigned long *i2conclr;
  
} I2C_INFO;

static I2C_INFO i2c_Dev[I2C_MAX_INTERFACE - I2C_MIN_INTERFACE + 1] = {
#if I2C_INTERFACE_IN_USE(0)
  { 1, 
    I2C_REFERENCE_CLEARED, 0, I2C_SLAVE_ADDRESS, I2C_ADDRESS_SIZE, I2C_CLOCK_RATE, 
/* Add the correct register addresses within your first I2C port. Also add 
   any other variable initialisation required */   
    (volatile unsigned long *)(LPC_I2C0_BASE + 0x00),   // I20CONSET
    (volatile unsigned long *)(LPC_I2C0_BASE + 0x04),   // I20STAT
    (volatile unsigned long *)(LPC_I2C0_BASE + 0x08),   // I20DAT
    (volatile unsigned long *)(LPC_I2C0_BASE + 0x10),   // I20SCLH
    (volatile unsigned long *)(LPC_I2C0_BASE + 0x14),   // I20SCLL
    (volatile unsigned long *)(LPC_I2C0_BASE + 0x18)    // I20CONCLR
   
  },
#endif /* I2C_INTERFACE_IN_USE(0) */

#if I2C_INTERFACE_IN_USE(1) 
  { 2, 
    I2C_REFERENCE_CLEARED, 0, I2C_SLAVE_ADDRESS, I2C_ADDRESS_SIZE, I2C_CLOCK_RATE, 
/* Add the correct register addresses within your first I2C port. Also add 
   any other variable initialisation required */
   
    (volatile unsigned long *)(LPC_I2C1_BASE + 0x00),   // I21CONSET
    (volatile unsigned long *)(LPC_I2C1_BASE + 0x04),   // I21STAT
    (volatile unsigned long *)(LPC_I2C1_BASE + 0x08),   // I21DAT
    (volatile unsigned long *)(LPC_I2C1_BASE + 0x10),   // I21SCLH
    (volatile unsigned long *)(LPC_I2C1_BASE + 0x14),   // I21SCLL
    (volatile unsigned long *)(LPC_I2C1_BASE + 0x18)    // I21CONCLR   
    
  },
#endif /* I2C_INTERFACE_IN_USE(1) */

#if I2C_INTERFACE_IN_USE(2) 
  { 3, 
    I2C_REFERENCE_CLEARED, 0, I2C_SLAVE_ADDRESS, I2C_ADDRESS_SIZE, I2C_CLOCK_RATE, 

/* Add the correct register addresses within your first I2C port. Also add 
   any other variable initialisation required */
  
    (volatile unsigned long *)(LPC_I2C2_BASE + 0x00),   // I22CONSET
    (volatile unsigned long *)(LPC_I2C2_BASE + 0x04),   // I22STAT
    (volatile unsigned long *)(LPC_I2C2_BASE + 0x08),   // I22DAT
    (volatile unsigned long *)(LPC_I2C2_BASE + 0x10),   // I22SCLH
    (volatile unsigned long *)(LPC_I2C2_BASE + 0x14),   // I22SCLL
    (volatile unsigned long *)(LPC_I2C2_BASE + 0x18)    // I22CONCLR  
  },
#endif /* I2C_INTERFACE_IN_USE(2) */

/* USER CODE =====================================================================*/
/* Add / remove code for any extra port initialisation */   
/* END USER CODE ================================================================ */  
};



/*----------------------------------------------------------------------------
  I2C Bit definitions
 *---------------------------------------------------------------------------*/
#define I2C_AA      0x00000004
#define I2C_SI      0x00000008
#define I2C_STO     0x00000010
#define I2C_STA     0x00000020
#define I2C_I2EN    0x00000040

/*----------------------------------------------------------------------------
  I2C Status register definitions
 *---------------------------------------------------------------------------*/
/* Master */
#define I2CSR_START              0x08
#define I2CSR_REP_START          0x10
/* Master Transmitter */
#define I2CSR_MT_SLA_ACK         0x18
#define I2CSR_MT_SLA_NACK        0x20
#define I2CSR_MT_DATA_ACK        0x28
#define I2CSR_MT_DATA_NACK       0x30
#define I2CSR_MT_ARB_LOST        0x38
/* Master Receiver */
#define I2CSR_MR_ARB_LOST        0x38
#define I2CSR_MR_SLA_ACK         0x40
#define I2CSR_MR_SLA_NACK        0x48
#define I2CSR_MR_DATA_ACK        0x50
#define I2CSR_MR_DATA_NACK       0x58

#define I2CSR_NO_INFO            0xF8

// return values
#define I2C_ERROR_NODEV          0x01


/*----------------------------------------------------------------------------
  Function Prototypes
 *---------------------------------------------------------------------------*/
static void          i2cSendStart      (I2C_INFO *pI2cDev);
static void          i2cSendStop       (I2C_INFO *pI2cDev);
static void          i2cWaitForComplete(I2C_INFO *pI2cDev);
static void          i2cSendByte       (I2C_INFO *pI2cDev, unsigned char data);
static void          i2cReceiveByte    (I2C_INFO *pI2cDev, unsigned char ackFlag);
static unsigned char i2cGetReceivedByte(I2C_INFO *pI2cDev);
static unsigned char i2cGetStatus      (I2C_INFO *pI2cDev);
static void          i2cDelay          (unsigned int);          // delay in ms


/*----------------------------------------------------------------------------
  I2C create a reference
 *---------------------------------------------------------------------------*/
void ARM_I2C_CreateConfigurationReference (
                         int    interface,
                unsigned int    slaveAddress,
                unsigned int    addressSize,   
                unsigned int    clockRate,
                         int*  pReferenceOut,
                         int*  pErrOut) {

  I2C_INFO      *pI2cDev;
  unsigned short scl;
  LPC_I2C_TypeDef* I2Cx;

  if ( (interface < I2C_MIN_INTERFACE) ||                // check the I2C interface
       (interface > I2C_MAX_INTERFACE)   ) {
    *pErrOut = I2C_INTERFACE_NOTKNOWN_ERR;
    return;
  }

  if ( (addressSize != I2C_7BIT ) &&
       (addressSize != I2C_10BIT)   ){                   // check the I2C address size
    *pErrOut = I2C_ADDRESSSIZE_NOTSUPPORTED_ERR;
    return;
  }

  *pErrOut = I2C_OK;
  pI2cDev = &i2c_Dev[(interface - I2C_MIN_INTERFACE)];

  switch (interface) {
    #if I2C_INTERFACE_IN_USE(0)
    case 0:
      /* Add code to initialise the pinout hardware for I2C 0 */ 
      

	  PINSEL_ConfigPin(0,27,1);
	  PINSEL_ConfigPin(0,28,1);
	  I2Cx = LPC_I2C0;
      break;
    #endif /* I2C_INTERFACE_IN_USE(0) */
    
    #if I2C_INTERFACE_IN_USE(1) 
    case 1:
      /* Add code to initialise the pinout hardware for I2C 0 */


	  PINSEL_ConfigPin(0,0,3);
	  PINSEL_ConfigPin(0,1,3);
	  I2Cx = LPC_I2C1;
      break;
    #endif /* I2C_INTERFACE_IN_USE(1) */

    #if I2C_INTERFACE_IN_USE(2) 
    case 2:
      /* Add code to initialise the pinout hardware for I2C 2 */

	  PINSEL_ConfigPin(0,10,2);
	  PINSEL_ConfigPin(0,11,2);
	  I2Cx = LPC_I2C2;
      break;
    #endif /* I2C_INTERFACE_IN_USE(1) */

    default:
      ; // Should never happen (protected by port check above)
  };

  	I2C_Init(I2Cx,(clockRate*1000));
	I2C_Cmd(I2Cx,ENABLE);

  pI2cDev->status        = I2C_REFERENCE_CREATED;
  pI2cDev->interface     = interface;
  pI2cDev->slaveAddress  = slaveAddress & 0x3FF;         // maximum is 10bit
  pI2cDev->addressSize   = addressSize;   
  pI2cDev->clockRate     = (clockRate*1000);   

  *pReferenceOut = pI2cDev->reference;
    /*
  i2c1 : P0.0 en P0.1
  i2c2 : P0.10 en P0.11
  i2c1 : P0.19 en P0.20
  i2c0 : P.0.27 en P0.28
  i2c2 : P1.15 en P1.16
  i2c0 : P1.30 en P1.31
  i2c1 : P2.14 en P2.15
  i2c2 : P2.30 en P2.31
  i2c2 : P4.20 en P4.21
  i2c2 : P4.29 en ?

  i2c0 : P5.2 en P5.3

  */
}

/*----------------------------------------------------------------------------
  I2C perform a write/read access
 *---------------------------------------------------------------------------*/
void ARM_I2C_WriteRead (
                         int   nReferenceIn,
                    T_bArray*  pDataWr,
                unsigned int   bytesToRead,
                    T_bArray*  pDataRd,
                         int*  pErrOut) {

  I2C_INFO     *pI2cDev;
  unsigned char i2cSlaveAdrBH, i2cSlaveAdrBL;            // High / Low part of slave address

  unsigned int i     = 0;
  unsigned int wrLen = pDataWr->len;
  unsigned int rdLen = bytesToRead;

  I2C_M_SETUP_Type transfercfg;
  


  uint8_t txbuf[20],rxbuf[20];
  for(i=0;i<wrLen;i++)
  {
  	txbuf[i]=pDataWr->data[i];
  }

  nReferenceIn -= 1;                                     // reference is starting from 1

  if ( (nReferenceIn < (I2C_MIN_INTERFACE)) ||           // check the I2C reference
       (nReferenceIn > (I2C_MAX_INTERFACE))   ) {
    *pErrOut = I2C_REFERENCE_NOTKNOWN_ERR;
    return;
  }

  *pErrOut = I2C_OK;
  pI2cDev = &i2c_Dev[(nReferenceIn - I2C_MIN_INTERFACE)];

  if (pI2cDev->status != I2C_REFERENCE_CREATED) {
    *pErrOut = I2C_REFERENCE_NOTCREATED_ERR;
    return;
  }


  	if(wrLen)
  	{ 
  		transfercfg.sl_addr7bit=pI2cDev->slaveAddress;
  		transfercfg.tx_length=wrLen;
  		transfercfg.rx_length=0;
  		transfercfg.tx_data=&txbuf[0];//(uint8_t*)pDataWr->data[0];//
  		transfercfg.rx_data=NULL;
  		transfercfg.retransmissions_max=3;
  		I2C_MasterTransferData(LPC_I2C0,&transfercfg,I2C_TRANSFER_POLLING);
  	}


  	if(bytesToRead)
  	{
		transfercfg.sl_addr7bit=pI2cDev->slaveAddress;
		transfercfg.retransmissions_max=3;
		transfercfg.tx_data=NULL;
  		transfercfg.tx_length=0;
  		transfercfg.rx_data=&rxbuf[0];//(uint8_t*)pDataRd->data[0];//
		transfercfg.rx_length=bytesToRead; 
  		I2C_MasterTransferData(LPC_I2C0,&transfercfg,I2C_TRANSFER_POLLING);
		pDataRd->len=bytesToRead;
		
		for(i=0;i<bytesToRead;i++)
		{
			pDataRd->data[i]=rxbuf[i];
		}
	}		
}


/*----------------------------------------------------------------------------
  I2C close a  reference
 *---------------------------------------------------------------------------*/
void ARM_I2C_CloseReference (
                         int   nReferenceIn,
                         int*  pErrOut) {
  I2C_INFO *pI2cDev;
  LPC_I2C_TypeDef* I2Cx;
  nReferenceIn -= 1;                                     // reference is starting from 1

  if ( (nReferenceIn < (I2C_MIN_INTERFACE)) ||           // check the I2C reference
       (nReferenceIn > (I2C_MAX_INTERFACE))   ) {
    *pErrOut = I2C_REFERENCE_NOTKNOWN_ERR;
    return;
  }

  *pErrOut = I2C_OK;
  pI2cDev = &i2c_Dev[(nReferenceIn - I2C_MIN_INTERFACE)];

  switch (pI2cDev->reference) {
    #if I2C_INTERFACE_IN_USE(0)
    case 0:
      /* Uninitialise the pinout hardware for UART 0 */    
      PINSEL_ConfigPin(0,27,0);
	  PINSEL_ConfigPin(0,28,0);
	  I2Cx=LPC_I2C0;	  
      break;
    #endif /* I2C_INTERFACE_IN_USE(0) */
    
    #if I2C_INTERFACE_IN_USE(1) 
    case 1:
      /* USER CODE =====================================================================*/
      /* Uninitialise the pinout hardware for UART 0 */    
      PINSEL_ConfigPin(0,0,0);
	  PINSEL_ConfigPin(0,1,0);
	  I2Cx=LPC_I2C0;

      break;
    #endif /* I2C_INTERFACE_IN_USE(1) */

    #if I2C_INTERFACE_IN_USE(2) 
    case 2:
      /* USER CODE =====================================================================*/
      /* Uninitialise the pinout hardware for UART 0 */
	  PINSEL_ConfigPin(0,10,1);
	  PINSEL_ConfigPin(0,11,1);
	  I2Cx=LPC_I2C0;
      break;
    #endif /* I2C_INTERFACE_IN_USE(2) */

    default:
      ; // Should never happen (protected by reference check above)  
  }     
  I2C_DeInit(I2Cx);
  pI2cDev->status = I2C_REFERENCE_CLEARED;
  
}


/*----------------------------------------------------------------------------
  I2C lowlevel functions
 *---------------------------------------------------------------------------*/
static void i2cSendStart(I2C_INFO *pI2cDev) {
  *pI2cDev->i2conclr = I2C_SI;                           // clear SI bit to begin transfer
  *pI2cDev->i2conset = I2C_STA;                          // transmit start condition
}

static void i2cSendStop(I2C_INFO *pI2cDev) {
  *pI2cDev->i2conset = I2C_STO;                          // transmit stop condition
  *pI2cDev->i2conclr = I2C_SI;                           // clear SI bit to begin transfer
}

static void i2cWaitForComplete(I2C_INFO *pI2cDev) {
  while (!(*pI2cDev->i2conset & I2C_SI));
}

static void i2cSendByte(I2C_INFO *pI2cDev, unsigned char data) {
  *pI2cDev->i2dat    = data;                             // save data into data register
  *pI2cDev->i2conclr = I2C_SI;                           // clear SI bit to begin transfer
}

static void i2cReceiveByte(I2C_INFO *pI2cDev, unsigned char ackFlag) {
  if (ackFlag) {                                         // begin receive over i2c
    *pI2cDev->i2conset = I2C_AA;                         // ackFlag = TRUE: ACK the recevied data
  }
  else {
    *pI2cDev->i2conclr = I2C_AA;                         // ackFlag = FALSE: NACK the recevied data
  }
  *pI2cDev->i2conclr = I2C_SI;                           // clear SI bit to begin transfer
}

static unsigned char i2cGetReceivedByte(I2C_INFO *pI2cDev) {
  return *pI2cDev->i2dat;                                // return received data byte
}

static unsigned char i2cGetStatus(I2C_INFO *pI2cDev) {
  return *pI2cDev->i2stat;                               // return satus byte
}

static void i2cDelay (unsigned int ms) {
  os_dly_wait (ms);                                      // delay in msec
}

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/







