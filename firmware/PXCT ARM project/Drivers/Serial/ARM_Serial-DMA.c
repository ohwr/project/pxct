/*----------------------------------------------------------------------------
 *      Name:    ARM_Serial.c
 *      Purpose: Template generic / LPC23xx UART ('serial') driver for LabVIEW
 *      Version: v1.00
 *      Note(s):
 *----------------------------------------------------------------------------
 *      This file is part of the uVision/ARM development tools.
 *      This software may only be used under the terms of a valid, current,
 *      end user licence from KEIL for a compatible version of KEIL software
 *      development tools. Nothing else gives you the right to use it.
 *
 *      Copyright (c) 2007 Keil Software.
 *---------------------------------------------------------------------------*/
 
#define SERIAL_PORT_IN_USE(port)    ((SER_MINPORT <= port) && (SER_MAXPORT >= port)) 
 
 
/* Add the correct header file for your selected CPU */ 

#include <LPC17xx.H>                      // LPC1788 definitions


#include "LVConfig.h"
#include "ARM_Serial.h"

#include "core_cm3.h"
#include "lpc17xx_uart.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpdma.h"

/* Set minimum and maximum port numbers available 
   NOTE: Setting this to only the required ports will reduce RAM and code size    */   

#define SER_MINPORT   0
#define SER_MAXPORT   1


#define SER_IN_BUF_SIZE   64              /* Input buffer in bytes (power 2) */
#define SER_OUT_BUF_SIZE  0              /* Output buffer in bytes (power 2) */

#define SER_PORTCLOSED    0
#define SER_PORTOPENED    1

#define SER_OK                  1
#define SER_GENERAL_ERR         0
#define SER_PORTNOTKNOWN_ERR  (-1)
#define SER_PORTNOTOPENED_ERR (-1)
#define SER_DEVNOTKNOWN_ERR   (-1)

/* Buffer masks */
#define SER_IN_MASK           (SER_IN_BUF_SIZE-1ul)
#define SER_OUT_MASK          (SER_OUT_BUF_SIZE-1ul)

/* Buffer read / write macros */
#define IN_BUF_RESET(serDev)        (serDev->ser_in.rd_idx = serDev->ser_in.wr_idx = 0)
#define IN_BUF_WR(serDev, dataIn)   (serDev->ser_in.data[SER_IN_MASK & serDev->ser_in.wr_idx++] = (dataIn))
#define IN_BUF_RD(serDev)           (serDev->ser_in.data[SER_IN_MASK & serDev->ser_in.rd_idx++])   
#define IN_BUF_EMPTY(serDev)        (serDev->ser_in.rd_idx == serDev->ser_in.wr_idx)
#define IN_BUF_FULL(serDev)         (serDev->ser_in.rd_idx == serDev->ser_in.wr_idx+1)
#define IN_BUF_COUNT(serDev)        (SER_IN_MASK & (serDev->ser_in.wr_idx - serDev->ser_in.rd_idx))

char outbuffer[256];
int outlength;

U32 Channel3_TC=1;
U32 Channel3_ERROR=1;

// Serial input buffer
typedef struct __ser_in_t {
  volatile unsigned char data[SER_IN_BUF_SIZE];
  volatile unsigned int wr_idx;
  volatile unsigned int rd_idx;
} ser_in_t;

#define UARTNormal(UARTx,buffer,bytesToWrite) UART_Send(UARTx, (uint8_t*)buffer , bytesToWrite, BLOCKING)
#define UARTDMA(UARTx,buffer,bytesToWrite) UART_DMA_SEND(UARTx,(uint8_t*)buffer,bytesToWrite)

#ifndef UART0DMA
	#define UART0Func(buffer,bytesToWrite) UARTNormal(LPC_UART0,buffer,bytesToWrite)
#else
	#if UART0DMA
		#define UART0Func(buffer,bytesToWrite) UART_DMA_SEND(0,buffer,bytesToWrite)	
	#else
		#define UART0Func(buffer,bytesToWrite) UARTNormal(LPC_UART0,buffer,bytesToWrite)
	#endif
#endif

#ifndef UART1DMA
	#define UART1Func(buffer,bytesToWrite) UARTNormal((LPC_UART_TypeDef*)LPC_UART1,buffer,bytesToWrite)
#else
	#if UART1DMA
		#define UART1Func(buffer,bytesToWrite) UART_DMA_SEND(1,buffer,bytesToWrite)	
	#else
		#define UART1Func(buffer,bytesToWrite) UARTNormal((LPC_UART_TypeDef*)LPC_UART1,buffer,bytesToWrite)
	#endif
#endif

#ifndef UART2DMA
	#define UART2Func(buffer,bytesToWrite) UARTNormal(LPC_UART2,buffer,bytesToWrite)
#else
	#if UART2DMA
		#define UART2Func(buffer,bytesToWrite) UART_DMA_SEND(2,buffer,bytesToWrite)	
	#else
		#define UART2Func(buffer,bytesToWrite) UARTNormal(LPC_UART2,buffer,bytesToWrite)
	#endif
#endif



/*----------------------------------------------------------------------------
  structure to administrate the UART
 *---------------------------------------------------------------------------*/
typedef __irq void (*ser_irq_fp)(void);           // RV irq function pointer typedef

#if SERIAL_PORT_IN_USE(0)
static __irq void ser_irq_0 (void);               // RV irq function prototype for port 0
#endif /* SERIAL_PORT_IN_USE(0) */

#if SERIAL_PORT_IN_USE(1)
static __irq void ser_irq_1 (void);               // RV irq function prototype for port 1
#endif /* SERIAL_PORT_IN_USE(1) */

#if SERIAL_PORT_IN_USE(2)
static __irq void ser_irq_2 (void);               // RV irq function prototype for port 2
#endif /* SERIAL_PORT_IN_USE(2) */

#if SERIAL_PORT_IN_USE(3)
static __irq void ser_irq_3 (void);               // RV irq function prototype for port 3
#endif /* SERIAL_PORT_IN_USE(3) */

typedef struct {
           int   serialDeviceNumber;       // = portNumber + 1
           unsigned int   status;
           unsigned long  baudRate;
           unsigned int   dataBits;
           unsigned int   stopBits;
           unsigned int   parity;
           
/* Add the correct defines for the registers within your UART. Also add 
   any other port specific variables that are required */
  
  volatile unsigned long *rbr;
  volatile unsigned long *thr;
  volatile unsigned long *dll;
  volatile unsigned long *dlm;
  volatile unsigned long *lcr;
  volatile unsigned long *lsr;
  volatile unsigned long *fdr;
  volatile unsigned long *fcr;
  volatile unsigned long *iir;
  volatile unsigned long *ier;
  volatile unsigned long *irqAddr;
  volatile unsigned long *irqCntl;  
  unsigned long          ser_irq_mask;              // Serial IRQ enable / disable bit mask
  unsigned long          txFifoEmpty      :1;       // 0x000(0000000X) Indicates transmit fifo is empty and needs to be kickstarted
  unsigned long          unused_flags     :31;      // 0xXXX(XXXXXXX0) Unused 
  
  ser_irq_fp             ser_irq;                   // Serial IRQ function
  ser_in_t               ser_in;
} SER_INFO;

static SER_INFO ser_Dev[SER_MAXPORT - SER_MINPORT + 1] = {
#if SERIAL_PORT_IN_USE(0)
  { 1, 
    SER_PORTCLOSED, 9600, 8, 0, 0, 

/* Add the correct register addresses within your first UART. Also add 
   any other variable initialisation required */
   
    (volatile unsigned long *)(LPC_UART0_BASE + 0x00),   /* RBR */
    (volatile unsigned long *)(LPC_UART0_BASE + 0x00),   /* THR */
    (volatile unsigned long *)(LPC_UART0_BASE + 0x00),   /* DLL */
    (volatile unsigned long *)(LPC_UART0_BASE + 0x04),   /* DLM */
    (volatile unsigned long *)(LPC_UART0_BASE + 0x0C),   /* LCR */
    (volatile unsigned long *)(LPC_UART0_BASE + 0x14),   /* LSR */
    (volatile unsigned long *)(LPC_UART0_BASE + 0x28),   /* FDR */
    (volatile unsigned long *)(LPC_UART0_BASE + 0x08),   /* FCR */
    (volatile unsigned long *)(LPC_UART0_BASE + 0x08),   /* IIR */
    (volatile unsigned long *)(LPC_UART0_BASE + 0x04),   /* IER */
    (volatile unsigned long *)(0x10000000  + 0x54),    /* IRQ Addr */
    (volatile unsigned long *)(NVIC + 0x218),    /* IRQ Cntl */
    UART0_IRQn,                                           /* IRQ mask bit */
    1,                                                    /* txFifoEmpty */
    0,                                                    /* unused_flags */
    
    ser_irq_0                                             /* IRQ function */
  },
#endif /* SERIAL_PORT_IN_USE(0) */

#if SERIAL_PORT_IN_USE(1) 
  { 2, 
    SER_PORTCLOSED, 9600, 8, 0, 0, 
    
/* Add the correct register addresses within your first UART. Also add 
   any other variable initialisation required */
  
    (volatile unsigned long *)(LPC_UART1_BASE + 0x00),   /* RBR */
    (volatile unsigned long *)(LPC_UART1_BASE + 0x00),   /* THR */
    (volatile unsigned long *)(LPC_UART1_BASE + 0x00),   /* DLL */
    (volatile unsigned long *)(LPC_UART1_BASE + 0x04),   /* DLM */
    (volatile unsigned long *)(LPC_UART1_BASE + 0x0C),   /* LCR */
    (volatile unsigned long *)(LPC_UART1_BASE + 0x14),   /* LSR */
    (volatile unsigned long *)(LPC_UART1_BASE + 0x28),   /* FDR */
    (volatile unsigned long *)(LPC_UART1_BASE + 0x08),   /* FCR */
    (volatile unsigned long *)(LPC_UART1_BASE + 0x08),   /* IIR */
    (volatile unsigned long *)(LPC_UART1_BASE + 0x04),   /* IER */
    (volatile unsigned long *)(0x10000000  + 0x58),    /* IRQ Addr */
    (volatile unsigned long *)(NVIC + 0x21C),    /* IRQ Cntl */
    UART1_IRQn,                                           /* IRQ mask bit */
    1,                                                    /* txFifoEmpty */
    0,                                                    /* unused_flags */
   
    ser_irq_1                                             /* IRQ function */    
  },
#endif /* SERIAL_PORT_IN_USE(1) */

#if SERIAL_PORT_IN_USE(2) 
  { 3, 
    SER_PORTCLOSED, 9600, 8, 0, 0, 
    
/* Add the correct register addresses within your first UART. Also add 
   any other variable initialisation required */

    (volatile unsigned long *)(LPC_UART2_BASE + 0x00),   /* RBR */
    (volatile unsigned long *)(LPC_UART2_BASE + 0x00),   /* THR */
    (volatile unsigned long *)(LPC_UART2_BASE + 0x00),   /* DLL */
    (volatile unsigned long *)(LPC_UART2_BASE + 0x04),   /* DLM */
    (volatile unsigned long *)(LPC_UART2_BASE + 0x0C),   /* LCR */
    (volatile unsigned long *)(LPC_UART2_BASE + 0x14),   /* LSR */
    (volatile unsigned long *)(LPC_UART2_BASE + 0x28),   /* FDR */
    (volatile unsigned long *)(LPC_UART2_BASE + 0x08),   /* FCR */
    (volatile unsigned long *)(LPC_UART2_BASE + 0x08),   /* IIR */
    (volatile unsigned long *)(LPC_UART2_BASE + 0x04),   /* IER */
    (volatile unsigned long *)(0x10000000  + 0x5C),    /* IRQ Addr */
    (volatile unsigned long *)(NVIC + 0x270),    /* IRQ Cntl */
    UART2_IRQn,                                           /* IRQ mask bit */
    1,                                                    /* txFifoEmpty */
    0,                                                    /* unused_flags */
    
    ser_irq_2                                             /* IRQ function */    
  },
#endif /* SERIAL_PORT_IN_USE(2) */

#if SERIAL_PORT_IN_USE(3)  
  { 4, 
    SER_PORTCLOSED, 9600, 8, 0, 0, 
    

/* Add the correct register addresses within your first UART. Also add 
   any other variable initialisation required */
   
    (volatile unsigned long *)(UART3_BASE_ADDR + 0x00),   /* RBR */
    (volatile unsigned long *)(UART3_BASE_ADDR + 0x00),   /* THR */
    (volatile unsigned long *)(UART3_BASE_ADDR + 0x00),   /* DLL */
    (volatile unsigned long *)(UART3_BASE_ADDR + 0x04),   /* DLM */
    (volatile unsigned long *)(UART3_BASE_ADDR + 0x0C),   /* LCR */
    (volatile unsigned long *)(UART3_BASE_ADDR + 0x14),   /* LSR */
    (volatile unsigned long *)(UART3_BASE_ADDR + 0x28),   /* FDR */
    (volatile unsigned long *)(UART3_BASE_ADDR + 0x08),   /* FCR */
    (volatile unsigned long *)(UART3_BASE_ADDR + 0x08),   /* IIR */
    (volatile unsigned long *)(UART3_BASE_ADDR + 0x04),   /* IER */
    (volatile unsigned long *)(VIC_BASE_ADDR + 0x174),    /* IRQ Addr */
    (volatile unsigned long *)(VIC_BASE_ADDR + 0x274),    /* IRQ Cntl */
    0x20000000,                                           /* IRQ mask bit */
    1,                                                    /* txFifoEmpty */
    0,                                                    /* unused_flags */
    
    ser_irq_3                                             /* IRQ function */    
  } 
#endif /* SERIAL_PORT_IN_USE(3) */
 
};



/*----------------------------------------------------------------------------
  open the UART
 *---------------------------------------------------------------------------*/
int ARM_Serial_OpenPort (int port, int *serDevNo) {
	PINSEL_CFG_Type pincfg;

  if ( (port < SER_MINPORT) ||                      /* check the port number */
       (port > SER_MAXPORT)   ) {
    return (SER_PORTNOTKNOWN_ERR);
  }

  switch (port) {
    #if SERIAL_PORT_IN_USE(0)
    case 0:
      /* Add code to initialise the pinout hardware for UART 0 */
	  	pincfg.Portnum=0;
		pincfg.Funcnum=1;
		pincfg.Pinnum=2;
		PINSEL_ConfigPin(&pincfg);	  
	    pincfg.Pinnum=3;
		PINSEL_ConfigPin(&pincfg);
    #endif
    #if SERIAL_PORT_IN_USE(1)
    case 1:
      /* Add code to initialise the pinout hardware for UART 1 */
	  	pincfg.Portnum=2;
		pincfg.Funcnum=2;
		pincfg.Pinnum=0;
		PINSEL_ConfigPin(&pincfg);	  
	    pincfg.Pinnum=1;
		PINSEL_ConfigPin(&pincfg);

	    pincfg.Pinnum=2;
		PINSEL_ConfigPin(&pincfg);
	    pincfg.Pinnum=3;
		PINSEL_ConfigPin(&pincfg);
	    pincfg.Pinnum=4;
		PINSEL_ConfigPin(&pincfg);
	    pincfg.Pinnum=5;
		PINSEL_ConfigPin(&pincfg);
	    pincfg.Pinnum=6;
		PINSEL_ConfigPin(&pincfg);
	    pincfg.Pinnum=7;
		PINSEL_ConfigPin(&pincfg);


   	#endif
    #if SERIAL_PORT_IN_USE(2)      
    case 2:
      /* Add code to initialise the pinout hardware for UART 2 */
	  	pincfg.Portnum=0;
		pincfg.Funcnum=1;
		pincfg.Pinnum=10;
		PINSEL_ConfigPin(&pincfg);	  
	    pincfg.Pinnum=11;
		PINSEL_ConfigPin(&pincfg);
    #endif
    #if SERIAL_PORT_IN_USE(3)
    case 3:
      /* Add code to initialise the pinout hardware for UART 3 */

    #endif  
    default:
      ; // Should never happen (protected by port check above)
  };

  ser_Dev[port - SER_MINPORT].status = SER_PORTOPENED;
  *serDevNo = ser_Dev[port - SER_MINPORT].serialDeviceNumber;

  return (SER_OK);
}

/*----------------------------------------------------------------------------
  close the UART
 *---------------------------------------------------------------------------*/
int ARM_Serial_ClosePort (int serDevNo) {
  SER_INFO *pSerDev;
  PINSEL_CFG_Type pincfg;
  LPC_UART_TypeDef* UARTx;

  serDevNo -= 1;                                  /* Convert the serDevNo to the port number */
  if ( (serDevNo < (SER_MINPORT)) ||              /* check the serial device number */
       (serDevNo > (SER_MAXPORT))   ) {
    return (SER_DEVNOTKNOWN_ERR);
  }

  pSerDev = &ser_Dev[(serDevNo - SER_MINPORT)];

  //while(!pSerDev->txFifoEmpty) 
  //  os_dly_wait(10);                  /* wait for fifo to empty before closing port */

	  /* Disable the interrupt in the VIC and UART controllers */
	NVIC_DisableIRQ(pSerDev->ser_irq_mask);			/* Disable VIC interrupt     */
  UART_DeInit(UARTx);
  UART_TxCmd(UARTx,DISABLE);       
  //*(pSerDev->ier) = 0x00;                           /* Disable UART interrupts   */       
  



 
  switch (serDevNo - SER_MINPORT) {
    #if SERIAL_PORT_IN_USE(0)
    case 0:
      /* Uninitialise the pinout hardware for UART 0 */
	  	pincfg.Portnum=0;
		pincfg.Funcnum=0;
		pincfg.Pinnum=2;
		PINSEL_ConfigPin(&pincfg);	  
	    pincfg.Pinnum=3;
		PINSEL_ConfigPin(&pincfg);
		UARTx=LPC_UART0;
    #endif
    #if SERIAL_PORT_IN_USE(1)
    case 1:
      /* Uninitialise the pinout hardware for UART 1 */	
   UART_FullModemConfigMode(LPC_UART1,UART1_MODEM_MODE_AUTO_RTS,DISABLE);
   UART_FullModemConfigMode(LPC_UART1,UART1_MODEM_MODE_AUTO_CTS,DISABLE);
   UART_IntConfig((LPC_UART_TypeDef *)LPC_UART1, UART_INTCFG_RLS, DISABLE);

	  	pincfg.Portnum=2;
		pincfg.Funcnum=0;
		pincfg.Pinnum=0;
		PINSEL_ConfigPin(&pincfg);	  
	    pincfg.Pinnum=1;
		PINSEL_ConfigPin(&pincfg);

	    pincfg.Pinnum=2;
		PINSEL_ConfigPin(&pincfg);
	    pincfg.Pinnum=3;
		PINSEL_ConfigPin(&pincfg);
	    pincfg.Pinnum=4;
		PINSEL_ConfigPin(&pincfg);
	    pincfg.Pinnum=5;
		PINSEL_ConfigPin(&pincfg);
	    pincfg.Pinnum=6;
		PINSEL_ConfigPin(&pincfg);
	    pincfg.Pinnum=7;
		PINSEL_ConfigPin(&pincfg);
		UARTx=(LPC_UART_TypeDef*)LPC_UART1;




    #endif
    #if SERIAL_PORT_IN_USE(2)
    case 2:
      /* Uninitialise the pinout hardware for UART 2 */
	  	pincfg.Portnum=0;
		pincfg.Funcnum=0;
		pincfg.Pinnum=10;
		PINSEL_ConfigPin(&pincfg);	  
	    pincfg.Pinnum=11;
		PINSEL_ConfigPin(&pincfg);
		UARTx=LPC_UART2;
    #endif
    #if SERIAL_PORT_IN_USE(3)
    case 3:
      /* Uninitialise the pinout hardware for UART 3 */

    
      break;
    #endif
    
    default:
      ; // Should never happen (protected by port check above)  
  }



  pSerDev->status   = SER_PORTCLOSED;
  
  return (SER_OK);
}

/*----------------------------------------------------------------------------
  initialize the UART
 *---------------------------------------------------------------------------*/
int ARM_Serial_InitPort (int serDevNo, unsigned long baudrate, unsigned int  databits,
                                unsigned int  parity,   unsigned int  stopbits) {

  unsigned char lcr_p, lcr_s, lcr_d;
  unsigned long dll;
  SER_INFO *pSerDev;
  LPC_UART_TypeDef* UARTx;
  UART_CFG_Type UARTxConfig;

  serDevNo -= 1;                         /* Convert the serDevNo to the port number */
  if ( (serDevNo < (SER_MINPORT)) ||     /* check the serial device number */
       (serDevNo > (SER_MAXPORT))   ) {
    return (SER_DEVNOTKNOWN_ERR);
  }

  pSerDev = &ser_Dev[(serDevNo - SER_MINPORT)];

  switch(serDevNo)
  {
   case 1:
   UARTx=(LPC_UART_TypeDef*)LPC_UART1;
   break;
   case 2:
   UARTx=LPC_UART2;
   break;
   case 0:
   default:
   UARTx=LPC_UART0;
   break;
  }
  UARTxConfig.Baud_rate=baudrate;
  UARTxConfig.Databits=databits-5;


  switch (stopbits) {
    case 1:                              /* 1,5 Stop bits */
    case 2:                              /* 2   Stop bits */
	  UARTxConfig.Stopbits=UART_STOPBIT_2;
    break;
    case 0:                              /* 1   Stop bit  */
    default:
	  UARTxConfig.Stopbits=UART_STOPBIT_1;
    break;
  }

  switch (parity) {
    case 1:                              /* Parity Odd    */
	  UARTxConfig.Parity=UART_PARITY_ODD;
    break;
    case 2:                              /* Parity Even   */
	  UARTxConfig.Parity=UART_PARITY_EVEN;
    break;
    case 3:                              /* Parity Mark   */
    break;
    case 4:                              /* Parity Space  */
    break;
    case 0:                              /* Parity None   */
    default:
	  UARTxConfig.Parity=UART_PARITY_NONE;
    break;
  }



  pSerDev->baudRate = baudrate;
  pSerDev->dataBits = databits;
  pSerDev->stopBits = stopbits;
  pSerDev->parity   = parity;
  
  IN_BUF_RESET(pSerDev);

  UART_Init(UARTx,&UARTxConfig);
  UART_TxCmd(UARTx,ENABLE);
  

  /* Set up and enable the UART interrupt in the interrupt controller */
 
  *(pSerDev->irqAddr) = (unsigned long)pSerDev->ser_irq;  /* Set interrupt function           */
                            
  NVIC_SetPriority(pSerDev->ser_irq_mask,0x0F);	 			/* Set interrupt priority           */
  NVIC_EnableIRQ(pSerDev->ser_irq_mask);                  	/* Enable interrupt */ 

  switch(serDevNo)
  {
   case 1:
   //UART_FullModemConfigMode(LPC_UART1,UART1_MODEM_MODE_AUTO_RTS,ENABLE);
   //UART_FullModemConfigMode(LPC_UART1,UART1_MODEM_MODE_AUTO_CTS,ENABLE);
   UART_IntConfig((LPC_UART_TypeDef *)LPC_UART1, UART_INTCFG_RLS, ENABLE);
   break;
   default:
   break;
   }

  UART_IntConfig(UARTx,UART_INTCFG_RBR,ENABLE);
 
  GPDMA_Init();
  
  return (SER_OK);
}

/*----------------------------------------------------------------------------
  read data from UART
 *---------------------------------------------------------------------------*/
int ARM_Serial_Read (int serDevNo, char *buffer, const int *length) {
  SER_INFO *pSerDev;
  int bytesToRead;
  
  serDevNo -= 1;                         /* Convert the serDevNo to the port number */
  if ( (serDevNo < (SER_MINPORT)) ||     /* check the serial device number */
       (serDevNo > (SER_MAXPORT))   ) {
    return (SER_DEVNOTKNOWN_ERR);
  }
  pSerDev = &ser_Dev[(serDevNo - SER_MINPORT)];

  bytesToRead = *length;
  while (bytesToRead--) {
    while (IN_BUF_EMPTY(pSerDev));       /* Block until data is available if none */
    *buffer++ = IN_BUF_RD(pSerDev);
  }
  return (*length);  
}

/*----------------------------------------------------------------------------
  write data to the UART
 *---------------------------------------------------------------------------*/
int ARM_Serial_Write (int serDevNo, const char *buffer, int *length) {
  SER_INFO *pSerDev;
  int i, bytesToWrite, bytesWritten;
  LPC_UART_TypeDef* UARTx;
  GPDMA_Channel_CFG_Type GPDMACfg;
  U8 destination;
  
  serDevNo -= 1;                         /* Convert the serDevNo to the port number */
  if ( (serDevNo < (SER_MINPORT)) ||     /* check the serial device number */
       (serDevNo > (SER_MAXPORT))   ) {
    return (SER_DEVNOTKNOWN_ERR);
  }
  pSerDev = &ser_Dev[(serDevNo - SER_MINPORT)];

  // Write *length bytes
  bytesToWrite = *length;
  bytesWritten = bytesToWrite;

  /* Write the data to the transmit section of the UART hardware */
  
  /* Lets just use the 16 byte hardware FIFO to buffer outgoing data */

   switch(serDevNo)
  {
   case 1:
   UARTx=(LPC_UART_TypeDef*)LPC_UART1;
   destination = GPDMA_CONN_UART1_Tx;
   UART1Func((uint8_t*)&buffer[0],bytesToWrite);
   break;
   case 2:
   UARTx=LPC_UART2;
   destination = GPDMA_CONN_UART2_Tx;
   UART2Func((uint8_t*)&buffer[0],bytesToWrite);
   break;
   case 0:
   default:
   UARTx=LPC_UART0;
   destination = GPDMA_CONN_UART0_Tx;
	UART0Func((uint8_t*)&buffer[0],bytesToWrite);


   break;
  }

	//UART_Send(UARTx, (uint8_t*)buffer , bytesToWrite, BLOCKING);
	/*
	// Wait for GPDMA processing complete
	while(!Channel3_TC)
	{
		os_dly_wait(1);
	}
  	GPDMA_ChannelCmd(3, DISABLE);
	NVIC_DisableIRQ(DMA_IRQn);
	
	memcpy(&outbuffer[0],buffer,*length);
	outlength= (*length);
	//UART_Send(UARTx, (uint8_t*)buffer , bytesToWrite, BLOCKING);
	
	GPDMACfg.ChannelNum = 3;
	// Source memory
	GPDMACfg.SrcMemAddr = (uint32_t) outbuffer;
	// Destination memory - don't care
	GPDMACfg.DstMemAddr = 0;
	// Transfer size
	GPDMACfg.TransferSize = outlength;
	// Transfer width - don't care
	GPDMACfg.TransferWidth = 0;
	// Transfer type
	GPDMACfg.TransferType = GPDMA_TRANSFERTYPE_M2P;
	// Source connection - don't care
	GPDMACfg.SrcConn = 0;
	// Destination connection
	GPDMACfg.DstConn = destination;
	// Linker List Item - unused
	GPDMACfg.DMALLI = 0;
	// Setup channel with given parameter
	GPDMA_Setup(&GPDMACfg);

	Channel3_TC=0;

    	// Enable GPDMA channel 1
    GPDMA_ChannelCmd(3, ENABLE);
	NVIC_EnableIRQ(DMA_IRQn);*/  
	 
  return (bytesWritten); 
}

/*----------------------------------------------------------------------------
  check if character(s) are available at the serial interface
 *---------------------------------------------------------------------------*/
int ARM_Serial_AvailChar (int serDevNo, int *availChar) {
  SER_INFO *pSerDev;

  serDevNo -= 1;                         /* Convert the serDevNo to the port number */
  if ( (serDevNo < (SER_MINPORT)) ||     /* check the serial device number */
       (serDevNo > (SER_MAXPORT))   ) {
    return (SER_DEVNOTKNOWN_ERR);
  }
  pSerDev = &ser_Dev[(serDevNo - SER_MINPORT)];

  *availChar = IN_BUF_COUNT(pSerDev);

  return (SER_OK);
}

#if SERIAL_PORT_IN_USE(0)
/*----------------------------------------------------------------------------
  UART 0 interrupt
 *---------------------------------------------------------------------------*/
static __irq void ser_irq_0 (void) {
  SER_INFO *pSerDev = &ser_Dev[0 - SER_MINPORT];
  
  /* Handle serial interrupts, including writing received data to the 
     receive data register for UART 0 */

  volatile unsigned long iir;
  
  iir = *(pSerDev->iir);
   
  if ((iir & 0x4) || (iir & 0xC)) {                           // RDA or CTI pending
    while (*(pSerDev->lsr) & 0x01) {                          // Rx FIFO is not empty
      IN_BUF_WR(pSerDev, *(pSerDev->rbr));                    // Read Rx FIFO to buffer  
    }
  }
  if ((iir & 0x2)) {                                          // TXMIS pending
    pSerDev->txFifoEmpty = 1;
  }

  NVIC_ClearPendingIRQ(UART0_IRQn);
     
}

#endif /* SERIAL_PORT_IN_USE(0) */


#if SERIAL_PORT_IN_USE(1)
/*----------------------------------------------------------------------------
  UART 1 interrupt
 *---------------------------------------------------------------------------*/
static __irq void ser_irq_1 (void) { 
  SER_INFO *pSerDev = &ser_Dev[1 - SER_MINPORT];
  
  /* Handle serial interrupts, including writing received data to the 
     receive data register for UART 1 */
  volatile unsigned long iir;
  
  iir = *(pSerDev->iir);
   
  if ((iir & 0x4) || (iir & 0xC)) {                           // RDA or CTI pending
    while (*(pSerDev->lsr) & 0x01) {                          // Rx FIFO is not empty
      IN_BUF_WR(pSerDev, *(pSerDev->rbr));                    // Read Rx FIFO to buffer  
    }
  }
  if ((iir & 0x2)) {                                          // TXMIS pending
    pSerDev->txFifoEmpty = 1;
  }
  NVIC_ClearPendingIRQ(UART1_IRQn);  
}

#endif /* SERIAL_PORT_IN_USE(1) */


#if SERIAL_PORT_IN_USE(2)
/*----------------------------------------------------------------------------
  UART 2 interrupt
 *---------------------------------------------------------------------------*/
static __irq void ser_irq_2 (void) { 
  SER_INFO *pSerDev = &ser_Dev[2 - SER_MINPORT];
  /* Handle serial interrupts, including writing received data to the 
     receive data register for UART 2 */
  
  volatile unsigned long iir;
  
  iir = *(pSerDev->iir);
   
  if ((iir & 0x4) || (iir & 0xC)) {                           // RDA or CTI pending
    while (*(pSerDev->lsr) & 0x01) {                          // Rx FIFO is not empty
      IN_BUF_WR(pSerDev, *(pSerDev->rbr));                    // Read Rx FIFO to buffer  
    }
  }
  if ((iir & 0x2)) {                                          // TXMIS pending
    pSerDev->txFifoEmpty = 1;
  }

  NVIC_ClearPendingIRQ(UART2_IRQn);   
}

#endif /* SERIAL_PORT_IN_USE(2) */


#if SERIAL_PORT_IN_USE(3)
/*----------------------------------------------------------------------------
  UART 3 interrupt
 *---------------------------------------------------------------------------*/
static __irq void ser_irq_3 (void) { 
  SER_INFO *pSerDev = &ser_Dev[3 - SER_MINPORT];
  
  /* Handle serial interrupts, including writing received data to the 
     receive data register for UART 3 */

  volatile unsigned long iir;
  
  iir = *(pSerDev->iir);
   
  if ((iir & 0x4) || (iir & 0xC)) {                           // RDA or CTI pending
    while (*(pSerDev->lsr) & 0x01) {                          // Rx FIFO is not empty
      IN_BUF_WR(pSerDev, *(pSerDev->rbr));                    // Read Rx FIFO to buffer  
    }
  }
  if ((iir & 0x2)) {                                          // TXMIS pending
    pSerDev->txFifoEmpty = 1;
  }
  NVIC_ClearPendingIRQ(UART3_IRQn);
   
}
#endif /* SERIAL_PORT_IN_USE(3) */


/*----------------------------------------------------------------------------
  The following functions are used by the printf / scanf if stdout is
  redirected to the UART. They do not need to be modified
 *---------------------------------------------------------------------------*/

#ifndef UART_PRINTF_BAUDRATE 
#define UART_PRINTF_BAUDRATE 115200
#endif

#ifndef UART_PRINTF_PORT 
#define UART_PRINTF_PORT 1
#endif

#if ((UART_PRINTF_PORT != 0) && (UART_PRINTF_PORT != 1))
  #error "UART_PRINTF_PORT must be defined as either 0 or 1!"
#endif

static int printfDevNo;

void init_serial (void)  {               /* Initialize Serial Interface       */

  ARM_Serial_OpenPort (UART_PRINTF_PORT, &printfDevNo);
  ARM_Serial_InitPort(printfDevNo,
               UART_PRINTF_BAUDRATE,
               8,   /* 8 data bits */ 
               0,   /* No partity */ 
               0);  /* 1 stop bit */
}

/* implementation of putchar (also used by printf function to output data)    */
#define CR     0x0D
int sendchar (int ch)  {                 /* Write character to UART    */
  int length = 1;
  int cr = CR;

  if (ch == '\n')  {
    ARM_Serial_Write (printfDevNo, (const char *)&cr, &length);
  }
  ARM_Serial_Write (printfDevNo, (const char *)&ch, &length);


  return (ch);
}


int getkey (void)  {                     /* Read character from UART   */
  int availChar = 0;
  int length = 1;
  char data;

  do {
    ARM_Serial_AvailChar(printfDevNo, &availChar);
  } while (!availChar);

  ARM_Serial_Read (printfDevNo, &data, &length);

  return (data);
}

/*********************************************************************//**
 * @brief		GPDMA interrupt handler sub-routine
 * @param[in]	None
 * @return 		None
 **********************************************************************/
void DMA_IRQHandler (void)
{


	//voor UART via DMA
	if (GPDMA_IntGetStatus(GPDMA_STAT_INT, 3))
	{
		Channel3_TC++;
		//check interrupt status on channel 0
		// Check counter terminal status
		if(GPDMA_IntGetStatus(GPDMA_STAT_INTTC, 3))
		{
			// Clear terminate counter Interrupt pending
			GPDMA_ClearIntPending (GPDMA_STATCLR_INTTC, 3);
			 //Channel3_TC++;
			//dmaRdCh_TermianalCnt++;
		}
		if (GPDMA_IntGetStatus(GPDMA_STAT_INTERR, 3))
		{
			// Clear error counter Interrupt pending
			GPDMA_ClearIntPending (GPDMA_STATCLR_INTERR, 3);
			 Channel3_ERROR++;
			//dmaRdCh_ErrorCnt++;
		}
	}
}


UART_DMA_SEND(int UARTx, const char* buffer,int length)
{
  	GPDMA_Channel_CFG_Type GPDMACfg;
  	U8 destination;

   switch(UARTx)
  {
   case 1:
   destination = GPDMA_CONN_UART1_Tx;
   break;
   case 2:
   destination = GPDMA_CONN_UART2_Tx;
   break;
   case 0:
   default:
   destination = GPDMA_CONN_UART0_Tx;
   break;
  }



	// Wait for GPDMA processing complete
	while(!Channel3_TC)
	{
		os_dly_wait(1);
	}
  	GPDMA_ChannelCmd(3, DISABLE);
	NVIC_DisableIRQ(DMA_IRQn);
	memset(&outbuffer[0],0,256);
	memcpy(&outbuffer[0],buffer,length);
	outlength= length;
	//UART_Send(UARTx, (uint8_t*)buffer , bytesToWrite, BLOCKING);
	
	GPDMACfg.ChannelNum = 3;
	// Source memory
	GPDMACfg.SrcMemAddr = (uint32_t) outbuffer;
	// Destination memory - don't care
	GPDMACfg.DstMemAddr = 0;
	// Transfer size
	GPDMACfg.TransferSize = outlength;
	// Transfer width - don't care
	GPDMACfg.TransferWidth = 0;
	// Transfer type
	GPDMACfg.TransferType = GPDMA_TRANSFERTYPE_M2P;
	// Source connection - don't care
	GPDMACfg.SrcConn = 0;
	// Destination connection
	GPDMACfg.DstConn = destination;
	// Linker List Item - unused
	GPDMACfg.DMALLI = 0;
	// Setup channel with given parameter
	GPDMA_Setup(&GPDMACfg);

	Channel3_TC=0;

    	// Enable GPDMA channel 1
    GPDMA_ChannelCmd(3, ENABLE);
	NVIC_EnableIRQ(DMA_IRQn);


}

