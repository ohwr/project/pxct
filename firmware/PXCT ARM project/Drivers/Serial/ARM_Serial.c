/*----------------------------------------------------------------------------
 *      Name:    ARM_Serial.c
 *      Purpose: Template generic / LPC23xx UART ('serial') driver for LabVIEW
 *      Version: v1.00
 *      Note(s):
 *----------------------------------------------------------------------------
 *      This file is part of the uVision/ARM development tools.
 *      This software may only be used under the terms of a valid, current,
 *      end user licence from KEIL for a compatible version of KEIL software
 *      development tools. Nothing else gives you the right to use it.
 *
 *      Copyright (c) 2007 Keil Software.
 *---------------------------------------------------------------------------*/
 
#define SERIAL_PORT_IN_USE(port)    ((SER_MINPORT <= port) && (SER_MAXPORT >= port)) 
 
 
/* Add the correct header file for your selected CPU */ 

#include <LPC17xx.H>                      // LPC1758 definitions


#include "LVConfig.h"
#include "ARM_Serial.h"

#include "core_cm3.h"
#include "lpc17xx_uart.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpdma.h"

#define TRIG_SET()		GPIO_SetValue(1, 1 << 29)
#define TRIG_CLEAR()	GPIO_ClearValue(1, 1 << 29)

#define GPIO_RS485_DIRCTRL_DTR	(PINSEL_PIN_5)
#define GPIO_RS485_DIRCTRL_RTS	(PINSEL_PIN_7)

extern void Set_UART_Pin_Functions(U8 Port, U8 Pin, U8 Func);
extern void Set_UART1_RS485_Settings(U8 SettingsNr,U8 Value);
extern void Set_UART1_Flow(U8 SettingsNr, U8 Value);
int UART_NoDMA_Send(int serDevNo, uint8_t *buffer, uint32_t bytesToWrite);
int UART_DMA_Send(int serDevNo, const char* buffer,int length);

#ifndef UART0DMA
	#define UART0_Send(buffer,bytesToWrite) UART_NoDMA_Send(1,buffer,bytesToWrite)
#else
	#if UART0DMA
		#define UART0_Send(buffer,bytesToWrite) UART_DMA_Send(1,buffer,bytesToWrite)              
	#else
		#define UART0_Send(buffer,bytesToWrite) UART_NoDMA_Send(1,buffer,bytesToWrite)
	#endif
#endif

#ifndef UART1DMA
	#define UART1_Send(buffer,bytesToWrite) UART_NoDMA_Send(2,buffer,bytesToWrite)
#else
	#if UART1DMA
		#define UART1_Send(buffer,bytesToWrite) UART_DMA_Send(2,buffer,bytesToWrite)              
	#else
		#define UART1_Send(buffer,bytesToWrite) UART_NoDMA_Send(2,buffer,bytesToWrite)
	#endif
#endif

#define UART2_Send(buffer,bytesToWrite) UART_NoDMA_Send(3,buffer,bytesToWrite)


/* Set minimum and maximum port numbers available 
   NOTE: Setting this to only the required ports will reduce RAM and code size    */   

#define SER_MINPORT   0
#define SER_MAXPORT   0


#define SER_IN_BUF_SIZE   512            /* Input buffer in bytes (power 2) */
#define SER_OUT_BUF_SIZE  0              /* Output buffer in bytes (power 2) */

#define SER_PORTCLOSED    0x0
#define SER_PORTOPENED    0x1

#define SER_OK                  1
#define SER_GENERAL_ERR         0
#define SER_PORTNOTKNOWN_ERR  (-1)
#define SER_PORTNOTOPENED_ERR (-1)
#define SER_DEVNOTKNOWN_ERR   (-1)

/* Buffer masks */
#define SER_IN_MASK           (SER_IN_BUF_SIZE-1ul)
#define SER_OUT_MASK          (SER_OUT_BUF_SIZE-1ul)

/* Buffer read / write macros */
#define IN_BUF_RESET(serDev)        (serDev->ser_in.rd_idx = serDev->ser_in.wr_idx = 0)
#define IN_BUF_WR(serDev, dataIn)   (serDev->ser_in.data[SER_IN_MASK & serDev->ser_in.wr_idx++] = (dataIn))
#define IN_BUF_RD(serDev)           (serDev->ser_in.data[SER_IN_MASK & serDev->ser_in.rd_idx++])   
#define IN_BUF_EMPTY(serDev)        (serDev->ser_in.rd_idx == serDev->ser_in.wr_idx)
#define IN_BUF_FULL(serDev)         (serDev->ser_in.rd_idx == serDev->ser_in.wr_idx+1)
#define IN_BUF_COUNT(serDev)        (SER_IN_MASK & (serDev->ser_in.wr_idx - serDev->ser_in.rd_idx))

#define PINGPONG_BUFF 	512
#define NO_CURRENT_BUF	0
#define CURRENT_PING	1
#define CURRENT_PONG	2

#if UART0DMA
//UART0
char uart0DmaPingBuf[PINGPONG_BUFF];
int uart0DmaPingBufLen=0;
int uart0DmaPingBufBusy=0;
char uart0DmaPongBuf[PINGPONG_BUFF];
int uart0DmaPongBufLen=0;
int uart0DmaPongBufBusy=0;
int uart0CurrentDmaBuf = NO_CURRENT_BUF;
GPDMA_Channel_CFG_Type GPDMACfg0;
U32 dmaChannel4Rdy=1;
U32 dmaChannel4Err=1;
#endif

#if UART1DMA
//UART1
char uart1DmaPingBuf[PINGPONG_BUFF];
int uart1DmaPingBufLen=0;
int uart1DmaPingBufBusy=0;
char uart1DmaPongBuf[PINGPONG_BUFF];
int uart1DmaPongBufLen=0;
int uart1DmaPongBufBusy=0;
int uart1CurrentDmaBuf = NO_CURRENT_BUF;
GPDMA_Channel_CFG_Type GPDMACfg1;
U32 dmaChannel3Rdy=1;
U32 dmaChannel3Err=1;
#endif

U8 UART1_PORT=0;

//alle settings voor UART die bij open of init gedaan moeten worden
int8_t UART0_0_2 = 1;
int8_t UART0_0_3 = 1;

int8_t UART1_0_15 = -1;
int8_t UART1_0_16 = -1;
int8_t UART1_0_17 = -1;
int8_t UART1_0_18 = -1;
int8_t UART1_0_22 = -1;
int8_t UART1_2_0 = 2;
int8_t UART1_2_1 = 2;
int8_t UART1_2_2 = 2;
int8_t UART1_2_3 = 2;
int8_t UART1_2_4 = 2;
int8_t UART1_2_5 = 2;
int8_t UART1_2_6 = 2;
int8_t UART1_2_7 = 2;
//int8_t UART1_2_0 = -1;
//int8_t UART1_2_1 = -1;
//int8_t UART1_2_2 = -1;
//int8_t UART1_2_3 = -1;
//int8_t UART1_2_4 = -1;
//int8_t UART1_2_5 = -1;
//int8_t UART1_2_6 = -1;
//int8_t UART1_2_7 = -1;

int8_t UART2_0_10 = 1;
int8_t UART2_0_11 = 1;
int8_t UART2_2_8 = -1;
int8_t UART2_2_9 = -1;

int8_t UART1_CTS = -1;
int8_t UART1_RTS = -1;
int UART1_RS485_HalfDuplex=0;
UART1_RS485_CTRLCFG_Type UART1_RS485_Settings = {
	DISABLE, 					/* Normal MultiDrop mode State */
	ENABLE,  					/* Receiver State */
	DISABLE, 					/* Auto Address Detect mode state */
	DISABLE, 					/* Auto Direction Control State */
	UART1_RS485_DIRCTRL_RTS, 	/* If direction control is enabled, state pin RTS is used for direction control */
	RESET, 						/* Polarity of the direction control signal on the RTS (or DTR) pin */
	0,							/* address match value for RS-485/EIA-485 mode, 8-bit long */
	0							/* delay time is in periods of the baud clock, 8-bit long */
};

//buffer en functies voor CTS data
#define UART_RING_BUFSIZE 256
// Serial input buffer
typedef struct __ser_in_t {
  volatile unsigned char data[SER_IN_BUF_SIZE];
  volatile unsigned int wr_idx;
  volatile unsigned int rd_idx;
} ser_in_t;

/*----------------------------------------------------------------------------
  structure to administrate the UART
 *---------------------------------------------------------------------------*/
typedef __irq void (*ser_irq_fp)(void);           // RV irq function pointer typedef

#if SERIAL_PORT_IN_USE(0)
static __irq void ser_irq_0 (void);               // RV irq function prototype for port 0
#endif /* SERIAL_PORT_IN_USE(0) */

#if SERIAL_PORT_IN_USE(1)
static __irq void ser_irq_1 (void);               // RV irq function prototype for port 1
#endif /* SERIAL_PORT_IN_USE(1) */

#if SERIAL_PORT_IN_USE(2)
static __irq void ser_irq_2 (void);               // RV irq function prototype for port 2
#endif /* SERIAL_PORT_IN_USE(2) */

typedef struct {
   int   		  serialDeviceNumber;       // = portNumber + 1
   unsigned int   status;
   unsigned long  baudRate;
   unsigned int   dataBits;
   unsigned int   stopBits;
   unsigned int   parity;
           
/* Add the correct defines for the registers within your UART. Also add 
   any other port specific variables that are required */
  
  volatile unsigned long *rbr;
  volatile unsigned long *thr;
  volatile unsigned long *dll;
  volatile unsigned long *dlm;
  volatile unsigned long *lcr;
  volatile unsigned long *lsr;
  volatile unsigned long *fdr;
  volatile unsigned long *fcr;
  volatile unsigned long *iir;
  volatile unsigned long *ier;
  volatile unsigned long *rs485ctrl;
  volatile unsigned long *irqAddr;
  volatile unsigned long *irqCntl;  
  unsigned long          ser_irq_mask;              // Serial IRQ enable / disable bit mask
  unsigned long          transmitting     :1;       // 0x000(0000000X) Indicates transmittion
  unsigned long          unused_flags     :31;      // 0xXXX(XXXXXXX0) Unused 
  
  ser_irq_fp             ser_irq;                   // Serial IRQ function
  ser_in_t               ser_in;
} SER_INFO;

static SER_INFO ser_Dev[SER_MAXPORT - SER_MINPORT + 1] = {
#if SERIAL_PORT_IN_USE(0)
  { 1, 
    SER_PORTCLOSED, 9600, 8, 0, 0, 

/* Add the correct register addresses within your first UART. Also add 
   any other variable initialisation required */
   
    (volatile unsigned long *)(LPC_UART0_BASE + 0x00),   /* RBR */
    (volatile unsigned long *)(LPC_UART0_BASE + 0x00),   /* THR */
    (volatile unsigned long *)(LPC_UART0_BASE + 0x00),   /* DLL */
    (volatile unsigned long *)(LPC_UART0_BASE + 0x04),   /* DLM */
    (volatile unsigned long *)(LPC_UART0_BASE + 0x0C),   /* LCR */
    (volatile unsigned long *)(LPC_UART0_BASE + 0x14),   /* LSR */
    (volatile unsigned long *)(LPC_UART0_BASE + 0x28),   /* FDR */
    (volatile unsigned long *)(LPC_UART0_BASE + 0x08),   /* FCR */
    (volatile unsigned long *)(LPC_UART0_BASE + 0x08),   /* IIR */
    (volatile unsigned long *)(LPC_UART0_BASE + 0x04),   /* IER */
    (volatile unsigned long *)(LPC_UART0_BASE + 0x1C),   /* RS485CTRL point to SCR */
    (volatile unsigned long *)(0x10000000  + 0x54),    /* IRQ Addr */
    (volatile unsigned long *)(NVIC + 0x218),    /* IRQ Cntl */
    UART0_IRQn,                                           /* IRQ mask bit */
    0,                                                    /* transmitting */
    0,                                                    /* unused_flags */
    
    ser_irq_0                                             /* IRQ function */
  },
#endif /* SERIAL_PORT_IN_USE(0) */

#if SERIAL_PORT_IN_USE(1) 
  { 2, 
    SER_PORTCLOSED, 9600, 8, 0, 0, 
    
/* Add the correct register addresses within your first UART. Also add 
   any other variable initialisation required */
  
    (volatile unsigned long *)(LPC_UART1_BASE + 0x00),   /* RBR */
    (volatile unsigned long *)(LPC_UART1_BASE + 0x00),   /* THR */
    (volatile unsigned long *)(LPC_UART1_BASE + 0x00),   /* DLL */
    (volatile unsigned long *)(LPC_UART1_BASE + 0x04),   /* DLM */
    (volatile unsigned long *)(LPC_UART1_BASE + 0x0C),   /* LCR */
    (volatile unsigned long *)(LPC_UART1_BASE + 0x14),   /* LSR */
    (volatile unsigned long *)(LPC_UART1_BASE + 0x28),   /* FDR */
    (volatile unsigned long *)(LPC_UART1_BASE + 0x08),   /* FCR */
    (volatile unsigned long *)(LPC_UART1_BASE + 0x08),   /* IIR */
    (volatile unsigned long *)(LPC_UART1_BASE + 0x04),   /* IER */
    (volatile unsigned long *)(LPC_UART1_BASE + 0x4C),   /* RS485CTRL*/
    (volatile unsigned long *)(0x10000000  + 0x58),    /* IRQ Addr */
    (volatile unsigned long *)(NVIC + 0x21C),    /* IRQ Cntl */
    UART1_IRQn,                                           /* IRQ mask bit */
    0,                                                    /* transmitting */
    0,                                                    /* unused_flags */
   
    ser_irq_1                                             /* IRQ function */    
  },
#endif /* SERIAL_PORT_IN_USE(1) */

#if SERIAL_PORT_IN_USE(2) 
  { 3, 
    SER_PORTCLOSED, 9600, 8, 0, 0, 
    
/* Add the correct register addresses within your first UART. Also add 
   any other variable initialisation required */

    (volatile unsigned long *)(LPC_UART2_BASE + 0x00),   /* RBR */
    (volatile unsigned long *)(LPC_UART2_BASE + 0x00),   /* THR */
    (volatile unsigned long *)(LPC_UART2_BASE + 0x00),   /* DLL */
    (volatile unsigned long *)(LPC_UART2_BASE + 0x04),   /* DLM */
    (volatile unsigned long *)(LPC_UART2_BASE + 0x0C),   /* LCR */
    (volatile unsigned long *)(LPC_UART2_BASE + 0x14),   /* LSR */
    (volatile unsigned long *)(LPC_UART2_BASE + 0x28),   /* FDR */
    (volatile unsigned long *)(LPC_UART2_BASE + 0x08),   /* FCR */
    (volatile unsigned long *)(LPC_UART2_BASE + 0x08),   /* IIR */
    (volatile unsigned long *)(LPC_UART2_BASE + 0x04),   /* IER */
    (volatile unsigned long *)(LPC_UART2_BASE + 0x1C),   /* RS485CTRL point to SCR */
    (volatile unsigned long *)(0x10000000  + 0x5C),    /* IRQ Addr */
    (volatile unsigned long *)(NVIC + 0x270),    /* IRQ Cntl */
    UART2_IRQn,                                           /* IRQ mask bit */
    0,                                                    /* transmitting */
    0,                                                    /* unused_flags */
    ser_irq_2                                             /* IRQ function */    
  }
#endif /* SERIAL_PORT_IN_USE(2) */
};

/*----------------------------------------------------------------------------
  open the UART
 *---------------------------------------------------------------------------*/
int ARM_Serial_OpenPort (int port, int *serDevNo) {
	PINSEL_CFG_Type pincfg;
	pincfg.OpenDrain = 0;
	pincfg.Pinmode = 0;

  if ( (port < SER_MINPORT) ||                      /* check the port number */
       (port > SER_MAXPORT)   ) {
    return (SER_PORTNOTKNOWN_ERR);
  }

  switch (port) {
    #if SERIAL_PORT_IN_USE(0)
    case 0:
      /* Add code to initialise the pinout hardware for UART 0 */

	  	/*pincfg.Portnum=0;
		pincfg.Funcnum=1;
		pincfg.Pinnum=2;
		PINSEL_ConfigPin(&pincfg);	  
	    pincfg.Pinnum=3;
		PINSEL_ConfigPin(&pincfg);	
		pincfg.Portnum=0;
		pincfg.Pinnum=2;
		pincfg.Funcnum=UART0_0_2;
		PINSEL_ConfigPin(&pincfg);

		pincfg.Portnum=0;
		pincfg.Pinnum=3;
		pincfg.Funcnum=UART0_0_3;
		PINSEL_ConfigPin(&pincfg);	 */

			if(UART0_0_2>=0)
			{			
	  		pincfg.Portnum=0;
			pincfg.Funcnum=UART0_0_2;
			pincfg.Pinnum=2;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART0_0_3>=0)
			{			
	  		pincfg.Portnum=0;
			pincfg.Funcnum=UART0_0_3;
			pincfg.Pinnum=3;
			PINSEL_ConfigPin(&pincfg);	  
			}


    #endif
    #if SERIAL_PORT_IN_USE(1)
    case 1:
      /* Add code to initialise the pinout hardware for UART 1 */
	  
//	  	if(UART1_PORT==1)
//		{
//	  		pincfg.Portnum=0;
//			pincfg.Funcnum=1;
//			pincfg.Pinnum=15;
//			PINSEL_ConfigPin(&pincfg);	  
//	    	pincfg.Pinnum=16;
//			PINSEL_ConfigPin(&pincfg);
//		} else
//		{
/*	  		pincfg.Portnum=2;
			pincfg.Funcnum=2;
			pincfg.Pinnum=0;
			PINSEL_ConfigPin(&pincfg);	  
	    	pincfg.Pinnum=1;
			PINSEL_ConfigPin(&pincfg);
//		}

		pincfg.Portnum=2;
	    pincfg.Pinnum=2;
		PINSEL_ConfigPin(&pincfg);
	    pincfg.Pinnum=3;
		PINSEL_ConfigPin(&pincfg);
	    pincfg.Pinnum=4;
		PINSEL_ConfigPin(&pincfg);
	    pincfg.Pinnum=5;
		PINSEL_ConfigPin(&pincfg);
	    pincfg.Pinnum=6;
		PINSEL_ConfigPin(&pincfg);
	    pincfg.Pinnum=7;
		PINSEL_ConfigPin(&pincfg);*/	 
			if(UART1_0_15>=0)
			{			
	  		pincfg.Portnum=0;
			pincfg.Funcnum=UART1_0_15;
			pincfg.Pinnum=15;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART1_0_16>=0)
			{			
	  		pincfg.Portnum=0;
			pincfg.Funcnum=UART1_0_16;
			pincfg.Pinnum=16;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART1_0_17>=0)
			{			
	  		pincfg.Portnum=0;
			pincfg.Funcnum=UART1_0_17;
			pincfg.Pinnum=17;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART1_0_18>=0)
			{			
	  		pincfg.Portnum=0;
			pincfg.Funcnum=UART1_0_18;
			pincfg.Pinnum=18;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART1_0_22>=0)
			{			
	  		pincfg.Portnum=0;
			pincfg.Funcnum=UART1_0_22;
			pincfg.Pinnum=22;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART1_2_0>=0)
			{			
	  		pincfg.Portnum=2;
			pincfg.Funcnum=UART1_2_0;
			pincfg.Pinnum=0;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART1_2_1>=0)
			{			
	  		pincfg.Portnum=2;
			pincfg.Funcnum=UART1_2_1;
			pincfg.Pinnum=1;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART1_2_2>=0)
			{			
	  		pincfg.Portnum=2;
			pincfg.Funcnum=UART1_2_2;
			pincfg.Pinnum=2;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART1_2_3>=0)
			{			
	  		pincfg.Portnum=2;
			pincfg.Funcnum=UART1_2_3;
			pincfg.Pinnum=3;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART1_2_4>=0)
			{			
	  		pincfg.Portnum=2;
			pincfg.Funcnum=UART1_2_4;
			pincfg.Pinnum=4;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART1_2_5>=0)
			{			
	  		pincfg.Portnum=2;
			pincfg.Funcnum=UART1_2_5;
			pincfg.Pinnum=5;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART1_2_6>=0)
			{			
	  		pincfg.Portnum=2;
			pincfg.Funcnum=UART1_2_6;
			pincfg.Pinnum=6;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART1_2_7>=0)
			{			
	  		pincfg.Portnum=2;
			pincfg.Funcnum=UART1_2_7;
			pincfg.Pinnum=7;
			PINSEL_ConfigPin(&pincfg);	  
			}
			
   	#endif
    #if SERIAL_PORT_IN_USE(2)      
    case 2:
      /* Add code to initialise the pinout hardware for UART 2 */
	  	/*pincfg.Portnum=0;
		pincfg.Funcnum=1;
		pincfg.Pinnum=10;
		PINSEL_ConfigPin(&pincfg);	  
	    pincfg.Pinnum=11;
		PINSEL_ConfigPin(&pincfg);	*/
			if(UART2_0_10>=0)
			{			
	  		pincfg.Portnum=0;
			pincfg.Funcnum=UART2_0_10;
			pincfg.Pinnum=10;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART2_0_11>=0)
			{			
	  		pincfg.Portnum=0;
			pincfg.Funcnum=UART2_0_11;
			pincfg.Pinnum=11;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(!(UART2_2_8<0))
			{			
	  		pincfg.Portnum=2;
			pincfg.Funcnum=UART2_2_8;
			pincfg.Pinnum=8;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART2_2_9>=0)
			{			
	  		pincfg.Portnum=2;
			pincfg.Funcnum=UART2_2_9;
			pincfg.Pinnum=9;
			PINSEL_ConfigPin(&pincfg);	  
			}



    #endif
    default:
      ; // Should never happen (protected by port check above)
  };

  ser_Dev[port - SER_MINPORT].status = SER_PORTOPENED;
  *serDevNo = ser_Dev[port - SER_MINPORT].serialDeviceNumber;

  return (SER_OK);
}

/*----------------------------------------------------------------------------
  close the UART
 *---------------------------------------------------------------------------*/
int ARM_Serial_ClosePort (int serDevNo) {
  SER_INFO *pSerDev;
  PINSEL_CFG_Type pincfg;
  LPC_UART_TypeDef* UARTx;

  serDevNo -= 1;                                  /* Convert the serDevNo to the port number */
  if ( (serDevNo < (SER_MINPORT)) ||              /* check the serial device number */
       (serDevNo > (SER_MAXPORT))   ) {
    return (SER_DEVNOTKNOWN_ERR);
  }

  pSerDev = &ser_Dev[(serDevNo - SER_MINPORT)];

  switch (serDevNo) {
  	case 0: UARTx=LPC_UART0; break;
	case 1: UARTx=(LPC_UART_TypeDef*)LPC_UART1; break;
	case 2: UARTx=LPC_UART2; break;
   	default: return SER_DEVNOTKNOWN_ERR;
  }

  //while(!pSerDev->txFifoEmpty) 
  //  os_dly_wait(10);                  /* wait for fifo to empty before closing port */

	  /* Disable the interrupt in the VIC and UART controllers */
  NVIC_DisableIRQ(pSerDev->ser_irq_mask);			/* Disable VIC interrupt     */
  UART_DeInit(UARTx);
  //*(pSerDev->ier) = 0x00;                           /* Disable UART interrupts   */       
 
  switch (serDevNo - SER_MINPORT) {
    #if SERIAL_PORT_IN_USE(0)
    case 0:
      /* Uninitialise the pinout hardware for UART 0 */
	  	/*pincfg.Portnum=0;
		pincfg.Funcnum=0;
		pincfg.Pinnum=2;
		PINSEL_ConfigPin(&pincfg);	  
	    pincfg.Pinnum=3;
		PINSEL_ConfigPin(&pincfg);
		UARTx=LPC_UART0; */

			if(UART0_0_2>=0)
			{			
	  		pincfg.Portnum=0;
			pincfg.Funcnum=0;
			pincfg.Pinnum=2;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART0_0_3>=0)
			{			
	  		pincfg.Portnum=0;
			pincfg.Funcnum=0;
			pincfg.Pinnum=3;
			PINSEL_ConfigPin(&pincfg);	  
			}

    #endif
    #if SERIAL_PORT_IN_USE(1)
    case 1:
      /* Uninitialise the pinout hardware for UART 1 */	
   //UART_FullModemConfigMode(LPC_UART1,UART1_MODEM_MODE_AUTO_RTS,DISABLE);
   //UART_FullModemConfigMode(LPC_UART1,UART1_MODEM_MODE_AUTO_CTS,DISABLE);
   //UART_IntConfig((LPC_UART_TypeDef *)LPC_UART1, UART_INTCFG_RLS, DISABLE);

/*	  	pincfg.Portnum=2;
		pincfg.Funcnum=0;
		pincfg.Pinnum=0;
		PINSEL_ConfigPin(&pincfg);	  
	    pincfg.Pinnum=1;
		PINSEL_ConfigPin(&pincfg);

	    pincfg.Pinnum=2;
		PINSEL_ConfigPin(&pincfg);
	    pincfg.Pinnum=3;
		PINSEL_ConfigPin(&pincfg);
	    pincfg.Pinnum=4;
		PINSEL_ConfigPin(&pincfg);
	    pincfg.Pinnum=5;
		PINSEL_ConfigPin(&pincfg);
	    pincfg.Pinnum=6;
		PINSEL_ConfigPin(&pincfg);
	    pincfg.Pinnum=7;
		PINSEL_ConfigPin(&pincfg);
		UARTx=(LPC_UART_TypeDef*)LPC_UART1;	 */

			if(UART1_0_15>=0)
			{			
	  		pincfg.Portnum=0;
			pincfg.Funcnum=0;
			pincfg.Pinnum=15;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART1_0_16>=0)
			{			
	  		pincfg.Portnum=0;
			pincfg.Funcnum=0;
			pincfg.Pinnum=16;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART1_0_17>=0)
			{			
	  		pincfg.Portnum=0;
			pincfg.Funcnum=0;
			pincfg.Pinnum=17;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART1_0_18>=0)
			{			
	  		pincfg.Portnum=0;
			pincfg.Funcnum=0;
			pincfg.Pinnum=18;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART1_0_22>=0)
			{			
	  		pincfg.Portnum=0;
			pincfg.Funcnum=0;
			pincfg.Pinnum=22;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART1_2_0>=0)
			{			
	  		pincfg.Portnum=2;
			pincfg.Funcnum=0;
			pincfg.Pinnum=0;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART1_2_1>=0)
			{			
	  		pincfg.Portnum=2;
			pincfg.Funcnum=0;
			pincfg.Pinnum=1;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART1_2_2>=0)
			{			
	  		pincfg.Portnum=2;
			pincfg.Funcnum=0;
			pincfg.Pinnum=2;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART1_2_3>=0)
			{			
	  		pincfg.Portnum=2;
			pincfg.Funcnum=0;
			pincfg.Pinnum=3;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART1_2_4>=0)
			{			
	  		pincfg.Portnum=2;
			pincfg.Funcnum=0;
			pincfg.Pinnum=4;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART1_2_5>=0)
			{			
	  		pincfg.Portnum=2;
			pincfg.Funcnum=0;
			pincfg.Pinnum=5;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART1_2_6>=0)
			{			
	  		pincfg.Portnum=2;
			pincfg.Funcnum=0;
			pincfg.Pinnum=6;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART1_2_7>=0)
			{			
	  		pincfg.Portnum=2;
			pincfg.Funcnum=0;
			pincfg.Pinnum=7;
			PINSEL_ConfigPin(&pincfg);	  
			}
		
					
    #endif
    #if SERIAL_PORT_IN_USE(2)
    case 2:
      /* Uninitialise the pinout hardware for UART 2 */
	  /*	pincfg.Portnum=0;
		pincfg.Funcnum=0;
		pincfg.Pinnum=10;
		PINSEL_ConfigPin(&pincfg);	  
	    pincfg.Pinnum=11;
		PINSEL_ConfigPin(&pincfg);
		UARTx=LPC_UART2; */

			if(UART2_0_10>=0)
			{			
	  		pincfg.Portnum=0;
			pincfg.Funcnum=0;
			pincfg.Pinnum=10;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART2_0_11>=0)
			{			
	  		pincfg.Portnum=0;
			pincfg.Funcnum=0;
			pincfg.Pinnum=11;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(!(UART2_2_8<0))
			{			
	  		pincfg.Portnum=2;
			pincfg.Funcnum=0;
			pincfg.Pinnum=8;
			PINSEL_ConfigPin(&pincfg);	  
			}
			if(UART2_2_9>=0)
			{			
	  		pincfg.Portnum=2;
			pincfg.Funcnum=0;
			pincfg.Pinnum=9;
			PINSEL_ConfigPin(&pincfg);	  
			}


    #endif
    default:
      ; // Should never happen (protected by port check above)  
  }

  pSerDev->status = SER_PORTCLOSED;
  
  return (SER_OK);
}

/*----------------------------------------------------------------------------
  initialize the UART
 *---------------------------------------------------------------------------*/
int ARM_Serial_InitPort (int serDevNo, unsigned long baudrate, unsigned int  databits,
                                unsigned int  parity,   unsigned int  stopbits) {
	unsigned char lcr_p, lcr_s, lcr_d;
	unsigned long dll;
	SER_INFO *pSerDev;
	LPC_UART_TypeDef* UARTx;
	UART_CFG_Type UARTxConfig;
	UART_FIFO_CFG_Type UARTFIFOConfigStruct;
	UART1_RS485_CTRLCFG_Type RS485_settings;

	serDevNo -= 1;                         /* Convert the serDevNo to the port number */
	if ( (serDevNo < (SER_MINPORT)) ||     /* check the serial device number */
			(serDevNo > (SER_MAXPORT))   ) {
		return (SER_DEVNOTKNOWN_ERR);
	}

	pSerDev = &ser_Dev[(serDevNo - SER_MINPORT)];

	switch (serDevNo) {
  		case 0: UARTx=LPC_UART0; break;
		case 1: UARTx=(LPC_UART_TypeDef*)LPC_UART1; break;
		case 2: UARTx=LPC_UART2; break;
   		default: return (SER_DEVNOTKNOWN_ERR);
	}
  	UARTxConfig.Baud_rate=baudrate;
  	UARTxConfig.Databits=databits-5;

  	switch (stopbits) {
    	case 1:                              /* 1,5 Stop bits */
    	case 2:                              /* 2   Stop bits */
	  		UARTxConfig.Stopbits=UART_STOPBIT_2;
    		break;
    	case 0:                              /* 1   Stop bit  */
    	default:
	  		UARTxConfig.Stopbits=UART_STOPBIT_1;
    		break;
  	}

  	switch (parity) {
    	case 1:                              /* Parity Odd    */
	  		UARTxConfig.Parity=UART_PARITY_ODD;
    		break;
    	case 2:                              /* Parity Even   */
	  		UARTxConfig.Parity=UART_PARITY_EVEN;
    		break;
    	case 3:                              /* Parity Mark   */
    		break;
    	case 4:                              /* Parity Space  */
    		break;
    	case 0:                              /* Parity None   */
    	default:
	  		UARTxConfig.Parity=UART_PARITY_NONE;
    		break;
  	}
  	pSerDev->baudRate = baudrate;
  	pSerDev->dataBits = databits;
  	pSerDev->stopBits = stopbits;
  	pSerDev->parity   = parity;
  
  	IN_BUF_RESET(pSerDev);

  	UART_Init(UARTx,&UARTxConfig);

	UART_FIFOConfigStructInit(&UARTFIFOConfigStruct);

  	if (serDevNo == 1) {
		// Initialize FIFO for UART1 peripheral
		UART_FIFOConfig((LPC_UART_TypeDef *)LPC_UART1, &UARTFIFOConfigStruct);
	}

	UART_TxCmd(UARTx,ENABLE);

//  if(serDevNo==1)
//  {
//  	if(UART1_RTS>=0)
//  	{
//  		UART_FullModemConfigMode(LPC_UART1,UART1_MODEM_MODE_AUTO_RTS,UART1_RTS);
//   	}
//   	if(UART1_CTS>=0)
//   	{
//   		//UART_FullModemConfigMode(LPC_UART1,UART1_MODEM_MODE_AUTO_CTS,UART1_CTS);
//   	}
//  }

  	/* Set up and enable the UART interrupt in the interrupt controller */
 
  	*(pSerDev->irqAddr) = (unsigned long)pSerDev->ser_irq;  /* Set interrupt function           */
                            
  	NVIC_SetPriority(pSerDev->ser_irq_mask,0x0F);	 			/* Set interrupt priority           */
  	NVIC_EnableIRQ(pSerDev->ser_irq_mask);                  	/* Enable interrupt */ 


//// Reset ring buf head and tail idx
//	__BUF_RESET(rb.rx_head);
//	__BUF_RESET(rb.rx_tail);
//	__BUF_RESET(rb.tx_head);
//	__BUF_RESET(rb.tx_tail);
//		TxIntStat = RESET;

  	if (serDevNo == 1) {
		//UART_FullModemConfigMode(LPC_UART1,UART1_MODEM_MODE_AUTO_RTS,ENABLE);
		//UART_FullModemConfigMode(LPC_UART1,UART1_MODEM_MODE_AUTO_CTS,ENABLE);
		
		/* Enable UART line status interrupt */
		UART_IntConfig(UARTx, UART_INTCFG_RLS, ENABLE);
		/* Enable UART Transmitter Holding Register Empty status interrupt */
		UART_IntConfig(UARTx, UART_INTCFG_THRE, ENABLE);
		
		UART_RS485Config(LPC_UART1,&UART1_RS485_Settings);
	}
  	/* Enable UART Rx interrupt */
  	UART_IntConfig(UARTx,UART_INTCFG_RBR,ENABLE);

//	if(!UART1_CTS)
//	{ 
	GPDMA_Init();
//	} 
  	return (SER_OK);
}

/*----------------------------------------------------------------------------
  read data from UART
 *---------------------------------------------------------------------------*/
int ARM_Serial_Read (int serDevNo, char *buffer, const int *length) {
	SER_INFO *pSerDev;
	int bytesToRead;
  
	serDevNo -= 1;                         /* Convert the serDevNo to the port number */
  	if ( (serDevNo < (SER_MINPORT)) ||     /* check the serial device number */
    		(serDevNo > (SER_MAXPORT))   ) {
    	return (SER_DEVNOTKNOWN_ERR);
  	}
  	pSerDev = &ser_Dev[(serDevNo - SER_MINPORT)];

  	bytesToRead = *length;
  	while (bytesToRead--) {
		while (IN_BUF_EMPTY(pSerDev));       /* Block until data is available if none */
    	*buffer++ = IN_BUF_RD(pSerDev);
  	}
  	return (*length);  
}

/*----------------------------------------------------------------------------
  write data to the UART
 *---------------------------------------------------------------------------*/
int ARM_Serial_Write (int serDevNo, const char *buffer, int *length) {
  	SER_INFO *pSerDev;
  	int i, bytesToWrite, bytesWritten;
  	LPC_UART_TypeDef* UARTx;
  	GPDMA_Channel_CFG_Type GPDMACfg;
  	U8 destination;
  
  	serDevNo -= 1;                         /* Convert the serDevNo to the port number */
  	if ( (serDevNo < (SER_MINPORT)) ||     /* check the serial device number */
    		(serDevNo > (SER_MAXPORT))   ) {
    	return (SER_DEVNOTKNOWN_ERR);
  	}
  	pSerDev = &ser_Dev[(serDevNo - SER_MINPORT)];

  	// Write *length bytes
  	bytesToWrite = *length;
  	bytesWritten = bytesToWrite;

  	/* Write the data to the transmit section of the UART hardware */
  
  	/* Lets just use the 16 byte hardware FIFO to buffer outgoing data */

   	switch (serDevNo) {
   		case 0:
   			UARTx=LPC_UART0;
   			destination = GPDMA_CONN_UART0_Tx;
			UART0_Send((uint8_t*)&buffer[0],bytesToWrite);
			break;
   		case 1:
   			UARTx=(LPC_UART_TypeDef*)LPC_UART1;
	   		destination = GPDMA_CONN_UART1_Tx;
   			UART1_Send((uint8_t*)&buffer[0],bytesToWrite);	 //define geeft aan DMA of normal 
			break;
   		case 2:
   			UARTx=LPC_UART2;
   			destination = GPDMA_CONN_UART2_Tx;
   			UART2_Send((uint8_t*)&buffer[0],bytesToWrite);
   			break;
   		default:
			return (SER_DEVNOTKNOWN_ERR);
  	}
  	return (bytesWritten); 
}

/*----------------------------------------------------------------------------
  check if character(s) are available at the serial interface
 *---------------------------------------------------------------------------*/
int ARM_Serial_AvailChar (int serDevNo, int *availChar) {
	SER_INFO *pSerDev;

	serDevNo -= 1;                         /* Convert the serDevNo to the port number */
	if ( (serDevNo < (SER_MINPORT)) ||     /* check the serial device number */
			(serDevNo > (SER_MAXPORT))   ) {
		return (SER_DEVNOTKNOWN_ERR);
	}
	pSerDev = &ser_Dev[(serDevNo - SER_MINPORT)];

	*availChar = IN_BUF_COUNT(pSerDev);

	return (SER_OK);
}

/*----------------------------------------------------------------------------
  check if currently transmitting
 *---------------------------------------------------------------------------*/
int ARM_Serial_TransmitStatus (int serDevNo, int *txStatus) {
  	SER_INFO *pSerDev;
  	volatile unsigned long rs485ctrl;
	unsigned long rs485_DE_readback=0;

  	serDevNo -= 1;                         /* Convert the serDevNo to the port number */
  	if ( (serDevNo < (SER_MINPORT)) ||     /* check the serial device number */
			(serDevNo > (SER_MAXPORT))   ) {
    	return (SER_DEVNOTKNOWN_ERR);
  	}
  	pSerDev = &ser_Dev[(serDevNo - SER_MINPORT)];

	if (serDevNo == 1) {
		rs485ctrl = *(pSerDev->rs485ctrl);

		if (UART1_RS485_HalfDuplex) {
			if (rs485ctrl & UART1_RS485CTRL_DCTRL_EN) {
				if (rs485ctrl & UART1_RS485CTRL_SEL_DTR) {
					// UART1_RS485_DIRCTRL_DTR
					rs485_DE_readback = ((GPIO_ReadValue(PINSEL_PORT_2) >> GPIO_RS485_DIRCTRL_DTR) & 0x1);
				}
				else {
					// UART1_RS485_DIRCTRL_RTS
					rs485_DE_readback = ((GPIO_ReadValue(PINSEL_PORT_2) >> GPIO_RS485_DIRCTRL_RTS) & 0x1);
				}
				if (!(rs485ctrl & UART1_RS485CTRL_OINV_1)) {
					rs485_DE_readback = (~rs485_DE_readback) & 0x1;
				}
			}
		}
  	}
	*txStatus = pSerDev->transmitting | rs485_DE_readback;

  	return (SER_OK);
}


#if SERIAL_PORT_IN_USE(0)
/*----------------------------------------------------------------------------
  UART 0 interrupt
 *---------------------------------------------------------------------------*/
static __irq void ser_irq_0 (void) {
  SER_INFO *pSerDev = &ser_Dev[0 - SER_MINPORT];
  
  /* Handle serial interrupts, including writing received data to the 
     receive data register for UART 0 */

  volatile unsigned long iir;
  
  iir = *(pSerDev->iir);
   
  if ((iir & 0x4) || (iir & 0xC)) {                           // RDA or CTI pending
    while (*(pSerDev->lsr) & 0x01) {                          // Rx FIFO is not empty
      IN_BUF_WR(pSerDev, *(pSerDev->rbr));                    // Read Rx FIFO to buffer  
    }
  }
  if ((iir & 0x2)) {                                          // THRE pending
    pSerDev->transmitting = 0;
  }

  NVIC_ClearPendingIRQ(UART0_IRQn);
     
}

#endif /* SERIAL_PORT_IN_USE(0) */


#if SERIAL_PORT_IN_USE(1)
/*----------------------------------------------------------------------------
  UART 1 interrupt
 *---------------------------------------------------------------------------*/
static __irq void ser_irq_1 (void) { 
	SER_INFO *pSerDev = &ser_Dev[1 - SER_MINPORT];
  
	/* Handle serial interrupts, including writing received data to the 
     receive data register for UART 1 */
 
  	volatile unsigned long iir, rs485ctrl;
	volatile unsigned long dummy;
	unsigned long receive_disable=0, rs485_DE_readback=0;

	rs485ctrl = *(pSerDev->rs485ctrl);

	if (UART1_RS485_HalfDuplex) {
		if (rs485ctrl & UART1_RS485CTRL_DCTRL_EN) {
			if (rs485ctrl & UART1_RS485CTRL_SEL_DTR) {
				// UART1_RS485_DIRCTRL_DTR
				rs485_DE_readback = ((GPIO_ReadValue(PINSEL_PORT_2) >> GPIO_RS485_DIRCTRL_DTR) & 0x1);
			}
			else {
				// UART1_RS485_DIRCTRL_RTS
				rs485_DE_readback = ((GPIO_ReadValue(PINSEL_PORT_2) >> GPIO_RS485_DIRCTRL_RTS) & 0x1);
			}
			if (!(rs485ctrl & UART1_RS485CTRL_OINV_1)) {
				rs485_DE_readback = (~rs485_DE_readback) & 0x1;
			}
		} 
		receive_disable = (pSerDev->transmitting) | rs485_DE_readback;
	}
  
	iir = *(pSerDev->iir);
   
	if ((iir & UART_IIR_INTID_RDA) || (iir & UART_IIR_INTID_CTI)) { // RDA or CTI pending
		if (!receive_disable) {
	    	while (*(pSerDev->lsr) & UART_LSR_RDR) {            // Rx FIFO is not empty
	  			IN_BUF_WR(pSerDev, *(pSerDev->rbr));            // Read Rx FIFO to buffer  
			}
		} 
		else {
			// Clear Rx FIFO during half duplex transmissions
	    	while (*(pSerDev->lsr) & UART_LSR_RDR) {            // Rx FIFO is not empty
	  			dummy = *(pSerDev->rbr);            			// Read Rx FIFO to dummy  
			}
    	}
  	}
  	if ((iir & UART_IIR_INTID_THRE)) {                          // THRE pending
		//while (!(*(pSerDev->lsr) & UART_LSR_TEMT));           // Wait for transmitter to be empty
	    pSerDev->transmitting = 0;
  	}
  	NVIC_ClearPendingIRQ(UART1_IRQn);  
}

#endif /* SERIAL_PORT_IN_USE(1) */


#if SERIAL_PORT_IN_USE(2)
/*----------------------------------------------------------------------------
  UART 2 interrupt
 *---------------------------------------------------------------------------*/
static __irq void ser_irq_2 (void) { 
  SER_INFO *pSerDev = &ser_Dev[2 - SER_MINPORT];
  /* Handle serial interrupts, including writing received data to the 
     receive data register for UART 2 */
  
  volatile unsigned long iir;
  
  iir = *(pSerDev->iir);
   
  if ((iir & 0x4) || (iir & 0xC)) {                           // RDA or CTI pending
    while (*(pSerDev->lsr) & 0x01) {                          // Rx FIFO is not empty
      IN_BUF_WR(pSerDev, *(pSerDev->rbr));                    // Read Rx FIFO to buffer  
    }
  }
  if ((iir & 0x2)) {                                          // THRE pending
    pSerDev->transmitting = 0;
  }

  NVIC_ClearPendingIRQ(UART2_IRQn);   
}

#endif /* SERIAL_PORT_IN_USE(2) */


/*----------------------------------------------------------------------------
  The following functions are used by the printf / scanf if stdout is
  redirected to the UART. They do not need to be modified
 *---------------------------------------------------------------------------*/

#ifndef UART_PRINTF_BAUDRATE 
#define UART_PRINTF_BAUDRATE 115200
#endif

#ifndef UART_PRINTF_PORT 
#define UART_PRINTF_PORT 0
#endif

#if ((UART_PRINTF_PORT != 0) && (UART_PRINTF_PORT != 1))
  #error "UART_PRINTF_PORT must be defined as either 0 or 1!"
#endif

static int printfDevNo;

void init_serial (void)  {               /* Initialize Serial Interface       */

  ARM_Serial_OpenPort (UART_PRINTF_PORT, &printfDevNo);
  ARM_Serial_InitPort(printfDevNo,
               UART_PRINTF_BAUDRATE,
               8,   /* 8 data bits */ 
               0,   /* No partity */ 
               0);  /* 1 stop bit */
}

/* implementation of putchar (also used by printf function to output data)    */
#define CR     0x0D
int sendchar (int ch)  {                 /* Write character to UART    */
  int length = 1;
  int cr = CR;

  if (ch == '\n')  {
	ARM_Serial_Write (printfDevNo, (const char *)&cr, &length);
  }
  ARM_Serial_Write (printfDevNo, (const char *)&ch, &length);
  return (ch);
}


int getkey (void)  {                     /* Read character from UART   */
  int availChar = 0;
  int length = 1;
  char data;

  do {
    ARM_Serial_AvailChar(printfDevNo, &availChar);
  } while (!availChar);

  ARM_Serial_Read (printfDevNo, &data, &length);
  return (data);
}

int kbhit (void)  {                     /* Read character from UART   */
  int availChar = 0;

  ARM_Serial_AvailChar(printfDevNo, &availChar);
  if (availChar < 0) {
	availChar = 0;
  }
  return (availChar ? 1 : 0);
}

/*********************************************************************//**
 * @brief		GPDMA interrupt handler sub-routine
 * @param[in]	None
 * @return 		None
 **********************************************************************/
void DMA_IRQHandler (void)
{
	SER_INFO *pSerDev = &ser_Dev[0 - SER_MINPORT];
#if UART0DMA
	//voor UART0 via DMA
	if (GPDMA_IntGetStatus(GPDMA_STAT_INT, 4)) {
		pSerDev = &ser_Dev[0 - SER_MINPORT];
		//dmaChannel4Rdy++;
		//check interrupt status on channel 4
		// Check counter terminal status
		if(GPDMA_IntGetStatus(GPDMA_STAT_INTTC, 4)) {
			// Clear terminate counter Interrupt pending
			GPDMA_ClearIntPending (GPDMA_STATCLR_INTTC, 4);
			if(uart0CurrentDmaBuf==CURRENT_PING) {
			 	//buffer 1 is verstuurd
				uart0DmaPingBufLen=0;				
				//memset(&uart0DmaPingBuf[0],0,PINGPONG_BUFF);
				uart0CurrentDmaBuf=NO_CURRENT_BUF;
				uart0DmaPingBufBusy=0;				
			}
			else if(uart0CurrentDmaBuf==CURRENT_PONG) {
			 	//buffer2 is verstuurd
				uart0DmaPongBufLen=0;		   				
				//memset(&uart0DmaPongBuf[0],0,PINGPONG_BUFF);
				uart0CurrentDmaBuf=NO_CURRENT_BUF;
				uart0DmaPongBufBusy=0;				
			}
			GPDMA_ChannelCmd(4, DISABLE);

			//dmaChannel4Rdy++;
			if(uart0DmaPingBufLen&&!uart0DmaPingBufBusy) {
			 	//verstuur ping buffer alsnog
				pSerDev->transmitting=1;
			 	uart0DmaPingBufBusy=1;
				uart0CurrentDmaBuf=CURRENT_PING;
				// Source memory
				GPDMACfg0.SrcMemAddr = (uint32_t) uart0DmaPingBuf;
				// Transfer size
				GPDMACfg0.TransferSize = uart0DmaPingBufLen;
				GPDMA_Setup(&GPDMACfg0);

				dmaChannel4Rdy=0;
				GPDMA_ChannelCmd(4, ENABLE);

			} else if (uart0DmaPongBufLen&&!uart0DmaPongBufBusy) {
				//verstuur pong buffer alsnog
				pSerDev->transmitting=1;
				uart0DmaPongBufBusy=1;
				uart0CurrentDmaBuf=CURRENT_PONG;
				// Source memory
				GPDMACfg0.SrcMemAddr = (uint32_t) uart0DmaPongBuf;
				// Transfer size
				GPDMACfg0.TransferSize = uart0DmaPongBufLen;
				GPDMA_Setup(&GPDMACfg0);

				dmaChannel4Rdy=0;
				GPDMA_ChannelCmd(4, ENABLE);
			} else { 	 
				//niets meer te versturen of nog niet klaar.
				dmaChannel4Rdy=1;
			}
			//dmaChannel4Rdy++;
			//dmaRdCh_TermianalCnt++;
		}
		if (GPDMA_IntGetStatus(GPDMA_STAT_INTERR, 4)) {
			// Clear error counter Interrupt pending
			GPDMA_ClearIntPending (GPDMA_STATCLR_INTERR, 4);
			dmaChannel4Err++;
			//dmaRdCh_ErrorCnt++;
		}
	}
#endif // UART0DMA
#if UART1DMA
	//voor UART via DMA
	if (GPDMA_IntGetStatus(GPDMA_STAT_INT, 3)) {
		pSerDev = &ser_Dev[1 - SER_MINPORT];
		//check interrupt status on channel 3
		// Check counter terminal status
		if(GPDMA_IntGetStatus(GPDMA_STAT_INTTC, 3))	{
			// Clear terminate counter Interrupt pending
			GPDMA_ClearIntPending (GPDMA_STATCLR_INTTC, 3);
			if(uart1CurrentDmaBuf==CURRENT_PING) {
			 	//buffer 1 is verstuurd
				uart1DmaPingBufLen=0;				
				//memset(&uart1DmaPingBuf[0],0,PINGPONG_BUFF);
				uart1CurrentDmaBuf=NO_CURRENT_BUF;
				uart1DmaPingBufBusy=0;
				//isr_sem_send (&semaphore1_1);				
			}
			else if(uart1CurrentDmaBuf==CURRENT_PONG) {
			 	//buffer2 is verstuurd
				uart1DmaPongBufLen=0;		   				
				//memset(&uart1DmaPongBuf[0],0,PINGPONG_BUFF);
				uart1CurrentDmaBuf=NO_CURRENT_BUF;
				uart1DmaPongBufBusy=0;
				//isr_sem_send (&semaphore1_2);				
			}
			GPDMA_ChannelCmd(3, DISABLE);

			if(uart1DmaPingBufLen&&!uart1DmaPingBufBusy) {
			 	//verstuur ping buffer alsnog
				//if(os_sem_wait (semaphore1_1,0)!=OS_R_TMO)
				//{
				pSerDev->transmitting=1;
				uart1DmaPingBufBusy=1;
				uart1CurrentDmaBuf=CURRENT_PING;
				// Source memory
				GPDMACfg1.SrcMemAddr = (uint32_t) uart1DmaPingBuf;
				// Transfer size
				GPDMACfg1.TransferSize = uart1DmaPingBufLen;
				GPDMA_Setup(&GPDMACfg1);

				dmaChannel3Rdy=0;
				GPDMA_ChannelCmd(3, ENABLE);
			//	} else
			//	{
			//		   dmaChannel3Rdy++;
			//	}
			} else if (uart1DmaPongBufLen&&!uart1DmaPongBufBusy) {
				//if(os_sem_wait (semaphore1_2,0)!=OS_R_TMO)
				//{
				//verstuur pong buffer alsnog
				pSerDev->transmitting=1;
				uart1DmaPongBufBusy=1;
				//os_sem_wait (semaphore1_2,0xFFFF);
				uart1CurrentDmaBuf=CURRENT_PONG;										  	
				// Source memory
				GPDMACfg1.SrcMemAddr = (uint32_t) uart1DmaPongBuf;
				// Transfer size
				GPDMACfg1.TransferSize = uart1DmaPongBufLen;
				GPDMA_Setup(&GPDMACfg1);

				dmaChannel3Rdy=0;
				GPDMA_ChannelCmd(3, ENABLE);
				//} else
				//{
				// 	dmaChannel3Rdy++;
			//}
			} else { 	 
				//niets meer te versturen of nog niet klaar.
				dmaChannel3Rdy=1;
			}
			//dmaChannel3Rdy++;
			//dmaRdCh_TermianalCnt++;
		}
		if (GPDMA_IntGetStatus(GPDMA_STAT_INTERR, 3)) {
			// Clear error counter Interrupt pending
			GPDMA_ClearIntPending (GPDMA_STATCLR_INTERR, 3);
			dmaChannel3Err++;
			//dmaRdCh_ErrorCnt++;
		}
	}
#endif // UART1DMA
}


int UART_DMA_Send(int serDevNo, const char* buffer,int length)
{
  	U8 destination;
	SER_INFO *pSerDev;

  	NVIC_DisableIRQ(DMA_IRQn);

	serDevNo -= 1;                                  /* Convert the serDevNo to the port number */
	if ( (serDevNo < (SER_MINPORT)) ||              /* check the serial device number */
			(serDevNo > (SER_MAXPORT)) ) {
    	return (SER_DEVNOTKNOWN_ERR);
	}
	pSerDev = &ser_Dev[(serDevNo - SER_MINPORT)];

	switch(serDevNo) {
		case 0:	destination = GPDMA_CONN_UART0_Tx; break;
		case 1:	destination = GPDMA_CONN_UART1_Tx; break;
		default: return (SER_DEVNOTKNOWN_ERR);
	}

#if UART1DMA
	if(serDevNo==1) {
		pSerDev->transmitting=1;
		if(!dmaChannel3Rdy) {
			//__disable_irq();
			//nog bezig met DMA
			if(uart1CurrentDmaBuf==CURRENT_PONG) {
				//vul ping buffer aan
				if((uart1DmaPingBufLen+length)<PINGPONG_BUFF) {
					uart1DmaPingBufBusy=1;
					//os_sem_wait (semaphore1_1,0xFFFF);
					memcpy(&uart1DmaPingBuf[uart1DmaPingBufLen],buffer,length);	//vul ping buffer aan
					uart1DmaPingBufLen=uart1DmaPingBufLen+length; //nieuwe lengte voor ping buffer
					uart1DmaPingBufBusy=0;
					//os_sem_send(&semaphore1_1);
				}
			} 
			else if(uart1CurrentDmaBuf==CURRENT_PING) {
				//vul pong buffer aan
				if((uart1DmaPongBufLen+length)<PINGPONG_BUFF) {
					uart1DmaPongBufBusy=1;
					//os_sem_wait (semaphore1_2,0xFFFF);			
					memcpy(&uart1DmaPongBuf[uart1DmaPongBufLen],buffer,length); //vul pong buffer aan
					uart1DmaPongBufLen=uart1DmaPongBufLen+length; //nieuwe lengte voor pong buffer
					uart1DmaPongBufBusy=0;
					//os_sem_send(&semaphore1_2);	
				}
			}
			//NVIC_EnableIRQ(DMA_IRQn);
			//__enable_irq();
		} 
		else {
			//DMA is klaar		
			if(uart1DmaPingBufLen==0) {
				if(length>PINGPONG_BUFF) {
					length=PINGPONG_BUFF;
				}
				uart1DmaPingBufBusy=1;
				//os_sem_wait (semaphore1_1,0xFFFF);
				memcpy(&uart1DmaPingBuf[0],buffer,length);	//vul ping buffer
				uart1DmaPingBufLen=length; //nieuwe lengte voor ping buffer	
				uart1DmaPingBufBusy=0;
				//os_sem_send(&semaphore1_1);
			}
		} 
		/*while(!dmaChannel3Rdy) {
			os_dly_wait(1);
		}*/ 
		if(!uart1CurrentDmaBuf) {
			//geen DMA bezig, dus kan gaan versturen waar data in zit.
			GPDMA_ChannelCmd(3, DISABLE);
			GPDMACfg1.ChannelNum = 3;
			if(uart1DmaPingBufLen) {
				uart1DmaPingBufBusy=1;
				//os_sem_wait (semaphore1_1,0xFFFF);
				// Source memory
				GPDMACfg1.SrcMemAddr = (uint32_t) uart1DmaPingBuf;
				// Transfer size
				GPDMACfg1.TransferSize = uart1DmaPingBufLen;
				uart1CurrentDmaBuf=CURRENT_PING;
			} 
			else if(uart1DmaPongBufLen) {
				uart1DmaPongBufBusy=1;
				//os_sem_wait (semaphore1_2,0xFFFF);
				// Source memory
				GPDMACfg1.SrcMemAddr = (uint32_t) uart1DmaPongBuf;
				// Transfer size
				GPDMACfg1.TransferSize = uart1DmaPongBufLen;
				uart1CurrentDmaBuf=CURRENT_PONG;
			}									   
	
			// Destination memory - don't care
			GPDMACfg1.DstMemAddr = 0;
			
			// Transfer width - don't care
			GPDMACfg1.TransferWidth = 0;
			// Transfer type
			GPDMACfg1.TransferType = GPDMA_TRANSFERTYPE_M2P;
			// Source connection - don't care
			GPDMACfg1.SrcConn = 0;
			// Destination connection
			GPDMACfg1.DstConn = destination;
			// Linker List Item - unused
			GPDMACfg1.DMALLI = 0;
			// Setup channel with given parameter
			GPDMA_Setup(&GPDMACfg1);
			
			dmaChannel3Rdy=0;
	
			// Enable GPDMA channel 1
			GPDMA_ChannelCmd(3, ENABLE);
			//NVIC_EnableIRQ(DMA_IRQn);
		}
	} 
	//einde DMA channel 3
#endif // UART1DMA
#if UART0DMA
	if(serDevNo==0) {
		pSerDev->transmitting=1;
		if(!dmaChannel4Rdy) {
			//NVIC_DisableIRQ(DMA_IRQn);
			//__disable_irq();
			//nog bezig met DMA
	 		if(uart0CurrentDmaBuf==CURRENT_PONG) {
				//vul ping buffer aan
				if((uart0DmaPingBufLen+length)<PINGPONG_BUFF) {
					uart0DmaPingBufBusy=1;
					//os_sem_wait (semaphore2_1,0xFFFF);
					memcpy(&uart0DmaPingBuf[uart0DmaPingBufLen],buffer,length);	//vul ping buffer aan
					uart0DmaPingBufLen=uart0DmaPingBufLen+length; //nieuwe lengte voor ping buffer
					uart0DmaPingBufBusy=0;
					//os_sem_send(&semaphore2_1);
				}
			} 
			else if(uart0CurrentDmaBuf==CURRENT_PING) {
				//vul pong buffer aan
				if((uart0DmaPongBufLen+length)<PINGPONG_BUFF) {
					uart0DmaPongBufBusy=1;
					//os_sem_wait (semaphore2_2,0xFFFF);			
					memcpy(&uart0DmaPongBuf[uart0DmaPongBufLen],buffer,length); //vul pong buffer aan
					uart0DmaPongBufLen=uart0DmaPongBufLen+length; //nieuwe lengte voor pong buffer
					uart0DmaPongBufBusy=0;
					//os_sem_send(&semaphore2_2);	
				}
			}
			//NVIC_EnableIRQ(DMA_IRQn);
			//__enable_irq();
		} 
		else {
			//DMA is klaar		
			if(uart0DmaPingBufLen==0) {
				if(length>PINGPONG_BUFF) {
			 		length=PINGPONG_BUFF;
				}
				uart0DmaPingBufBusy=1;
				//os_sem_wait (semaphore2_1,0xFFFF);
				memcpy(&uart0DmaPingBuf[0],buffer,length);	//vul ping buffer 
				uart0DmaPingBufLen=length; //nieuwe lengte voor ping buffer	
				uart0DmaPingBufBusy=0;
				//os_sem_send(&semaphore2_1);
			}
		} 
		/*while(!dmaChannel3Rdy) {
			os_dly_wait(1);
		}*/ 
		if(!uart0CurrentDmaBuf) {
			//geen DMA bezig, dus kan gaan versturen waar data in zit.
  			GPDMA_ChannelCmd(4, DISABLE);
			GPDMACfg0.ChannelNum = 4;
			if(uart0DmaPingBufLen) {
				uart0DmaPingBufBusy=1;
				//os_sem_wait (semaphore2_1,0xFFFF);
				// Source memory
				GPDMACfg0.SrcMemAddr = (uint32_t) uart0DmaPingBuf;
				// Transfer size
				GPDMACfg0.TransferSize = uart0DmaPingBufLen;
				uart0CurrentDmaBuf=CURRENT_PING;
			} 
			else if(uart0DmaPongBufLen) {
				uart0DmaPongBufBusy=1;
				// os_sem_wait (semaphore2_2,0xFFFF);
				// Source memory
				GPDMACfg0.SrcMemAddr = (uint32_t) uart0DmaPongBuf;
				// Transfer size
				GPDMACfg0.TransferSize = uart0DmaPongBufLen;
				uart0CurrentDmaBuf=CURRENT_PONG;
			}									   

			// Destination memory - don't care
			GPDMACfg0.DstMemAddr = 0;

			// Transfer width - don't care
			GPDMACfg0.TransferWidth = 0;
			// Transfer type
			GPDMACfg0.TransferType = GPDMA_TRANSFERTYPE_M2P;
			// Source connection - don't care
			GPDMACfg0.SrcConn = 0;
			// Destination connection
			GPDMACfg0.DstConn = destination;
			// Linker List Item - unused
			GPDMACfg0.DMALLI = 0;
			// Setup channel with given parameter
			GPDMA_Setup(&GPDMACfg0);

			dmaChannel4Rdy=0;

    		// Enable GPDMA channel 1
    		GPDMA_ChannelCmd(4, ENABLE);
			//NVIC_EnableIRQ(DMA_IRQn);
		}
	}
#endif // UART0DMA
   	NVIC_EnableIRQ(DMA_IRQn);
	return length;
}

int UART_NoDMA_Send(int serDevNo, uint8_t *buffer, uint32_t bytesToWrite)
{
  	SER_INFO *pSerDev;
	LPC_UART_TypeDef* UARTx;

	serDevNo -= 1;                                  /* Convert the serDevNo to the port number */
	if ( (serDevNo < (SER_MINPORT)) ||              /* check the serial device number */
    	   (serDevNo > (SER_MAXPORT))   ) {
		return (SER_DEVNOTKNOWN_ERR);
  	}

  	pSerDev = &ser_Dev[(serDevNo - SER_MINPORT)];

	switch (serDevNo) {
		case 0: 
			UARTx=LPC_UART0; 
			break;
		case 1: 
			UARTx=(LPC_UART_TypeDef*)LPC_UART1; 
			break;
		case 2: 
			UARTx=LPC_UART2; 
			break;
	   	default: 
			return (SER_DEVNOTKNOWN_ERR);
	}
	pSerDev->transmitting = 1;
	UART_Send(UARTx, buffer , bytesToWrite, BLOCKING);
	return bytesToWrite;
}

void Set_UART_Pin_Functions(U8 Port, U8 Pin, U8 Func)
{
	if(Port==0&&Pin==2)		UART0_0_2=Func;
	if(Port==0&&Pin==3)		UART0_0_3=Func;
	if(Port==0&&Pin==15)	UART1_0_15=Func;
	if(Port==0&&Pin==16)	UART1_0_16=Func;
	if(Port==0&&Pin==17)	UART1_0_17=Func;
	if(Port==0&&Pin==18)	UART1_0_18=Func;
	if(Port==0&&Pin==22)	UART1_0_22=Func;
	if(Port==0&&Pin==10)	UART2_0_10=Func;
	if(Port==0&&Pin==11)	UART2_0_11=Func;
	if(Port==2&&Pin==0)		UART1_2_0=Func;
	if(Port==2&&Pin==1)		UART1_2_1=Func;
	if(Port==2&&Pin==2)		UART1_2_2=Func;
	if(Port==2&&Pin==3)		UART1_2_3=Func;
	if(Port==2&&Pin==4)		UART1_2_4=Func;
	if(Port==2&&Pin==5)		UART1_2_5=Func;
	if(Port==2&&Pin==6)		UART1_2_6=Func;
	if(Port==2&&Pin==7)		UART1_2_7=Func;
	if(Port==2&&Pin==8)		UART2_2_8=Func;
	if(Port==2&&Pin==9)		UART2_2_9=Func;
}

void Set_UART1_RS485_Settings(U8 SettingsNr, U8 Value)
{
	switch(SettingsNr)
	{
		case 0:	UART1_RS485_Settings.NormalMultiDropMode_State = (Value ? ENABLE : DISABLE); break;
		case 1:	UART1_RS485_Settings.Rx_State = (Value ? ENABLE : DISABLE);	break;
		case 2:	UART1_RS485_Settings.AutoAddrDetect_State = (Value ? ENABLE : DISABLE);	break;
		case 3: UART1_RS485_Settings.AutoDirCtrl_State = (Value ? ENABLE : DISABLE); break;
		case 4: UART1_RS485_Settings.DirCtrlPin = (Value ? UART1_RS485_DIRCTRL_DTR : UART1_RS485_DIRCTRL_RTS); break;
		case 5: UART1_RS485_Settings.DirCtrlPol_Level = (Value ? SET : RESET); break;
		case 6:	UART1_RS485_Settings.MatchAddrValue = Value; break;
		case 7: UART1_RS485_Settings.DelayValue = Value; break;
		case 8: UART1_RS485_HalfDuplex = (Value ? 1 : 0); break;
	 	default: break;
	}
}

void Set_UART1_Flow(U8 SettingsNr, U8 Value)
{
 	switch(SettingsNr)
	{
	 	case 0: UART1_CTS = Value; break;
	 	case 1: UART1_RTS = Value; break;
	 	default: break;
	}
}
