/*-----------------------------------------------------------------------------
-- leds.h
--
-- Copyright INCAA Computers BV, 2021
-- This source describes Open Hardware and is licensed under the
-- CERN-OHL-W v2
-- Source location: https://ohwr.org/project/pxct
-----------------------------------------------------------------------------*/

#ifndef SRC_LEDS_H_
#define SRC_LEDS_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

#define LED_OFF 	0
#define LED_GREEN 	1
#define LED_RED 	2
#define LED_YELLOW 	3

/* Creation en deletion */
void led_Init(void);

/* Status */
void led_StatRG(uint8_t value);
void led_StatY(bool value);

#ifdef __cplusplus
}
#endif

#endif /* SRC_LEDS_H_ */

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
