/*-----------------------------------------------------------------------------
-- leds.c
--
-- Copyright INCAA Computers BV, 2021
-- This source describes Open Hardware and is licensed under the
-- CERN-OHL-W v2
-- Source location: https://ohwr.org/project/pxct
-----------------------------------------------------------------------------*/

#include <stdbool.h>
#include "lpc_types.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"
#include "leds.h"

#define LED_PIN_STATY	10
#define LED_PIN_STATG	8
#define LED_PIN_STATR	9

void led_Init(void)
{
	PINSEL_CFG_Type pinsel_cfg;
	uint32_t bitValue;

	pinsel_cfg.Portnum = PINSEL_PORT_1;
	pinsel_cfg.Funcnum = 0;
	pinsel_cfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	pinsel_cfg.Pinnum = LED_PIN_STATY;
	PINSEL_ConfigPin(&pinsel_cfg);
	pinsel_cfg.Pinnum = LED_PIN_STATG;
	PINSEL_ConfigPin(&pinsel_cfg);
	pinsel_cfg.Pinnum = LED_PIN_STATR;
	PINSEL_ConfigPin(&pinsel_cfg);

	bitValue = ((1 << LED_PIN_STATY) | (1 << LED_PIN_STATG) | (1 << LED_PIN_STATR));
	GPIO_SetDir(PINSEL_PORT_1, bitValue, 1);
	GPIO_ClearValue(PINSEL_PORT_1, bitValue);
}

void led_StatRG(uint8_t value)
{
	switch (value) {
		case LED_GREEN :
			// enable Green only
			GPIO_ClearValue(PINSEL_PORT_1,(1 << LED_PIN_STATG));
			GPIO_SetValue(PINSEL_PORT_1,(1 << LED_PIN_STATR));
			break;
		case LED_RED :
			// enable Red only
			GPIO_ClearValue(PINSEL_PORT_1,(1 << LED_PIN_STATR));
			GPIO_SetValue(PINSEL_PORT_1,(1 << LED_PIN_STATG));
			break;
		case LED_YELLOW :
			// enable Red and Green
			GPIO_ClearValue(PINSEL_PORT_1,(1 << LED_PIN_STATR));
			GPIO_ClearValue(PINSEL_PORT_1,(1 << LED_PIN_STATG));
			break;
		default:
			// disable both 
			GPIO_SetValue(PINSEL_PORT_1,(1 << LED_PIN_STATR));
			GPIO_SetValue(PINSEL_PORT_1,(1 << LED_PIN_STATG));
	}
}

void led_StatY(bool value)
{
	if (value) {
		GPIO_SetValue(PINSEL_PORT_1,(1 << LED_PIN_STATY));
	}
	else {
		GPIO_ClearValue(PINSEL_PORT_1,(1 << LED_PIN_STATY));
	}
}

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
