/*-----------------------------------------------------------------------------
-- pxct.h
--
-- Copyright INCAA Computers BV, 2021
-- This source describes Open Hardware and is licensed under the
-- CERN-OHL-W v2
-- Source location: https://ohwr.org/project/pxct
-----------------------------------------------------------------------------*/

#ifndef SRC_PXCT_H_
#define SRC_PXCT_H_

#ifdef __cplusplus
extern "C"
{
#endif

#define MODULE_ID			"PXCT"

#define MODULE_ART_PER		"CPE-PXCT-19916"
#define MODULE_ART_TIM		"CPE-PXCT-19917"

#define MODULE_DESC			"PXi Communication Tester"
#define MODULE_VERSION		"1.0"

#define MODULE_MANUFACTURER	"INCAA Computers"

#define MODULE_SPLASH		"\n* %s " MODULE_VERSION " - " MODULE_DESC " - " MODULE_MANUFACTURER " *\n"

#ifdef __cplusplus
}
#endif

#endif /* SRC_PXCT_H_ */

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
