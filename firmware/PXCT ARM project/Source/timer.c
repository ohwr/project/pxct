/*-----------------------------------------------------------------------------
-- timer.c
--
-- Copyright INCAA Computers BV, 2021
-- This source describes Open Hardware and is licensed under the
-- CERN-OHL-W v2
-- Source location: https://ohwr.org/project/pxct
-----------------------------------------------------------------------------*/

#include <stdbool.h>
#include "lpc17xx_timer.h"
#include "timer.h"

void TIMER0_IRQHandler(void);
						
volatile bool timer0_flag = 0;

void Timer0_Init(uint32_t us) 
{
	TIM_TIMERCFG_Type TIM_ConfigStruct;
	TIM_MATCHCFG_Type TIM_MatchStruct;

	TIM_ConfigStructInit(TIM_TIMER_MODE, &TIM_ConfigStruct);
	TIM_Init(LPC_TIM0, TIM_TIMER_MODE, &TIM_ConfigStruct);	

	TIM_MatchStruct.MatchChannel = 0;
	TIM_MatchStruct.IntOnMatch = ENABLE; // interrupt will be generated if match
	TIM_MatchStruct.StopOnMatch = DISABLE;
	TIM_MatchStruct.ResetOnMatch = DISABLE;
	TIM_MatchStruct.ExtMatchOutputType = TIM_EXTMATCH_NOTHING;	// Do nothing for external output pin if match
	TIM_MatchStruct.MatchValue = us; // micro seconds

	TIM_ConfigMatch(LPC_TIM0, &TIM_MatchStruct);

	// preemption = 1, sub-priority = 1
	NVIC_SetPriority(TIMER0_IRQn, ((0x01<<3)|0x01));
	// Enable interrupt for timer 0
	NVIC_EnableIRQ(TIMER0_IRQn);
	// To start timer 0
	TIM_Cmd(LPC_TIM0, ENABLE);
}

void Timer0_DeInit(void) 
{
	TIM_Cmd(LPC_TIM0, DISABLE);
	NVIC_DisableIRQ(TIMER0_IRQn);
	TIM_DeInit(LPC_TIM0);
}

void TIMER0_IRQHandler(void)
{
	if (TIM_GetIntStatus(LPC_TIM0, TIM_MR0_INT)) {
		TIM_Cmd(LPC_TIM0, DISABLE);
		TIM_ClearIntPending(LPC_TIM0, TIM_MR0_INT);
		TIM_ResetCounter(LPC_TIM0);
		timer0_flag = true;
		TIM_Cmd(LPC_TIM0, ENABLE);
	}
}

bool Timer0_GetFlag(void)
{
	bool flag;

	flag = timer0_flag;
	if (flag) {
		NVIC_DisableIRQ(TIMER0_IRQn);
		timer0_flag = false;
		NVIC_EnableIRQ(TIMER0_IRQn);
		return true;
	}
	return false;
}

void Timer0_WaitForFlag(void)
{
	while (!Timer0_GetFlag()) {
		Sleep();
	}
}

void Timer0_WaitTime(void)
{
	while (!Timer0_GetFlag()) {
	}
}

void Sleep(void)
{
	SYSTICK_Cmd(DISABLE);
	CLKPWR_Sleep();
	SYSTICK_Cmd(ENABLE);
}

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
