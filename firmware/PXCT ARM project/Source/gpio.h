/*-----------------------------------------------------------------------------
-- gpio.h
--
-- Copyright INCAA Computers BV, 2021
-- This source describes Open Hardware and is licensed under the
-- CERN-OHL-W v2
-- Source location: https://ohwr.org/project/pxct
-----------------------------------------------------------------------------*/

#ifndef SRC_GPIO_H_
#define SRC_GPIO_H_

#include <stdbool.h>
#include "lpc_types.h"

#define GPIO_PXI_PORT_GA             0
#define GPIO_PXI_PIN_GA0             6
#define GPIO_PXI_PIN_GA1             7
#define GPIO_PXI_PIN_GA2             8
#define GPIO_PXI_PIN_GA3             9
#define GPIO_PXI_PIN_GA4             10

#define GPIO_PXI_GA_POS            	 6
#define GPIO_PXI_GA_WIDTH			 5
#define GPIO_PXI_GA					 0x1F
#define GPIO_PXI_GA0				 0x01
#define GPIO_PXI_GA1				 0x02
#define GPIO_PXI_GA2				 0x04
#define GPIO_PXI_GA3				 0x08
#define GPIO_PXI_GA4				 0x10

#define GPIO_PXI_PORT_PXI_TRIG       2
#define GPIO_PXI_PIN_PXI_TRIG0       0
#define GPIO_PXI_PIN_PXI_TRIG1       1
#define GPIO_PXI_PIN_PXI_TRIG2       2
#define GPIO_PXI_PIN_PXI_TRIG3       3
#define GPIO_PXI_PIN_PXI_TRIG4       4
#define GPIO_PXI_PIN_PXI_TRIG5       5
#define GPIO_PXI_PIN_PXI_TRIG6       6
#define GPIO_PXI_PIN_PXI_TRIG7       7

#define GPIO_PXI_TRIG_POS            0
#define GPIO_PXI_TRIG_WIDTH			 8
#define GPIO_PXI_TRIG				 0xFF
#define GPIO_PXI_TRIG0				 0x01
#define GPIO_PXI_TRIG1				 0x02
#define GPIO_PXI_TRIG2				 0x04
#define GPIO_PXI_TRIG3				 0x08
#define GPIO_PXI_TRIG4				 0x10
#define GPIO_PXI_TRIG5				 0x20
#define GPIO_PXI_TRIG6				 0x40
#define GPIO_PXI_TRIG7				 0x80

#define GPIO_PXI_PORT_PXI_LBL6       2
#define GPIO_PXI_PIN_PXI_LBL6        8

#define GPIO_PXI_PORT_PXI_LBR6       2
#define GPIO_PXI_PIN_PXI_LBR6        9

#define GPIO_PXI_PORT_PXI_STAR_IN    0
#define GPIO_PXI_PIN_PXI_STAR_IN     22

#define GPIO_PXI_PORT_PXI_STAR_OUT   1
#define GPIO_PXI_PIN_PXI_STAR_OUT    18
#define GPIO_PXI_PIN_PXI_STAR_EN     19
#define GPIO_PXI_PIN_PXI_STAR_SEL    20

#define GPIO_PXI_PORT_PXIE_DSTAR_OUT 1
#define GPIO_PXI_PIN_PXIE_DSTAR_OUT  28
#define GPIO_PXI_PIN_PXIE_DSTAR_SEL  29

#define GPIO_PORT_TRIG           	 1
#define GPIO_PIN_TRIG_IN         	 22
#define GPIO_PIN_TRIG_OUT        	 23
#define GPIO_PIN_TRIG_OE         	 24
#define GPIO_PIN_TRIG_TSEL       	 25

#define GPIO_PORT_TEST				 1
#define GPIO_PIN_TEST				 26

#define GPIO_PORT_MODE				 1
#define GPIO_PIN_MODE				 31

#define GPIO_IN						 1		 			
#define GPIO_OUT					 0


void gpio_Init(void);
uint32_t gpio_ReadBits(uint8_t port, int pos, int width);
uint32_t gpio_ReadBit(uint8_t port, uint8_t pin);
void gpio_WriteBits(uint8_t port, int pos, int width, uint32_t value);
void gpio_WriteBit(uint8_t port, uint8_t pin, bool level);

void gpio_WriteBitsDir(uint8_t port, int pos, int width, uint32_t value);
void gpio_WriteBitDir(uint8_t port, uint8_t pin, bool dir);

// GA
static __inline uint32_t gpio_GetPxiGa(void) { return gpio_ReadBits(GPIO_PXI_PORT_GA, GPIO_PXI_GA_POS, GPIO_PXI_GA_WIDTH); }

// PXI_TRIG[7:0]
static __inline void gpio_SetPxiDir(uint32_t value) { gpio_WriteBitsDir(GPIO_PXI_PORT_PXI_TRIG, GPIO_PXI_TRIG_POS, GPIO_PXI_TRIG_WIDTH, value); }
static __inline uint32_t gpio_GetPxi(void) { gpio_ReadBits(GPIO_PXI_PORT_PXI_TRIG, GPIO_PXI_TRIG_POS, GPIO_PXI_TRIG_WIDTH); }
static __inline void gpio_SetPxi(uint32_t value) { gpio_WriteBits(GPIO_PXI_PORT_PXI_TRIG, GPIO_PXI_TRIG_POS, GPIO_PXI_TRIG_WIDTH, value); }

// PXI_LBL6
static __inline void gpio_SetLblDir(uint32_t value) { gpio_WriteBitDir(GPIO_PXI_PORT_PXI_LBL6, GPIO_PXI_PIN_PXI_LBL6, value); }
static __inline uint32_t gpio_GetLbl(void) { gpio_ReadBit(GPIO_PXI_PORT_PXI_LBL6, GPIO_PXI_PIN_PXI_LBL6); }
static __inline void gpio_SetLbl(uint32_t value) { gpio_WriteBit(GPIO_PXI_PORT_PXI_LBL6, GPIO_PXI_PIN_PXI_LBL6, value); }

// PX_LBR6
static __inline void gpio_SetLbrDir(uint32_t value) { gpio_WriteBitDir(GPIO_PXI_PORT_PXI_LBR6, GPIO_PXI_PIN_PXI_LBR6, value); }
static __inline uint32_t gpio_GetLbr(void) { gpio_ReadBit(GPIO_PXI_PORT_PXI_LBR6, GPIO_PXI_PIN_PXI_LBR6); }
static __inline void gpio_SetLbr(uint32_t value) { gpio_WriteBit(GPIO_PXI_PORT_PXI_LBR6, GPIO_PXI_PIN_PXI_LBR6, value); }

// PXI_STAR, PXIE_DSTAR
static __inline uint32_t gpio_GetStarIn(void) {	gpio_ReadBit(GPIO_PXI_PORT_PXI_STAR_IN, GPIO_PXI_PIN_PXI_STAR_IN); }

static __inline uint32_t gpio_GetStarOut(void) { gpio_ReadBit(GPIO_PXI_PORT_PXI_STAR_OUT, GPIO_PXI_PIN_PXI_STAR_OUT); }
static __inline void gpio_SetStarOut(uint32_t value) { gpio_WriteBit(GPIO_PXI_PORT_PXI_STAR_OUT, GPIO_PXI_PIN_PXI_STAR_OUT, value); }

static __inline uint32_t gpio_GetStarEn(void) { gpio_ReadBit(GPIO_PXI_PORT_PXI_STAR_OUT, GPIO_PXI_PIN_PXI_STAR_EN); }
static __inline void gpio_SetStarEn(uint32_t value) { gpio_WriteBit(GPIO_PXI_PORT_PXI_STAR_OUT, GPIO_PXI_PIN_PXI_STAR_EN, value); }

static __inline uint32_t gpio_GetStarSel(void) { gpio_ReadBit(GPIO_PXI_PORT_PXI_STAR_OUT, GPIO_PXI_PIN_PXI_STAR_SEL); }
static __inline void gpio_SetStarSel(uint32_t value) { gpio_WriteBit(GPIO_PXI_PORT_PXI_STAR_OUT, GPIO_PXI_PIN_PXI_STAR_SEL, value); }

static __inline uint32_t gpio_GetDStarOut(void) { gpio_ReadBit(GPIO_PXI_PORT_PXIE_DSTAR_OUT, GPIO_PXI_PIN_PXIE_DSTAR_OUT); }
static __inline void gpio_SetDStarOut(uint32_t value) { gpio_WriteBit(GPIO_PXI_PORT_PXIE_DSTAR_OUT, GPIO_PXI_PIN_PXIE_DSTAR_OUT, value); }

static __inline uint32_t gpio_GetDStarSel(void) { gpio_ReadBit(GPIO_PXI_PORT_PXIE_DSTAR_OUT, GPIO_PXI_PIN_PXIE_DSTAR_SEL); }
static __inline void gpio_SetDStarSel(uint32_t value) { gpio_WriteBit(GPIO_PXI_PORT_PXIE_DSTAR_OUT, GPIO_PXI_PIN_PXIE_DSTAR_SEL, value); }

// TRIG
static __inline uint32_t gpio_GetTrigIn(void) { gpio_ReadBit(GPIO_PORT_TRIG, GPIO_PIN_TRIG_IN); }

static __inline uint32_t gpio_GetTrigOut(void) { gpio_ReadBit(GPIO_PORT_TRIG, GPIO_PIN_TRIG_OUT); }
static __inline void gpio_SetTrigOut(uint32_t value) { gpio_WriteBit(GPIO_PORT_TRIG, GPIO_PIN_TRIG_OUT, value); }

static __inline uint32_t gpio_GetTrigOutputEnable(void) { gpio_ReadBit(GPIO_PORT_TRIG, GPIO_PIN_TRIG_OE); }
static __inline void gpio_SetTrigOutputEnable(uint32_t value) { gpio_WriteBit(GPIO_PORT_TRIG, GPIO_PIN_TRIG_OE, value); }

static __inline uint32_t gpio_GetTrigTerm(void) { gpio_ReadBit(GPIO_PORT_TRIG, GPIO_PIN_TRIG_TSEL); }
static __inline void gpio_SetTrigTerm(uint32_t value) { gpio_WriteBit(GPIO_PORT_TRIG, GPIO_PIN_TRIG_TSEL, value); }

// TEST
static __inline void gpio_SetTest(uint32_t value) { gpio_WriteBit(GPIO_PORT_TEST, GPIO_PIN_TEST, value); }

// MODE
static __inline uint32_t gpio_GetMode(void) { gpio_ReadBit(GPIO_PORT_MODE, GPIO_PIN_MODE); }

#endif /* SRC_GPIO_H_ */

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
