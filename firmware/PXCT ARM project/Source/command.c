/*-----------------------------------------------------------------------------
-- command.c
--
-- Copyright INCAA Computers BV, 2021
-- This source describes Open Hardware and is licensed under the
-- CERN-OHL-W v2
-- Source location: https://ohwr.org/project/pxct
-----------------------------------------------------------------------------*/

#include <RTL.h>                      /* RTL kernel functions & defines      */
#include <stdio.h>                    /* standard I/O .h-file                */
#include <ctype.h>                    /* character functions                 */
#include <string.h>                   /* string and memory functions         */
#include <LPC17xx.H>                  /* LPC17xx definitions                 */

#include "lpc_types.h"
#include "lpc17xx_wdt.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_clkpwr.h"

#include "pxct.h"
#include "command.h"
#include "at_engine.h"
#include "gpio.h"
#include "leds.h"
#include "mcp3201.h"

#define ASCII_XON			0x11
#define ASCII_XOFF			0x13
#define ASCII_CR			0x0D // "\r"
#define ASCII_LF			0x0A // "\n"

#define CMD_STRING_BUF_SIZE	80
#define COMMANDS_MAX		50

#define CMD_ERR				0
#define CMD_OK				1

#define NO_TEST				NULL
#define NO_GET				NULL
#define NO_SET				NULL
#define NO_CONFIRM			NULL
#define NO_EXEC				NULL
	
static int 	menuGate;
static char commandBuffer[CMD_STRING_BUF_SIZE];
static char responseBuffer[CMD_STRING_BUF_SIZE];
static T_AT_storage  commands[COMMANDS_MAX];
	
typedef uint8_t ( *cmdTest_t )( uint32_t value);
typedef uint32_t ( *cmdGet_t )( void );
typedef void ( *cmdSet_t )( uint32_t value);
typedef uint8_t ( *cmdConfirm_t )( void );
typedef uint8_t ( *cmdExec_t )( void );

typedef struct
{
	cmdTest_t Test;
	cmdGet_t Get;
	cmdSet_t Set;
	cmdConfirm_t Confirm;
	cmdExec_t Exec;
}
cmdFunc_t;

/*-----------------------------------------------------------------------------            
        Private Function Definitions
 ------------------------------------------------------------------------------*/

char InputChar(void)
{
	int	i=0;
	char ch=0;

	while (i < 1) {
		ch = getchar();
		if (ch != ASCII_XON && ch != ASCII_XOFF) {
			i++;
		}
	}
	//printf("%c", ch);
	return ch;
}

int InputDecValue(void)
{
	int	i = 0;
	char ch, str[11];

	printf("* ");
	while (i < 10) {
		ch = InputChar();
		if (!isdigit(ch)) {
			if (i==0 && (ch == '-' || ch == '+')) {
				str[i++] = ch;
			}
			else {
				break;
			}
		}
		else {
			str[i++] = ch;
		}
	}
	str[i] = '\0';
	//printf("%s\n", str);
	WDT_Feed();
	return atoi(str);
}

static int InputKey(void)
{
	int	i = 0;
	char ch, str[6];

	//printf("* ");
	while (i < 5) {
		ch = InputChar();
		if (!isdigit(ch)) {
			return -1;
		}
		else {
			str[i++] = ch;
		}
		WDT_Feed();
	}
	str[5] = '\0';
	//printf("%s\n\r", str);
	return atoi(str);
}


static bool ScanKey()
{
	int key;

	key = InputKey();
	if (key > 0 && key == 0xC0DE) {
		return true;
	}
	return false;
}

static uint8_t StringToDWord(uint32_t *value, char *str)
{
	int status;
	char *tmpStr;
	char prefix[3];
	
	tmpStr = str;
	if (strlen(tmpStr) > 2) {
		status = sscanf(tmpStr, "%2s", prefix);
		if ((strcmp(prefix, "0x") == 0) || (strcmp(prefix, "0X") == 0)) {
			tmpStr += 2;
			status = sscanf(tmpStr, "%lx", value);
		}
		else {
			status = sscanf(tmpStr, "%ld", value);
		}
	}
	else {
		status = sscanf(tmpStr, "%ld", value);
	}
	return status;
}

void OutputDelay(void)
{
	Timer0_WaitTime();
}

void WriteChar(unsigned char data_out)
{
	fputc((int)data_out, stdout);
}

/*-----------------------------------------------------------------------------
        command handler function
 ------------------------------------------------------------------------------*/
void cmdFunctionsInit(cmdFunc_t *functions, cmdTest_t Test, cmdGet_t Get, cmdSet_t Set, cmdConfirm_t Confirm, cmdExec_t Exec)
{
	functions->Test = Test;
	functions->Get = Get;
	functions->Set = Set;
	functions->Confirm = Confirm;
	functions->Exec = Exec;
}

#define RESPONSE_ERROR		0
#define RESPONSE_OK			1
#define RESPONSE_STR		2

static char rspStr[64];

void cmdResponse(int type)
{
	switch (type) {
		case RESPONSE_ERROR:
			led_StatRG(LED_RED);
			AT_rspStr("ERROR\n\r");
			break;
		case RESPONSE_OK:
			led_StatRG(LED_GREEN);
			AT_rspStr("OK\n\r");
			break;
		case RESPONSE_STR:
			led_StatRG(LED_GREEN);
			AT_rspStr(rspStr);
			AT_rspStr("\n\r");
			break;
		default:
			led_StatRG(LED_OFF);
			break;
	}
}

// Handler for decimal/hexadecimal parameter commands
static void cmdCommandHandler(char *buffer, uint8_t type, int argOff, uint32_t min, uint32_t max, cmdFunc_t *functions)
{
	uint32_t parameter;
	int rspType = RESPONSE_ERROR;
	
	switch(type) {
		case _AT_TEST: // <cmd>=?<parameter>
			if (functions->Test != NO_TEST) {
				StringToDWord(&parameter, buffer+argOff);
				//sscanf(buffer+argOff, "%d", &parameter);
				if (parameter >= min && parameter <= max) {
					if (functions->Test(parameter)) {
						rspType = RESPONSE_OK;
					}
				}
			}
			break;
		case _AT_GET: // <cmd>?
			if (functions->Get != NO_GET) {
				parameter = functions->Get();
				sprintf(rspStr, "0x%lx", parameter);
				rspType = RESPONSE_STR;
			}
			break;
		case _AT_SET: // <cmd>=<parameter>
			if (functions->Set != NO_SET) {
				StringToDWord(&parameter, buffer+argOff);
				//sscanf(buffer+argOff, "%d", &parameter);
				if (parameter >= min && parameter <= max) {
					functions->Set(parameter);
					rspType = RESPONSE_OK;
				}
			}
			break;
		case _AT_CONFIRM: // <cmd>!
			if (functions->Confirm != NO_CONFIRM) {
				if (functions->Confirm()) {
					rspType = RESPONSE_OK;
				}
			}
			break;
		case _AT_EXEC: // <cmd>
			if (functions->Exec != NO_EXEC) {
				if (functions->Exec()) {
					rspType = RESPONSE_OK;
				}
			}
			break;
		default: 
			break;
	}
	cmdResponse(rspType);
}

// Error handler
void cmdHandler_Error(char *buffer, uint8_t type, int argOff)
{
	if (strlen(buffer) > 1) {
		cmdResponse(RESPONSE_ERROR);
	}
}

// Handler for *IDN : IEEE Std 488.2-1992, identification query
void cmdHandler_IDN(char *buffer, uint8_t type, int argOff)
{
	int rspType = RESPONSE_ERROR;
	
	if (type == _AT_GET) { // *IDN?
		if (gpio_GetMode()) {
			sprintf(rspStr, "%s,%s,0,%s", MODULE_MANUFACTURER, MODULE_ART_PER, MODULE_VERSION);
		}
		else {
			sprintf(rspStr, "%s,%s,0,%s", MODULE_MANUFACTURER, MODULE_ART_TIM, MODULE_VERSION);
		}
		rspType = RESPONSE_STR;
	}
	cmdResponse(rspType);
}

// Handler for *RST : IEEE Std 488.2-1992, full preset command
uint8_t cmdHandler_RST_EXEC(void)
{
	NVIC_SystemReset();
	return CMD_OK;
}

void cmdHandler_RST(char *buffer, uint8_t type, int argOff)
{
	cmdFunc_t functions;
	//printf(":RST\n\r");
	cmdFunctionsInit(&functions, NO_TEST, NO_GET, NO_SET, NO_CONFIRM, cmdHandler_RST_EXEC);
	cmdCommandHandler(buffer, type, argOff, 0, UINT32_MAX, &functions);
}

// Handler for :PXID : (MSB) pxi_trig7[2:0], pxi_trig[7:0] (LSB) Direction
void cmdHandler_PXID(char *buffer, uint8_t type, int argOff)
{
	cmdFunc_t functions;
	//printf(":PXID\n\r");
	cmdFunctionsInit(&functions, NO_TEST, NO_GET, gpio_SetPxiDir, NO_CONFIRM, NO_EXEC);
	cmdCommandHandler(buffer, type, argOff, 0, GPIO_PXI_TRIG, &functions);
}

// Handler for :PXI : (MSB) pxi_trig7[2:0], pxi_trig[7:0] (LSB) I/O
void cmdHandler_PXI(char *buffer, uint8_t type, int argOff)
{
	cmdFunc_t functions;
	//printf(":PXI\n\r");
	cmdFunctionsInit(&functions, NO_TEST, gpio_GetPxi, gpio_SetPxi, NO_CONFIRM, NO_EXEC);
	cmdCommandHandler(buffer, type, argOff, 0, GPIO_PXI_TRIG, &functions);
}

// Handler for :LBLD : pxi_lbl6 Direction
void cmdHandler_LBLD(char *buffer, uint8_t type, int argOff)
{
	cmdFunc_t functions;
	//printf(":LBLD\n\r");
	cmdFunctionsInit(&functions, NO_TEST, NO_GET, gpio_SetLblDir, NO_CONFIRM, NO_EXEC);
	cmdCommandHandler(buffer, type, argOff, 0, 1, &functions);
}

// Handler for :LBL : pxi_lbl6 I/O
void cmdHandler_LBL(char *buffer, uint8_t type, int argOff)
{
	cmdFunc_t functions;
	//printf(":LBL\n\r");
	cmdFunctionsInit(&functions, NO_TEST, gpio_GetLbl, gpio_SetLbl, NO_CONFIRM, NO_EXEC);
	cmdCommandHandler(buffer, type, argOff, 0, 1, &functions);
}

// Handler for :LBRD : pxi_lbr6 Direction
void cmdHandler_LBRD(char *buffer, uint8_t type, int argOff)
{
	cmdFunc_t functions;
	//printf(":LBRD\n\r");
	cmdFunctionsInit(&functions, NO_TEST, NO_GET, gpio_SetLbrDir, NO_CONFIRM, NO_EXEC);
	cmdCommandHandler(buffer, type, argOff, 0, 1, &functions);
}

// Handler for :LBR : pxi_lbr6 I/O
void cmdHandler_LBR(char *buffer, uint8_t type, int argOff)
{
	cmdFunc_t functions;
	//printf(":LBR\n\r");
	cmdFunctionsInit(&functions, NO_TEST, gpio_GetLbr, gpio_SetLbr, NO_CONFIRM, NO_EXEC);
	cmdCommandHandler(buffer, type, argOff, 0, 1, &functions);
}

// Handler for :GA : pxi_ga[4:0] Input
void cmdHandler_GA(char *buffer, uint8_t type, int argOff)
{
	cmdFunc_t functions;
	//printf(":GA\n\r");
	cmdFunctionsInit(&functions, NO_TEST, gpio_GetPxiGa, NO_SET, NO_CONFIRM, NO_EXEC);
	cmdCommandHandler(buffer, type, argOff, 0, GPIO_PXI_GA, &functions);
}

// Handler for :STAR : pxi_star_in/pxi_star_out
void cmdHandler_STAR(char *buffer, uint8_t type, int argOff)
{
	cmdFunc_t functions;
	//printf(":STAR\n\r");
	cmdFunctionsInit(&functions, NO_TEST, gpio_GetStarIn, gpio_SetStarOut, NO_CONFIRM, NO_EXEC);
	cmdCommandHandler(buffer, type, argOff, 0, 1, &functions);
}

// Handler for :STARE : pxi_star_en/pxie_dstar_en
void cmdHandler_STARE(char *buffer, uint8_t type, int argOff)
{
	cmdFunc_t functions;
	//printf(":STARE\n\r");
	cmdFunctionsInit(&functions, NO_TEST, gpio_GetStarEn, gpio_SetStarEn, NO_CONFIRM, NO_EXEC);
	cmdCommandHandler(buffer, type, argOff, 0, 1, &functions);
}

// Handler for :STARS : pxi_star_sel (0:star_out, 1:pxi_clk10)
void cmdHandler_STARS(char *buffer, uint8_t type, int argOff)
{
	cmdFunc_t functions;
	//printf(":STARS\n\r");
	cmdFunctionsInit(&functions, NO_TEST, gpio_GetStarSel, gpio_SetStarSel, NO_CONFIRM, NO_EXEC);
	cmdCommandHandler(buffer, type, argOff, 0, 1, &functions);
}

// Handler for :DSTAR : pxie_dstar_out
void cmdHandler_DSTAR(char *buffer, uint8_t type, int argOff)
{
	cmdFunc_t functions;
	//printf(":DSTAR\n\r");
	cmdFunctionsInit(&functions, NO_TEST, gpio_GetDStarOut, gpio_SetDStarOut, NO_CONFIRM, NO_EXEC);
	cmdCommandHandler(buffer, type, argOff, 0, 1, &functions);
}

// Handler for :DSTARS : pxie_dstar_sel (0:dstar_out, 1:pxi_clk10)
void cmdHandler_DSTARS(char *buffer, uint8_t type, int argOff)
{
	cmdFunc_t functions;
	//printf(":DSTARS\n\r");
	cmdFunctionsInit(&functions, NO_TEST, gpio_GetDStarSel, gpio_SetDStarSel, NO_CONFIRM, NO_EXEC);
	cmdCommandHandler(buffer, type, argOff, 0, 1, &functions);
}

// Handler for :TRGD : front panel TRiGger Direction
uint32_t cmdHandler_TRGD_GET(void)
{
	uint32_t value;

	value = gpio_GetTrigOutputEnable();
	return (value ? 0 : 1);
}

void cmdHandler_TRGD_SET(uint32_t value)
{
	if (value == 0) {
		// 0 = output => remove termination, enable output
		gpio_SetTrigTerm(0);
		gpio_SetTrigOutputEnable(1);
	}
	else {
		// 1 = input => disable output
		gpio_SetTrigOutputEnable(0);
	}
}

void cmdHandler_TRGD(char *buffer, uint8_t type, int argOff)
{
	cmdFunc_t functions;
	//printf(":TRGD\n\r");l
	cmdFunctionsInit(&functions, NO_TEST, cmdHandler_TRGD_GET, cmdHandler_TRGD_SET, NO_CONFIRM, NO_EXEC);
	cmdCommandHandler(buffer, type, argOff, 0, 1, &functions);
}

// Handler for :TRGT : front panel TRiGger Termination
void cmdHandler_TRGT(char *buffer, uint8_t type, int argOff)
{
	cmdFunc_t functions;
	//printf(":TRGT\n\r");
	cmdFunctionsInit(&functions, NO_TEST, gpio_GetTrigTerm, gpio_SetTrigTerm, NO_CONFIRM, NO_EXEC);
	cmdCommandHandler(buffer, type, argOff, 0, 1, &functions);
}

// Handler for :TRG : front panel TRiGger I/O
void cmdHandler_TRG(char *buffer, uint8_t type, int argOff)
{
	cmdFunc_t functions;
	//printf(":TRG\n\r");
	cmdFunctionsInit(&functions, NO_TEST, gpio_GetTrigIn, gpio_SetTrigOut, NO_CONFIRM, NO_EXEC);
	cmdCommandHandler(buffer, type, argOff, 0, 1, &functions);
}

// Handler for :TRGL : front panel TRiGger I/O level
uint32_t cmdHandler_TRGL_GET(void)
{
	uint16_t value;

	Mcp3201_Read(&value);
	return (uint32_t)value;
}

void cmdHandler_TRGL(char *buffer, uint8_t type, int argOff)
{
	cmdFunc_t functions;
	//printf(":TRGL\n\r");
	cmdFunctionsInit(&functions, NO_TEST, cmdHandler_TRGL_GET, NO_SET, NO_CONFIRM, NO_EXEC);
	cmdCommandHandler(buffer, type, argOff, 0, 0xFFF, &functions);
}


/*-----------------------------------------------------------------------------
        Public Function Definitions
 ------------------------------------------------------------------------------*/

void cmd_Init(void)
{
	AT_initParser(WriteChar, cmdHandler_Error, commandBuffer, responseBuffer, CMD_STRING_BUF_SIZE, commands, COMMANDS_MAX);
	
	AT_saveHandler("*IDN", cmdHandler_IDN);			// IEEE Std 488.2-1992, identification query
	AT_saveHandler("*RST", cmdHandler_RST);			// IEEE Std 488.2-1992, full reset command

	AT_saveHandler(":PXID", cmdHandler_PXID);		// 	pxi_trig_t[7:0]
	AT_saveHandler(":PXI", cmdHandler_PXI);			//	pxi_trig_i/0[7:0]

	AT_saveHandler(":LBLD", cmdHandler_LBLD);		// 	pxi_lbl6_t
	AT_saveHandler(":LBL", cmdHandler_LBL);			//	pxi_lbl6_i/o

	AT_saveHandler(":LBRD", cmdHandler_LBRD);		// 	pxi_lbr6_t
	AT_saveHandler(":LBR", cmdHandler_LBR);			//	pxi_lbr6_i/o

	AT_saveHandler(":GA", cmdHandler_GA);			//	pxi_ga[4:0]
	AT_saveHandler(":STAR", cmdHandler_STAR);		//	pxi_star_in/pxi_star_out
	AT_saveHandler(":STARE", cmdHandler_STARE);		//	pxi_star_en/pxie_dstar_en
	AT_saveHandler(":STARS", cmdHandler_STARS);		//	pxi_star_sel (0:star_out, 1:pxi_clk10)
	AT_saveHandler(":DSTAR", cmdHandler_DSTAR);		//	pxie_dstar_out
	AT_saveHandler(":DSTARS", cmdHandler_DSTARS);	//	pxie_dstar_sel (0:dstar_out, 1:pxi_clk10)

	AT_saveHandler(":TRGD", cmdHandler_TRGD);		//	trig_oe
	AT_saveHandler(":TRGT", cmdHandler_TRGT);		//	trig_tsel
	AT_saveHandler(":TRG", cmdHandler_TRG);			//	trig_in/out
	AT_saveHandler(":TRGL", cmdHandler_TRGL);		//	trig_in/out	level
}

bool cmd_Parser(void)
{
	char ch;
	bool res = false;
	
	while (kbhit()) {
		ch = getchar();
		//printf("%c", ch);
		//printf("<0x%x>", ch);
		switch (ch) {
			case ASCII_XON:
			case ASCII_XOFF:
				// Ignore XON and XOFF
				break;
			case '#': // Close menu using "#"
				menuGate = 0;		
				printf("* OK\n\r");
				break;
			case '!': // Open menu using "!!!"
				menuGate++;
				if (menuGate == 3) {
					printf("* OK\n\r");
				}
				else if (menuGate > 3) {
					menuGate = 0;							
				}
				break;
			default:
				if (menuGate == 3) {
					// Menu open
					switch (ch) {
						case ' ': // menu
						case '?': // help
							printf("* R \t\t Reboot.\n\r"); OutputDelay();
							printf("* v \t\t Version information.\n\r"); OutputDelay();
							break;
						case 'v': // version
							if (gpio_GetMode()) {
								printf(MODULE_SPLASH, MODULE_ART_PER); OutputDelay();
							}
							else {
								printf(MODULE_SPLASH, MODULE_ART_TIM); OutputDelay();
							}
							break;
						case 'F': // get firmware version
							printf("* %s\n\r", MODULE_VERSION); OutputDelay();
							break;
							
						case 'R': // Reboot
							if (ScanKey()) {
								NVIC_SystemReset();
							}
							break;
							
						default :
							menuGate = 0;
							break;
					}	
				}
				else {
					// Not in the menu
					menuGate = 0;
					
					// Handle the commands
					switch (ch) {
						case ASCII_LF:
							break;
						case ASCII_CR:
							AT_cmdChar(ASCII_CR);
							AT_cmdChar(ASCII_LF);
							AT_process();
							res = AT_reply();
							break;
						default:
							AT_cmdChar(ch);
							break;
					}
				}												
				break;						
		}
	}
	return res;
}

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
