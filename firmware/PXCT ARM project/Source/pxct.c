/*-----------------------------------------------------------------------------
-- pxct.c 
-- PXCT Main
--
-- Copyright INCAA Computers BV, 2021
-- This source describes Open Hardware and is licensed under the
-- CERN-OHL-W v2
-- Source location: https://ohwr.org/project/pxct
-----------------------------------------------------------------------------*/

#include <RTL.h>                      /* RTL kernel functions & defines      */
#include <stdio.h>                    /* standard I/O .h-file                */
#include <ctype.h>                    /* character functions                 */
#include <string.h>                   /* string and memory functions         */
#include <LPC17xx.H>                  /* LPC17xx definitions                 */

#include "lpc_types.h"
#include "lpc17xx_wdt.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_clkpwr.h"

#include "pxct.h"
#include "timer.h"
#include "leds.h"
#include "leds.h"
#include "gpio.h"

#define TIMER_RATE      25000

typedef struct {
	uint32_t	bootflag;
} PXCT_settings_t;

		
/*----------------------------------------------------------------------------
 * 		Main
 *---------------------------------------------------------------------------*/
int main(void)
{
	PXCT_settings_t *settings;
	bool res;
	int cnt;
	uint32_t test=0;

	/*-----------------------------------------------------------------------*/
	// Use the Keil Configuration Wizard when editing system_LPC17xx.c
	// to change the clock and power configuration.
	// When editing the system_LPC17xx.c file within the Keil IDE the 
	// 'Configuration Wizard' is the Tab next to the 'Text Editor' below the
	// editor panel.
	/*-----------------------------------------------------------------------*/
	SystemInit();    /* initialize clocks */

	WDT_Init(WDT_CLKSRC_IRC, WDT_MODE_RESET);
	WDT_Start(5000000); /* 5 seconds */
	init_serial();

	led_Init();
	gpio_Init();
	Mcp3201_Init();

    cmd_Init();
    
	led_StatY(true);
	led_StatRG(LED_YELLOW);

	Timer0_Init(TIMER_RATE);

	test = 1;
	gpio_SetTest(test);

	cnt = 0;
	while (1) {
		gpio_SetTest(test);
		test = ~test;
		if (kbhit()) {
			led_StatY(true);
		}
		if (cmd_Parser()) {
			cnt = 0;
		}
		WDT_Feed();
		if (Timer0_GetFlag()) {
			cnt++;
			led_StatY(false);
		}
		if (cnt == 20) {
			led_StatRG(LED_OFF);
		}
	}
}

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/

