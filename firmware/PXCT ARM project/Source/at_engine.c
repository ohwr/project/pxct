/*-----------------------------------------------------------------------------
-- at_engine.c
--
-- Copyright INCAA Computers BV, 2021
-- This source describes Open Hardware and is licensed under the
-- CERN-OHL-W v2
-- Source location: https://ohwr.org/project/pxct
-----------------------------------------------------------------------------*/
/*

    at_engine.c

    Copyright (c) 2011-2017 MikroElektronika.  All right reserved.

--------------------------------------------------------------------------------

    Version : 0.1.1

    Revision Log :

- 0.0.1 (Apr/2016) Module created                   Milos Vidojevic
    * RX based engine
    * Timer not required
    * Write pointer assingment without user interaction
    * Internal buffer with fixed size
    * Internal handler storage

- 0.1.0 (Dec/2016)  Major changes					Milos Vidojevic
    * Timer based engine
    * Timer requierd
    * User provides write (tx) pointer
    * Buffer provided drunig initialization

- 0.1.1 (Apr/2017) Improvements						Milos Vidojevic
    * MikroC coding rules
    * Parsing improved - all known AT command types implemented 
    * Hanlder protype improved - handler provides response type
    * Handler storage moved to userspace
    * HFC implemented

------------------------------------------------------------------------------*/

#include <stdio.h>
#include "at_engine.h"

/* Write pointer */
static T_AT_UART_Write          fpWrite;

/*
 * Command Storage
 *      counter / size / pointer
 */
static /*volatile*/ uint16_t        rxIdx;
static uint16_t                     rxSize;
static /*volatile*/ char*        		rxStorage;
#define AT_CLEAN_RX_BUFFER()                                                       \
{ rxIdx = 0; *rxStorage = 0; }

/*
 * Response Storage
 *      counter / size / pointer
 */
static /*volatile*/ uint16_t        txIdx;
static uint16_t                     txSize;
static /*volatile*/ char*        		txStorage;
#define AT_CLEAN_TX_BUFFER()                                                       \
{ txIdx = 0; *txStorage = 0; }


/* 
 * Handler vars 
 */
static T_AT_handler                 fpHandler;

/*
 * Handlers Storage
 *      counter / size / pointer
 */
static uint16_t                     handlerIdx;
static uint16_t                     handlerSize;
static T_AT_storage*                handlerStorage;

/* 
 * Look up table for START MARK string, must have "" as 0 member 
 */
#define M_LUT_S      	4
#define MS_LUT_S      	3
char MS_LUT[MS_LUT_S][M_LUT_S] =
{
    "",             	// Default - skipped by search
    "*",           		// *...
    ":"      			// :...
};


/* 
 * Look up table for END MARK string, must have "" as 0 member
 *
 *      ENDMARK represents the command type 
 */
#define AT_UNKNOWN      0
#define AT_TEST_F       1
#define AT_GET_F        2
#define AT_SET_F        3
#define AT_CONFIRM_F    4
#define AT_EXE_F        5

#define ME_LUT_S        6
char ME_LUT[ME_LUT_S][M_LUT_S] =
{
    "",             // Default - skipped by search
    "=?",           // Test
    "?",            // Get
    "=",            // Set
    "!",           	// Confirm
    "\r"          	// Exec
};

/*-----------------------------------------------------------------------------                
        Private Function Prototypes
 ------------------------------------------------------------------------------*/

/*
 * Transmission
 *
 * Transmission uses function pointer provided by the user as an argument
 * to the initialization of the engine.
 */
static void sendText( char *pInput /*, char delimiter */ );

/*
 * Simple Hash code generation
 *
 * Hash code is used to save the command to the storage in aim to have fixed
 * storage space for all functions.
 */
static uint32_t makeHash( char *pCmd );

/*
 * Search handler storage for provided command
 *
 * Function search the storage based on sting length and hash code.
 * If function returns zero command does not exists in storage area.
 */
static uint16_t findHandler( char* pCmd );

/*
 * Search input for strings from LUT table.
 * LUT table must be 2 dimensional char array.
 *
 * Depend of flag returned value is :
 * - index of found string at LUT
 * - found string offset inside input
 * - (-1) no match
 */
static int searchLut( char* pInput, char (*pLut)[ M_LUT_S ], int lutSize, int flag );

/*
 * Parsing
 *
 * @param[in] char* pInput - AT Command
 * @param[out] T_AT_handler* pHandler - handler pointer for the particular command
 * @param[out] int* argOff - offset location of the argument within pInput
 *
 * Function parses provided raw command string and returns previously saved
 * handler and timeout for the particular command. If command is not found
 * the default handler and default timeout will be returned.
 */
static uint8_t parseInput( char *pInput, T_AT_handler *pHandler, int *argOff);

/*-----------------------------------------------------------------------------            
        Private Function Definitions
 ------------------------------------------------------------------------------*/

static void sendText( char *pInput /*, char delimiter */)
{
    while(*pInput)
    {
        fpWrite(*pInput++);
    }
}

static uint32_t makeHash( char *pCmd )
{
    int ch;
	uint32_t hash = 5381;

	ch = *pCmd++;
    while (ch != 0) {
        hash = ((hash << 5) + hash) + ch; /* hash * 33 + c */
    	ch = *pCmd++;
    }
    return hash;
}

static uint16_t findHandler( char* pCmd )
{
    uint8_t     len;
    uint16_t    idx;
    uint32_t    hash;

    idx = 0;
    len = strlen(pCmd);
    hash = makeHash(pCmd);
    for(idx = 1; idx < handlerIdx; idx++)
        if(handlerStorage[idx].len == len)
            if(handlerStorage[idx].hash == hash)
                return idx;
    return _AT_UNKNOWN;
}

/* Flags */
#define LUT_IDX         0
#define IN_OFF          1
static int searchLut( char* pInput, char (*pLut)[ M_LUT_S ], int lutSize, int flag )
{
    uint8_t     inLen;
    uint8_t     inOff;
    uint8_t     lutLen;
    uint8_t     lutIdx;

    inLen = 0;
    inOff = 0;
    lutLen = 0;
    lutIdx = 0;
    if((inLen = strlen(pInput)) > _AT_CMD_MAXSIZE) {
        inLen = _AT_CMD_MAXSIZE;
		}
    for(lutIdx = 1; lutIdx < lutSize; lutIdx++)
    {
        lutLen = strlen(pLut[lutIdx]);
        for(inOff = 0; inOff < inLen; inOff++)
        {
            if(!strncmp(pLut[lutIdx], pInput + inOff, lutLen))
            {
                if(flag == LUT_IDX)
                    return lutIdx;
                else if(flag == IN_OFF)
                    return inOff;
            }
        }
    }
    return -1;
}

static uint8_t parseInput( char *pInput, T_AT_handler *pHandler, int *argOff)
{
    uint8_t hIdx;
    int     startOff, endOff;
    int     startIdx, endIdx;
    char    tmp[ _AT_CMD_MAXSIZE + 1 ];

    hIdx = 0;
    startOff = 0;
    endOff = 0;
    endIdx = 0;
    memset( tmp, 0, _AT_CMD_MAXSIZE + 1 );
    if((startOff = searchLut(pInput, MS_LUT, MS_LUT_S, IN_OFF)) == -1) {
        startOff = 0;
		}
    if((startIdx = searchLut(pInput, MS_LUT, MS_LUT_S, LUT_IDX)) == -1) {
        startIdx = 0;
		}
    if((endOff = searchLut(pInput, ME_LUT, ME_LUT_S, IN_OFF)) == -1) {
        endOff = _AT_CMD_MAXSIZE;
		}
    if((endIdx = searchLut(pInput, ME_LUT, ME_LUT_S, LUT_IDX)) == -1) {
        endIdx = 0;
		}
		strncpy(tmp, pInput + startOff, endOff - startOff);
		
		hIdx = findHandler(tmp);
		*pHandler = handlerStorage[hIdx].handler;
		
		if (endIdx != AT_UNKNOWN /*&& endIdx != AT_EXE_F*/) {
			*argOff = endOff + strlen(ME_LUT[endIdx]);
		}
		else {
			*argOff = endOff;
		}
		//printf("%s>%s[%d,%d][%d][%d][%d]\n\r", pInput, tmp, startOff, endOff, hIdx, endIdx, *argOff);
		
    return endIdx;
}

/*-----------------------------------------------------------------------------
        Public Function Definitions
 ------------------------------------------------------------------------------*/

void AT_initParser
(
        T_AT_UART_Write     pWrite,
        T_AT_handler        pHandler,
        char*            		pInBuffer,
        char*            		pOutBuffer,
        uint16_t            bufferSize,
        T_AT_storage*       pStorage,
        uint16_t            storageSize
)
{
    T_AT_storage cmd;

    cmd.handler     = pHandler;
    cmd.hash        = makeHash("");
    cmd.len         = 0;
    fpWrite         = pWrite;
    rxIdx           = 0;
    rxSize          = bufferSize;
    rxStorage       = pInBuffer;
    txIdx           = 0;
    txSize          = bufferSize;
    txStorage       = pOutBuffer;
    handlerIdx      = 0;
    handlerSize     = storageSize;
    handlerStorage  = pStorage;
    memset((void*)rxStorage, 0, rxSize);
    memset((void*)txStorage, 0, txSize);
    memset((void*)handlerStorage, 0, handlerSize * sizeof(T_AT_storage));
    handlerStorage[handlerIdx] = cmd;
    handlerIdx++;
}

void AT_cmdChar( char rxInput )
{
    *(rxStorage + rxIdx++) = rxInput;
		*(rxStorage + rxIdx) = '\0';	
		if ((rxIdx+1) == rxSize) {
				AT_CLEAN_RX_BUFFER();
		}
}

void AT_rspStr( char *txInput )
{
		char ch;
	
		ch = *txInput++;
    while(ch != 0) {
				*(txStorage + txIdx++) = ch;
				*(txStorage + txIdx) = '\0';
				if ((txIdx+1) == txSize) {
						AT_CLEAN_TX_BUFFER();
				}
				ch = *txInput++;
		}
}

int AT_saveHandler( char *pCmd, T_AT_handler pHandler )
{
    T_AT_storage cmd;

    if(!pHandler) {
        pHandler = handlerStorage[0].handler;
		}
    cmd.len = strlen(pCmd);
    if(cmd.len >= _AT_CMD_MAXSIZE) {
    	//printf("Cmd=%s, Err cmd.len\n\r", pCmd);
    	return 0;
		}
    if(handlerIdx == handlerSize) {
    	//printf("Cmd=%s, Err handlerSize\n\r", pCmd);
        return 0;
		}
    if(findHandler(pCmd)) {
    	//printf("Cmd=%s, Err findHandler\n\r", pCmd);
        return 0;
		}
    cmd.hash        = makeHash(pCmd);
    //printf("Cmd=%s,hash=%d\n\r", pCmd, cmd.hash);
    cmd.handler     = pHandler;
    handlerStorage[handlerIdx] = cmd;
    handlerIdx++;
    return (handlerSize - handlerIdx);
}

char* AT_response( void )
{
	return txStorage;
}

int AT_hasResponse( void )
{
	if (strlen(txStorage) > 0) {
		return 1;
	}
	return 0;
}

void AT_responsed( void )
{
	AT_CLEAN_TX_BUFFER();
}


bool AT_reply( void )
{
	if (AT_hasResponse()) {
		sendText(txStorage);
		AT_responsed();
		return true;
	}
	return false;
}

void AT_process( void)
{
    int argOff;
    uint8_t type;
    
    //printf(">> %s", rxStorage);
    type = parseInput(rxStorage, &fpHandler, &argOff);
    fpHandler(rxStorage, type, argOff);
    AT_CLEAN_RX_BUFFER();
}

/*------------------------------------------------------------------------------

  at_engine.c

  Copyright (c) 2011-2017 MikroElektronika.  All right reserved.

    This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.

------------------------------------------------------------------------------*/
