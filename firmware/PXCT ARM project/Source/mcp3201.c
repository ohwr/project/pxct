/*-----------------------------------------------------------------------------
-- mcp3201.c
--
-- Copyright INCAA Computers BV, 2021
-- This source describes Open Hardware and is licensed under the
-- CERN-OHL-W v2
-- Source location: https://ohwr.org/project/pxct
-----------------------------------------------------------------------------*/

#include <stdbool.h>

#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_ssp.h"

#include "mcp3201.h"

#define MCP_DEBUG 0

#define MCP_SSP_PORT		PINSEL_PORT_0		/* P0.x */
#define MCP_PIN_SSN			PINSEL_PIN_16		/* P0.16 */

#define MCP_SSP_LENGTH		2

static uint8_t 				dac_rx_b[MCP_SSP_LENGTH];
static uint8_t 				dac_tx_b[MCP_SSP_LENGTH];

void Mcp3201_Init(void)
{
	PINSEL_CFG_Type pinsel_cfg;
	SSP_CFG_Type ssp_cfg;

	// SPI Port 0 via P0.15 (SCK0), P0.16 (SSEL0) function GPIO, P0.17 (MISO0), P0.18 (MOSI0)
	pinsel_cfg.Portnum = MCP_SSP_PORT;
	pinsel_cfg.Funcnum = 0;
	pinsel_cfg.Pinmode = PINSEL_PINMODE_PULLUP;
	pinsel_cfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	
	pinsel_cfg.Pinnum = MCP_PIN_SSN; 
	PINSEL_ConfigPin(&pinsel_cfg);

	pinsel_cfg.Funcnum = 2;
	pinsel_cfg.Pinnum = 15; // SCK1
	PINSEL_ConfigPin(&pinsel_cfg);

	pinsel_cfg.Pinmode = PINSEL_PINMODE_PULLDOWN;
	pinsel_cfg.Pinnum = 18; // MOSI1
	PINSEL_ConfigPin(&pinsel_cfg);
	pinsel_cfg.Pinnum = 17; // MISO1
	PINSEL_ConfigPin(&pinsel_cfg);

	GPIO_SetDir(MCP_SSP_PORT,(1 << MCP_PIN_SSN),1); // P0.16 as output

	SSP_ConfigStructInit(&ssp_cfg);
	ssp_cfg.ClockRate = 1000; // Hz
	ssp_cfg.CPOL = 0;
	ssp_cfg.CPHA = 0;
	
	SSP_Init(LPC_SSP0,&ssp_cfg);
	SSP_Cmd(LPC_SSP0, ENABLE);
}

void Mcp3201_Read(uint16_t *data)
{
	SSP_DATA_SETUP_Type SSP_DATA_SETUP;
	uint8_t status;
	uint16_t rdata;
	int i;

	// Sets the dummy data for the read command
	dac_tx_b[0] = 0x00;
	dac_tx_b[1] = 0x00;

	SSP_DATA_SETUP.tx_data = (void*)dac_tx_b;
	SSP_DATA_SETUP.rx_data = (void*)dac_rx_b;
	SSP_DATA_SETUP.length = MCP_SSP_LENGTH;


	GPIO_ClearValue(MCP_SSP_PORT,(1 << MCP_PIN_SSN)); // SSN low
	SSP_ReadWrite(LPC_SSP0, &SSP_DATA_SETUP, SSP_TRANSFER_POLLING);
	GPIO_SetValue(MCP_SSP_PORT,(1 << MCP_PIN_SSN)); // SSN high

#if MCP_DEBUG
	printf(" <");
	for (i=0; i<MCP_SSP_LENGTH; i++) {
		printf(" %02X", dac_rx_b[i]);
	}
	printf("\n");
#endif

	rdata = 0;
	for (i=0; i<MCP_SSP_LENGTH; i++) {
		rdata <<= 8;
		rdata += dac_rx_b[i];
	}
	rdata >>= 1;
	rdata &= 0x0FFF;
	(*data) = rdata;
}

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
