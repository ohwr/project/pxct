/*-----------------------------------------------------------------------------
-- gpio.c
--
-- Copyright INCAA Computers BV, 2021
-- This source describes Open Hardware and is licensed under the
-- CERN-OHL-W v2
-- Source location: https://ohwr.org/project/pxct
-----------------------------------------------------------------------------*/

#include <stdbool.h>
#include "lpc_types.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"

#include "gpio.h"

static void gpio_InitIn(uint8_t port, uint8_t pin)
{
	PINSEL_CFG_Type pinsel_cfg;
	uint32_t bitValue;

	pinsel_cfg.Portnum = port;
	pinsel_cfg.Funcnum = 0;
	pinsel_cfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	pinsel_cfg.Pinnum = pin;
	PINSEL_ConfigPin(&pinsel_cfg);

	gpio_WriteBitDir(port, pin, GPIO_IN);
}

static void gpio_InitOut(uint8_t port, uint8_t pin, bool level)
{
	PINSEL_CFG_Type pinsel_cfg;
	uint32_t bitValue;

	pinsel_cfg.Portnum = port;
	pinsel_cfg.Funcnum = 0;
	pinsel_cfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	pinsel_cfg.Pinnum = pin;
	PINSEL_ConfigPin(&pinsel_cfg);

	gpio_WriteBit(port, pin, level);
	gpio_WriteBitDir(port, pin, GPIO_OUT);
}

void gpio_Init(void)
{
	// Mode
	gpio_InitIn(GPIO_PORT_MODE, GPIO_PIN_MODE);

	// Test
	gpio_InitOut(GPIO_PORT_TEST, GPIO_PIN_TEST, 0);

    // GA[4:0]
	gpio_InitIn(GPIO_PXI_PORT_GA, GPIO_PXI_PIN_GA0);
	gpio_InitIn(GPIO_PXI_PORT_GA, GPIO_PXI_PIN_GA1);
	gpio_InitIn(GPIO_PXI_PORT_GA, GPIO_PXI_PIN_GA2);
	gpio_InitIn(GPIO_PXI_PORT_GA, GPIO_PXI_PIN_GA3);
	gpio_InitIn(GPIO_PXI_PORT_GA, GPIO_PXI_PIN_GA4);

	// PXI_TRIG[7:0]
	gpio_InitIn(GPIO_PXI_PORT_PXI_TRIG, GPIO_PXI_PIN_PXI_TRIG0);
	gpio_InitIn(GPIO_PXI_PORT_PXI_TRIG, GPIO_PXI_PIN_PXI_TRIG1);
	gpio_InitIn(GPIO_PXI_PORT_PXI_TRIG, GPIO_PXI_PIN_PXI_TRIG2);
	gpio_InitIn(GPIO_PXI_PORT_PXI_TRIG, GPIO_PXI_PIN_PXI_TRIG3);
	gpio_InitIn(GPIO_PXI_PORT_PXI_TRIG, GPIO_PXI_PIN_PXI_TRIG4);
	gpio_InitIn(GPIO_PXI_PORT_PXI_TRIG, GPIO_PXI_PIN_PXI_TRIG5);
	gpio_InitIn(GPIO_PXI_PORT_PXI_TRIG, GPIO_PXI_PIN_PXI_TRIG6);
	gpio_InitIn(GPIO_PXI_PORT_PXI_TRIG, GPIO_PXI_PIN_PXI_TRIG7);

	// PXI_LBL6
	gpio_InitIn(GPIO_PXI_PORT_PXI_LBL6, GPIO_PXI_PIN_PXI_LBL6);

	// PXI_LBL6
	gpio_InitIn(GPIO_PXI_PORT_PXI_LBR6, GPIO_PXI_PIN_PXI_LBR6);

	// PXI_STAR, PXIE_DSTAR
	gpio_InitIn(GPIO_PXI_PORT_PXI_STAR_IN, GPIO_PXI_PIN_PXI_STAR_IN);

	gpio_InitOut(GPIO_PXI_PORT_PXI_STAR_OUT, GPIO_PXI_PIN_PXI_STAR_OUT, 0);
	gpio_InitOut(GPIO_PXI_PORT_PXI_STAR_OUT, GPIO_PXI_PIN_PXI_STAR_EN, 0);
	gpio_InitOut(GPIO_PXI_PORT_PXI_STAR_OUT, GPIO_PXI_PIN_PXI_STAR_SEL, 0);

	gpio_InitOut(GPIO_PXI_PORT_PXIE_DSTAR_OUT, GPIO_PXI_PIN_PXIE_DSTAR_OUT, 0);
	gpio_InitOut(GPIO_PXI_PORT_PXIE_DSTAR_OUT, GPIO_PXI_PIN_PXIE_DSTAR_SEL, 0);

	// TRIG
	gpio_InitIn(GPIO_PORT_TRIG, GPIO_PIN_TRIG_IN);
	gpio_InitOut(GPIO_PORT_TRIG, GPIO_PIN_TRIG_OUT, 0);
	gpio_InitOut(GPIO_PORT_TRIG, GPIO_PIN_TRIG_OE, 0);
	gpio_InitOut(GPIO_PORT_TRIG, GPIO_PIN_TRIG_TSEL, 0);

	// TEST
	gpio_InitOut(GPIO_PORT_TEST, GPIO_PIN_TEST, 0);
}

static uint32_t gpio_WidthToMask(int width)
{
	uint32_t mask;
	uint32_t value;

	mask = 0;
	if (width > 0) {
		value = 1 << width;
		mask = value - 1;
	}
	return mask;
}

uint32_t gpio_ReadBits(uint8_t port, int pos, int width)
{
	uint32_t value;
	uint32_t mask;

	mask = gpio_WidthToMask(width);
	value = GPIO_ReadValue(port);
	value >>= pos;
	value &= mask;
	return value;
}

uint32_t gpio_ReadBit(uint8_t port, uint8_t pin)
{
	return gpio_ReadBits(port, pin, 1);
}

void gpio_WriteBits(uint8_t port, int pos, int width, uint32_t value)
{
	int i;
	uint32_t bit;
	bool level;
	int pin;

	for (i=0; i<width; i++) {
		bit = value >> i;
		bit &= 0x1;
		level = ( bit ? true : false);
		pin = pos + i;
		gpio_WriteBit(port, pin, level);
	}
}
 
void gpio_WriteBit(uint8_t port, uint8_t pin, bool level)
{
	if (level) {
		GPIO_SetValue(port,(1 << pin));
	}
	else {
		GPIO_ClearValue(port,(1 << pin));
	}
}

void gpio_WriteBitsDir(uint8_t port, int pos, int width, uint32_t value)
{
	int i;
	uint32_t bit;
	bool dir;
	int pin;

	for (i=0; i<width; i++) {
		bit = value >> i;
		bit &= 0x1;
		dir = ( bit ? GPIO_IN : GPIO_OUT);
		pin = pos + i;
		gpio_WriteBitDir(port, pin, dir);
	}
}

void gpio_WriteBitDir(uint8_t port, uint8_t pin, bool dir) // dir: 0 = output, 1 = input
{
	if (dir == GPIO_IN) {
		GPIO_SetDir(port, (1 << pin), 0);
	}
	else {
		GPIO_SetDir(port, (1 << pin), 1);
	}
}

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
