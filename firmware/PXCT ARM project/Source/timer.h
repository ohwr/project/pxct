/*-----------------------------------------------------------------------------
-- timer.h
--
-- Copyright INCAA Computers BV, 2021
-- This source describes Open Hardware and is licensed under the
-- CERN-OHL-W v2
-- Source location: https://ohwr.org/project/pxct
-----------------------------------------------------------------------------*/

#ifndef SRC_TIMER_H_
#define SRC_TIMER_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

/* Creation en deletion */
void Timer0_Init(uint32_t us);
void Timer0_DeInit(void);

/* Status */
bool Timer0_GetFlag(void);
void Timer0_WaitForFlag(void);
void Timer0_WaitTime(void);
void Sleep(void);

#ifdef __cplusplus
}
#endif

#endif /* SRC_TIMER_H_ */

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
