/*-----------------------------------------------------------------------------
-- command.h
--
-- Copyright INCAA Computers BV, 2021
-- This source describes Open Hardware and is licensed under the
-- CERN-OHL-W v2
-- Source location: https://ohwr.org/project/pxct
-----------------------------------------------------------------------------*/

#ifndef SRC_COMMAND_H_
#define SRC_COMMAND_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>

void cmd_Init(void);
bool cmd_Parser(void);
	
#ifdef __cplusplus
}
#endif

#endif /* SRC_COMMAND_H_ */

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/

