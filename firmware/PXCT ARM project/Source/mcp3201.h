/*-----------------------------------------------------------------------------
-- mcp3201.h
--
-- Copyright INCAA Computers BV, 2021
-- This source describes Open Hardware and is licensed under the
-- CERN-OHL-W v2
-- Source location: https://ohwr.org/project/pxct
-----------------------------------------------------------------------------*/

#ifndef MCP3201_H_
#define MCP3201_H_

#include "LPC17xx.h"
#include "lpc_types.h"

#ifdef __cplusplus
extern "C"
{
#endif

void Mcp3201_Init(void);

void Mcp3201_Read(uint16_t *data);

#ifdef __cplusplus
}
#endif

#endif /* MCP3201_H_ */

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
