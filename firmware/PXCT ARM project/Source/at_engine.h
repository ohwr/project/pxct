/*-----------------------------------------------------------------------------
-- at_engine.h
--
-- Copyright INCAA Computers BV, 2021
-- This source describes Open Hardware and is licensed under the
-- CERN-OHL-W v2
-- Source location: https://ohwr.org/project/pxct
-----------------------------------------------------------------------------*/
/*

    at_engine.h

    Copyright (c) 2011-2017 MikroElektronika.  All right reserved.

------------------------------------------------------------------------------*/

/**
 * @file                                            	at_engine.h
 * @brief                     AT Parser
 *
 * @mainpage AT Parser
 *
 * @{
 *
 * ### Library Description ###
 *
 * Generic AT Parser usable on various architectures.
 *
 * ### Features ###
 *
 *    - Timer based engine
 *    - External buffer
 *    - External handler storage
 *    - External defined HFC functions
 *
 * @note In case of hardware flow control (HFC) usage definition of the
 * external RTS/CTS control and check functions is needed.
 *
 * @}
 *
 * @defgroup AT
 * @brief                   AT Parser
 *
 * @{
 *
 * Global Library Prefix : **AT**
 *
 * @example C4G_Click_ARM_KIN.c
 * @example C4G_Click_ARM_STM.c
 * @example C4G_Click_PIC.c
 * @example C4G_Click_PIC32.c
 * @example C4G_Click_DSPIC.c
 *
 ******************************************************************************/

#ifndef _AT_PARSER_H_
#define _AT_PARSER_H_

#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <stdbool.h>

/*-------------------------- HAL POINTERS ------------------------------------*/

#if defined( __MIKROC_PRO_FOR_ARM__ )       || \
    defined( __MIKROC_PRO_FOR_DSPIC__ )     || \
    defined( __MIKROC_PRO_FOR_PIC32__ )
typedef void ( *T_AT_UART_Write )(unsigned int data_out);

#elif defined( __MIKROC_PRO_FOR_AVR__ )     || \
      defined( __MIKROC_PRO_FOR_PIC__ )     || \
      defined( __MIKROC_PRO_FOR_8051__ )    || \
      defined( __MIKROC_PRO_FOR_FT90x__ )
typedef void ( *T_AT_UART_Write )(unsigned char data_out);
#else
typedef void ( *T_AT_UART_Write )(unsigned char data_out);
#endif

/*----------------------------------------------------------------------------*/

/**
 * @name        Configuration defs
 *
 * Configuration preprocessors.
 *
 ******************************************************************************/
///@{

/** End of response time between characters in ms */
#define _AT_ST_TIMER                                5		
/** Max command size excluding command args */
#define _AT_CMD_MAXSIZE                             15		
/** 0 HFC disabled / 1 HFC enabled */
#define _AT_HFC_CONTROL                             0		

#define _AT_CLRLEV_LOW															1
#define _AT_CLRLEV_HIGH															2

///@}
/**
 * @name                 AT Termintation Characters
 * 
 * Termintation characters - must be last character of the command string.
 *
 ******************************************************************************/
///@{

/** Global termintation character */
#define _AT_TERMINATE                               0x0D
/** Special termination character */
#define _AT_TERMINATE_ADD                           0x1A

/**
 * @name                 AT Command Types
 *
 * Command types provided as second argument during the handler call. 
 *
 * @note Use this values to decide which type of command made handler call.
 *
 ******************************************************************************/
///@{

#define _AT_UNKNOWN                                 (0)
#define _AT_TEST                                    (1)
#define _AT_GET                                     (2)
#define _AT_SET                                     (3)
#define _AT_CONFIRM                                 (4)
#define _AT_EXEC                                    (5)

///@}
/**
 * @name                 AT Parser Types
 ******************************************************************************/
///@{

/**
 * @typedef Handler Prototype
 */
typedef void ( *T_AT_handler )( char *buffer, uint8_t type, int argOff );

/**
 * @brief Parser Structure
 *
 * Struct is used for storing the command with timeout and callbacks.
 * Command strings are converted to the hash code for easiest comparision.
 */
typedef struct
{
    /** Command Length */
    uint16_t                    len;
    /** Command Hash Value */
    uint32_t                    hash;
    /** Get Callback */
    T_AT_handler                handler;

}T_AT_storage;

///@}
/**
 * @name                 AT Driver Functions
 ******************************************************************************/
///@{
#ifdef __cplusplus
extern "C"{
#endif

#if (_AT_HFC_CONTROL)
extern bool AT_getStateDCE();
extern void AT_setStateDTE(bool state);
#endif

/**
 * @brief AT Parser Initialization
 *
 * @param[in] pWrite            UARTx_Write function
 * @param[in] pHandler          default handler
 * @param[in] cmdTimeout        default timeout
 * @param[in] pInBuffer        	command buffer
 * @param[in] pOutBuffer        response buffer
 * @param[in] bufferSize        size of the buffer in bytes
 * @param[in] pStorage          handler storage
 * @param[in] storageSize       handler storage size
 *
 * Initialization should be executed before any other function. User can
 * execute this function later inside the application to reset AT Engine to
 * the default state.
 */
void AT_initParser
(
        T_AT_UART_Write     pWrite,
        T_AT_handler        pHandler,
        char*            		pInBuffer,
        char*            		pOutBuffer,
        uint16_t            bufferSize,
        T_AT_storage*       pStorage,
        uint16_t            storageSize
);

/**
 * @brief Transmit
 *
 * @param[in] txInput       char string transmitted 
 *
 * Function is used to populate response buffer with characters to be 
 * transmitted. 
 */
void AT_rspStr( char *txInput );

/**
 * @brief Receive
 *
 * @param[in] rxInput       char received 
 *
 * Function is used to populate command buffer with characters received.
 */
void AT_cmdChar( char rxInput );

/**
 * @brief Save AT Command to Storage
 *
 * @param[in] pCmd          command string
 * @param[in] pHandler      handler for provided command
 *
 * Saves handlers and timeout for the particular command.
 */
int AT_saveHandler( 
				char *pCmd, 
				T_AT_handler pHandler
);

/**
 * @brief AT response
 *
 * Function should be used with AT command responses to get the response characters
 */
char* AT_response( void );
int AT_hasResponse( void );
void AT_responsed( void );
				
/**
 * @brief AT reply
 *
 * Function should be used with AT command responses to send the text
 */
bool AT_reply( void );

/**
 * @brief AT Engine State Machine
 *
 * This function should be placed inside the infinite while loop.
 */
void AT_process( void );

#ifdef __cplusplus
} // extern "C"
#endif
#endif
///@}
///@}
/*------------------------------------------------------------------------------

  at_engine.h

  Copyright (c) 2011-2017 MikroElektronika.  All right reserved.

    This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.

------------------------------------------------------------------------------*/
