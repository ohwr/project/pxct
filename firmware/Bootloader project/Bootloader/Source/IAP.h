/*****************************************************************************
 * $Id$
 *
 * Project:		NXP LPC1100 Secondary Bootloader Example
 *
 * Description: Provides access to In-Application Programming (IAP) routines
 * 			    contained within the bootROM sector of LPC1100 devices.
 *
 * Copyright(C) 2010, NXP Semiconductor
 * All rights reserved.
 *
 *****************************************************************************
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * products. This software is supplied "AS IS" without any warranties.
 * NXP Semiconductors assumes no responsibility or liability for the
 * use of the software, conveys no license or title under any patent,
 * copyright, or mask work right to the product. NXP Semiconductors
 * reserves the right to make changes in the software without
 * notification. NXP Semiconductors also make no representation or
 * warranty that such application will be suitable for the specified
 * use without further testing or modification.
 *****************************************************************************/
#ifndef __IAP_H
#define	__IAP_H

#include <stdint.h>

/* IAP Command Status Codes */
#define IAP_STA_CMD_SUCCESS 								0
#define IAP_STA_INVALID_COMMAND 							1
#define IAP_STA_SRC_ADDR_ERROR 								2
#define IAP_STA_DST_ADDR_ERROR 								3
#define IAP_STA_SRC_ADDR_NOT_MAPPED 						4
#define IAP_STA_DST_ADDR_NOT_MAPPED 						5
#define IAP_STA_COUNT_ERROR 								6
#define IAP_STA_INVALID_SECTOR 								7
#define IAP_STA_SECTOR_NOT_BLANK							8
#define IAP_STA_SECTOR_NOT_PREPARED_FOR_WRITE_OPERATION 	9
#define IAP_STA_COMPARE_ERROR 								10
#define IAP_STA_BUSY 										11
#define IAP_STA_INVALD_PARAM 								12

#define USER_START_SECTOR 18
#define MAX_USER_SECTOR 29

#define USER_FLASH_START (sector_start_map[USER_START_SECTOR])
#define USER_FLASH_END	 (sector_end_map[MAX_USER_SECTOR])
#define USER_FLASH_SIZE  ((USER_FLASH_END - USER_FLASH_START) + 1)
#define MAX_FLASH_SECTOR 30

#define INVALID_RESULT 0xFFFFFFFF

/* Define start address of each Flash sector */
#define SECTOR_0_START      0x00000000
#define SECTOR_1_START      0x00001000
#define SECTOR_2_START      0x00002000
#define SECTOR_3_START      0x00003000
#define SECTOR_4_START      0x00004000
#define SECTOR_5_START      0x00005000
#define SECTOR_6_START      0x00006000
#define SECTOR_7_START      0x00007000
#define SECTOR_8_START      0x00008000
#define SECTOR_9_START      0x00009000
#define SECTOR_10_START     0x0000A000
#define SECTOR_11_START     0x0000B000
#define SECTOR_12_START     0x0000C000
#define SECTOR_13_START     0x0000D000
#define SECTOR_14_START     0x0000E000
#define SECTOR_15_START     0x0000F000
#define SECTOR_16_START     0x00010000
#define SECTOR_17_START     0x00018000
#define SECTOR_18_START     0x00020000
#define SECTOR_19_START     0x00028000
#define SECTOR_20_START     0x00030000
#define SECTOR_21_START     0x00038000
#define SECTOR_22_START     0x00040000
#define SECTOR_23_START     0x00048000
#define SECTOR_24_START     0x00050000
#define SECTOR_25_START     0x00058000
#define SECTOR_26_START     0x00060000
#define SECTOR_27_START     0x00068000
#define SECTOR_28_START     0x00070000
#define SECTOR_29_START     0x00078000



/* Define end address of each Flash sector */
#define SECTOR_0_END        0x00000FFF
#define SECTOR_1_END        0x00001FFF
#define SECTOR_2_END        0x00002FFF
#define SECTOR_3_END        0x00003FFF
#define SECTOR_4_END        0x00004FFF
#define SECTOR_5_END        0x00005FFF
#define SECTOR_6_END        0x00006FFF
#define SECTOR_7_END        0x00007FFF
#define SECTOR_8_END        0x00008FFF
#define SECTOR_9_END        0x00009FFF
#define SECTOR_10_END       0x0000AFFF
#define SECTOR_11_END       0x0000BFFF
#define SECTOR_12_END       0x0000CFFF
#define SECTOR_13_END       0x0000DFFF
#define SECTOR_14_END       0x0000EFFF
#define SECTOR_15_END       0x0000FFFF
#define SECTOR_16_END       0x00017FFF
#define SECTOR_17_END       0x0001FFFF
#define SECTOR_18_END       0x00027FFF
#define SECTOR_19_END       0x0002FFFF
#define SECTOR_20_END       0x00037FFF
#define SECTOR_21_END       0x0003FFFF
#define SECTOR_22_END       0x00047FFF
#define SECTOR_23_END       0x0004FFFF
#define SECTOR_24_END       0x00057FFF
#define SECTOR_25_END       0x0005FFFF
#define SECTOR_26_END       0x00067FFF
#define SECTOR_27_END       0x0006FFFF
#define SECTOR_28_END       0x00077FFF
#define SECTOR_29_END       0x0007FFFF


/* Define the flash page size, this is the minimum amount of data can be written in one operation */
#define IAP_FLASH_PAGE_SIZE_BYTES							256
#define IAP_FLASH_PAGE_SIZE_WORDS							(IAP_FLASH_PAGE_SIZE_BYTES >> 2)

extern const uint32_t sector_start_map[];
extern const uint32_t sector_end_map[];

void vIAP_ReinvokeISP(void);
uint32_t IAP_ReadPartID(uint32_t *pu32PartID);
uint32_t IAP_ReadBootVersion(uint32_t *pu32Major, uint32_t *pu32Minor);
uint32_t IAP_EraseSectors(uint32_t u32StartSector, uint32_t u32EndSector);
uint32_t IAP_PrepareSectors(uint32_t u32StartSector, uint32_t u32EndSector);
uint32_t IAP_CopyRAMToFlash(uint32_t u32DstAddr, uint32_t u32SrcAddr, uint32_t u32Len);
uint32_t IAP_BlankCheckSectors(uint32_t u32StartSector, uint32_t u32EndSector, uint32_t *pu32Result);
uint32_t IAP_Compare(uint32_t u32DstAddr, uint32_t u32SrcAddr, uint32_t u32Len, uint32_t *pu32Offset);
void u32IAP_ReadSerialNumber(uint32_t *pu32byte0, uint32_t *pu32byte1, uint32_t *pu32byte2, uint32_t *pu32byte3);

int IAP_PrepareErase(uint32_t sector_index);
int IAP_Program(uint32_t sector_index, uint32_t app_addr,uint8_t* data,uint32_t length);

void boot_jump( uint32_t address );

#endif /* end __IAP_H */

/*****************************************************************************
**                            End Of File
******************************************************************************/
