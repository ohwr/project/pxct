/*	======================================================================== */
/*  Copyright INCAA Computers BV, 2021                                       */
/*  This source describes Open Hardware and is licensed under the            */
/*  CERN-OHL-W v2                                                            */
/*  Source location: https://ohwr.org/project/pxct                           */
/*	======================================================================== */

#include <stdio.h>
#include <string.h>
#include "IAP.h"
#include "lpc17xx_wdt.h"
#include "lpc17xx_gpio.h"

/*	======================================================================== */
/*	This bootloader uses the following memory map of the LPC1758:			 */
/*	Sector	Size (kB)	Used by								Start addr (hex) */
/*	======================================================================== */
/*	0		4			bootloader (4 KB max)				0			 	 */
#define BOOT_SEC	0
#define BOOT_ADDR	0x00000
/*	------------------------------------------------------------------------ */
/*	1		4			flag and config memory (4 KB max)	1000			 */
#define CFG_SEC		1
#define CFG_ADDR	0x01000
/*	------------------------------------------------------------------------ */
/*	2		4			program	248 kB (max)				2000			 */
#define CODE_SEC	2
#define CODE_ADDR	0x02000
/*	3		4												3000			 */
/*	4		4												4000			 */
/*	5		4	 											5000			 */
/*	6		4	 											6000			 */
/*	7		4	 											7000			 */
/*	8		4	 											8000			 */
/*	9		4	 											9000			 */
/*	10		4	 											A000			 */
/*	11		4	 											B000			 */
/*	12		4	 											C000			 */
/*	13		4	 											D000			 */
/*	14		4	 											E000			 */
/*	15		4	 											F000			 */
/*	16		32	 											10000			 */
/*	17		32	 											18000			 */
/*	18		32	 											20000			 */
/*	19		32	 											28000			 */
/*	20		32	 											30000			 */
/*	21		32	 											38000			 */
/*	------------------------------------------------------------------------ */
/*	22		32			New program	248 kB (max)			40000			 */
#define NEW_ADDR	0x40000
/*	23		32												48000			 */
/*	24		32	 											50000			 */
/*	25		32	 											58000			 */
/*	26		32	 											60000			 */
/*	27		32	 											68000			 */
/*	28		32	 											70000			 */
/*	29		32			last 8 KB unused					78000			 */
/*	------------------------------------------------------------------------ */

uint32_t boot_flag = 0;
uint32_t cluster_version = 0;
uint32_t boot_start = 0;
int sector = 0;
int sector_size = 0;
int offset = 0;
int block = 0;
int result = 0;

#define IAP_BLOCK_SIZE 	256
#define BUF_CFG_SIZE 	4096  /* 4 kB sector */
unsigned char buf_data[IAP_BLOCK_SIZE];
unsigned char buf_config[BUF_CFG_SIZE];

#define LED_STATY	(1 << 10)
#define LED_STATG	(1 << 8)
#define LED_STATR	(1 << 9)

int main (void) {

	/* Initialize WDT (clock source from internal RC oscillator, reset on timeout) */
	WDT_Init(WDT_CLKSRC_IRC, WDT_MODE_RESET);

	/* Start WDT (30s = 30000000us) */
	WDT_Start(30000000);

	/* GPIO config Port 1: STATY (pin 10), STATG (pin 8), STATR (pin 9) as output */
	GPIO_SetDir(1, LED_STATY | LED_STATG | LED_STATR, 1);

	/* Set STATY LED */
	GPIO_SetValue(1, LED_STATY);
	/* Set STAT LED to yellow */
	GPIO_ClearValue(1, LED_STATG);
	GPIO_ClearValue(1, LED_STATR);
	
	/* Read flag from flash */
	memcpy(&boot_flag, (void*)CFG_ADDR, 4);
	
	/* Test flag and perform actions if set */
	if (boot_flag == 0xDEADBEEF) {
		/* Erase old program */
		for(sector=CODE_SEC; sector<=21; sector++) {
			/* Don't check result, only on write, so always write is tried before WDT reset */
			IAP_PrepareErase(sector);
		}
		
		/* Copy new program */
		/* 4 kB sectors */
		sector_size = 4096;
		for(sector=CODE_SEC; sector<=15; sector++) {
			for(block=0; block<(sector_size/IAP_BLOCK_SIZE); block++) {
				offset = (sector*sector_size) + block*IAP_BLOCK_SIZE;
				memcpy(&buf_data[0], (void*)(NEW_ADDR + offset), IAP_BLOCK_SIZE);
				result = IAP_Program(sector, offset, buf_data, IAP_BLOCK_SIZE);
				if (result) {
					/* Clear STATY LED */
					GPIO_ClearValue(1, LED_STATY);
					/* Change STAT LED to red */
					GPIO_ClearValue(1, LED_STATR);
					GPIO_SetValue(1, LED_STATG);
					while(1);
				}
			}
		}

		/* 32 kB sectors */
		sector_size = 32768;
		for(sector=16; sector<=21; sector++) {
			for(block=0; block<(sector_size/IAP_BLOCK_SIZE); block++) {
				offset = (sector-16)*sector_size + block*IAP_BLOCK_SIZE;
				memcpy(&buf_data[0], (void*)(0x00050000 + offset), IAP_BLOCK_SIZE);
				result = IAP_Program(sector, (0x00010000 + offset), buf_data, IAP_BLOCK_SIZE);
				if (result) {
					/* Clear STATY LED */
					GPIO_ClearValue(1, LED_STATY);
					/* Change STAT LED to red */
					GPIO_ClearValue(1, LED_STATR);
					GPIO_SetValue(1, LED_STATG);
					while(1);
				}
			}
		}

		/* Read config area from flash */
		memcpy(&buf_config, (void*)CFG_ADDR, sizeof(buf_config));
		/* Erase flag within config area */
		buf_config[0] = 0;
		buf_config[1] = 0;
		buf_config[2] = 0;
		buf_config[3] = 0;
		IAP_PrepareErase(CFG_SEC);
		sector_size = 4096;
		for(block=0; block<(sector_size/IAP_BLOCK_SIZE); block++) {
			offset = block*IAP_BLOCK_SIZE;
			result = IAP_Program(CFG_SEC, (CFG_ADDR + offset), &buf_config[offset], IAP_BLOCK_SIZE);
			if (result) {
				/* Clear STATY LED */
				GPIO_ClearValue(1, LED_STATY);
				/* Change STAT LED to red */
				GPIO_ClearValue(1, LED_STATR);
				GPIO_SetValue(1, LED_STATG);
				while(1);
			}
		}
	}
	/* Jump to user program (vector table is handled in system_init) */
	memcpy(&boot_start, (void*)CODE_ADDR, 4);
	if (boot_start != 0xFFFFFFFF) {
		/* Program at boot start location */
		boot_jump(CODE_ADDR);
	}
	else {
		/* Change STAT LED to red */
		GPIO_ClearValue(1, LED_STATR);
		GPIO_SetValue(1, LED_STATG);
	}
}
